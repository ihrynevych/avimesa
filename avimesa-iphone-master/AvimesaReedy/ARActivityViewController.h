//
//  ARActivityViewController.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 2/3/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARActivityViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *informationLabel;

-(void)setProgressValue:(int)progressValue;

@end
