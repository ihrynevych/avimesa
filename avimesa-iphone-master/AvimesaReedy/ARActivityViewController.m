//
//  ARActivityViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 2/3/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARActivityViewController.h"

@interface ARActivityViewController ()
{
    int percentProgressValue;
}

//progress view
//progress view components
@property (nonatomic,weak) IBOutlet UIView * progressBorderView;
@property (nonatomic,weak) IBOutlet UIView * progressBackgroundView;
@property (nonatomic,weak) IBOutlet UIView * progressFillView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressLeadingSpaceConstraint;

//progress view labels
@property (weak, nonatomic) IBOutlet UILabel *progressTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressBackgroundLabel;

@end

@implementation ARActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupProgress];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateProgressView];
}

-(void)setupProgress{
    
    // rounded corners
    self.progressBackgroundView.layer.cornerRadius = self.progressBackgroundView.bounds.size.height/2;
    self.progressBorderView.layer.cornerRadius = self.progressBorderView.bounds.size.height/2;
    self.progressFillView.layer.cornerRadius = self.progressFillView.bounds.size.height/2;
}
#pragma mark Progress View

-(void)setProgressValue:(int)progressValue
{
    percentProgressValue = progressValue;
    [self updateProgressView];
}

-(void)updateProgressView
{
    // update text
    self.progressTopLabel.text = [NSString stringWithFormat:@"%d %%",percentProgressValue];
    self.progressBackgroundLabel.text = [NSString stringWithFormat:@"%d %%",percentProgressValue];
    
    //calulate max width
    CGFloat maxWidth = self.progressBorderView.bounds.size.width - 2*self.progressLeadingSpaceConstraint.constant;
    
    //calculate progress value width
    CGFloat progressWidth;
    if (percentProgressValue >=100)
        progressWidth = maxWidth;
    else if (percentProgressValue<=0)
        progressWidth = 0.0f;
    else
        progressWidth = maxWidth*percentProgressValue/100;
    
    //update progress
    self.progressWidthConstraint.constant = progressWidth;
    
}

@end
