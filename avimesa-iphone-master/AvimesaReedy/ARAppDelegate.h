//
//  ARAppDelegate.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 8/27/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARUpdateController.h"
@interface ARAppDelegate : UIResponder <UIApplicationDelegate>
{
    UIAlertView *notificationAlertView;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ARUpdateController *dataUpdateController;
@end
