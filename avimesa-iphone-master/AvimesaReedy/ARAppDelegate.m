//
//  ARAppDelegate.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 8/27/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARAppDelegate.h"
#import <AWSSNS/AWSSNS.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "ARLoginManager.h"
#import "ARDeviceManager.h"


@implementation ARAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    //register for remote notification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
//    [application registerForRemoteNotificationTypes:
//     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
//    [UIAlertView startInstanceMonitor];
    //choose navigation type
    if ([[ARLoginManager sharedLoginManager] profileId]) {
        if ([[ARDeviceManager sharedManager] isExistsLocation])
            [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeMain];
        else
            [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeWelcome];
    }
    else
        [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeLogin];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (ARUpdateController*)dataUpdateController
{
    if (!_dataUpdateController) {
        _dataUpdateController = [[ARUpdateController alloc] init];
    }
    return _dataUpdateController;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        @try
        {
            NSString *token = [[[[deviceToken description]
                                 stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                stringByReplacingOccurrencesOfString: @">" withString: @""]
                               stringByReplacingOccurrencesOfString: @" " withString: @""];
            
            //init sns client
            AmazonSNSClient *snsClient = [[AmazonSNSClient alloc] initWithAccessKey:AMAZON_ACCESS_KEY_ID withSecretKey:AMAZON_SECRET_KEY];
            snsClient.endpoint = AMAZON_ENDPOINT_URL;
            
            //create endpoint request
            SNSCreatePlatformEndpointRequest *request = [[SNSCreatePlatformEndpointRequest alloc] init];
            
            //endpoint attributes
            [request setPlatformApplicationArn:AMAZON_PLATFORM_ARN];
            [request setToken:token];
            [request setAttributesValue:@"true" forKey:@"Enabled"];
            [request setCustomUserData:@"app device"];
            
            NSLog(@"%@", token);
            
            //create endpoint response
            SNSCreatePlatformEndpointResponse * response = [snsClient createPlatformEndpoint:request];
            
            if (response && !response.error && response.endpointArn)
            {
                //save endpoint arn
                [[NSUserDefaults standardUserDefaults] setObject:response.endpointArn forKey:AMAZON_ENDPOINT_ARN_KEY];
            }
            else
            {
                //remove endpoint arn
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:AMAZON_ENDPOINT_ARN_KEY];
            }
            [[NSUserDefaults standardUserDefaults]  synchronize];
        }
        @catch (AmazonServiceException *serviceException) {
            
            NSLog(@"%@", serviceException);
        }
    });
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    application.applicationIconBadgeNumber = 0;
    if ([userInfo valueForKey:@"aps"]&&[userInfo[@"aps"] valueForKey:@"alert"]) {
        NSString *msg = [NSString stringWithFormat:@"%@", [userInfo[@"aps"] valueForKey:@"alert"]];
        NSLog(@"%@",msg);
        [self createAlert:msg];
    }
    
}

- (void)createAlert:(NSString *)msg {
    if (notificationAlertView&& [notificationAlertView isVisible]) {
        [notificationAlertView dismissWithClickedButtonIndex:0 animated:NO];
    }
    notificationAlertView = [[UIAlertView alloc] initWithTitle:@"Message Received" message:[NSString stringWithFormat:@"%@", msg]delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [notificationAlertView show];
}
@end
