//
//  ARApplicationFacade.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/14/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ARAppDelegate;
@class MFSideMenuContainerViewController;

typedef NS_ENUM (NSUInteger, ARNavigationType) {
    ARNavigationTypeLogin,
    ARNavigationTypeWelcome,
    ARNavigationTypeMain
};

@interface ARApplicationFacade : NSObject

@property (nonatomic,strong) ARAppDelegate* appDelegate;
@property (nonatomic,strong) MFSideMenuContainerViewController* menuContainer;

+ (ARApplicationFacade *)sharedInstance;

- (void)switchToType:(ARNavigationType)type;

@end
