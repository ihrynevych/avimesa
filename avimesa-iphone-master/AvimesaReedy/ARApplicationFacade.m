//
//  ARApplicationFacade.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/14/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARApplicationFacade.h"
#import "ARAppDelegate.h"
#import "MFSideMenuContainerViewController.h"
#import "ARLoginManager.h"
@interface ARApplicationFacade()


@end
@implementation ARApplicationFacade

#pragma mark - Init

+ (ARApplicationFacade *)sharedInstance
{
    static id _default = nil;
    static dispatch_once_t safer;
    dispatch_once(&safer, ^(void)
                  {
                      _default = [[[self class] alloc] init];
                  });
    return _default;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        self.appDelegate = (ARAppDelegate*)[[UIApplication sharedApplication] delegate];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogout) name:AR_USER_LOGIN_NOT_VALID object:nil];
    }
    return self;
}

#pragma mark - public

-(void)userLogout
{
    //sign out
    [[ARLoginManager sharedLoginManager] performLogout];
    [self switchToType:ARNavigationTypeLogin];
}

- (void)switchToType:(ARNavigationType)type
{
    if (self.appDelegate) {
        // get controller identifier
        NSString * vcName = [self nameOfControllerWithType:type];
        
        //main storyboard
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        
        if (type == ARNavigationTypeLogin) {
            //login navigation
            UIViewController *vc = (UIViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:vcName];
            
            self.menuContainer = nil;
            self.appDelegate.window.rootViewController = vc;
            [self.appDelegate.window makeKeyAndVisible];
            
        }else if (type == ARNavigationTypeMain) {
            //standart navigation
            UIViewController *vc = (UIViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:vcName];
            //init side menu
            MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                            containerWithCenterViewController:nil
                                                            leftMenuViewController:vc
                                                            rightMenuViewController:nil];
            
            self.menuContainer = container;
            self.appDelegate.window.rootViewController = container;
            [self.appDelegate.window makeKeyAndVisible];
        }else if (type == ARNavigationTypeWelcome)
        {
            //welcome navigation
            UIViewController *vc = (UIViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:vcName];
            self.menuContainer = nil;
            self.appDelegate.window.rootViewController = vc;
            [self.appDelegate.window makeKeyAndVisible];
        }
    }

}

- (NSString *)nameOfControllerWithType:(ARNavigationType)type
{
    NSString *name;
    switch (type) {
        case ARNavigationTypeLogin:
            name = @"ARLoginViewController";
            break;
        case ARNavigationTypeMain:
            name = @"ARSideMenuController";
            break;
        case ARNavigationTypeWelcome:
            name = @"ARWelcomeNavigationController";
            break;
        default:
            name = @"";
            break;
    }
    return name;
}
@end
