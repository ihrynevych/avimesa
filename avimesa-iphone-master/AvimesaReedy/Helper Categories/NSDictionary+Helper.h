//
//  NSDictionary+Helper.h


#import <Foundation/Foundation.h>

@interface NSDictionary (Helper)

+ (NSDictionary*)dictionaryWithJSON:(NSString*)json;
+ (NSDictionary*)textAttributesDictionary;
+ (NSDictionary*)subtextAttributesDictionary;
+ (NSDictionary*)thumbnailTextAttributesDictionary;

@end
