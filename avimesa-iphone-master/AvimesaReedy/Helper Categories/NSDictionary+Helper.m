//
//  NSDictionary+Helper.m
//

#import "NSDictionary+Helper.h"

@implementation NSDictionary (Helper)

+ (NSDictionary*)dictionaryWithJSON:(NSString*)json
{
    NSError *error = nil;
    NSData* data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        NSLog(@"error parsing JSON data: %@", [error localizedDescription]);
        return nil;
    }
    //NSLog(@"dictionary from json: %@", dictionary);
    return dictionary;
}
+(NSDictionary *)textAttributesDictionary
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:22.0f],
                                 NSForegroundColorAttributeName:[UIColor whiteColor],
                                 NSParagraphStyleAttributeName:paragraphStyle};
    return attributes;
}
+(NSDictionary *)subtextAttributesDictionary
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Italic" size:16.0f],
                                 NSForegroundColorAttributeName:[UIColor whiteColor],
                                 NSParagraphStyleAttributeName:paragraphStyle};
    return attributes;
}
+(NSDictionary *)thumbnailTextAttributesDictionary
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Italic" size:12.0f],
                                 NSForegroundColorAttributeName:[UIColor whiteColor],
                                 NSParagraphStyleAttributeName:paragraphStyle};
    return attributes;
}

@end
