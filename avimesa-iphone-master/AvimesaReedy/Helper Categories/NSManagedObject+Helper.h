//
//  NSManagedObject+Helper.h
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Helper)

- (void)safeSetValuesForKeysWithDictionary:(NSDictionary *)keyedValues withAttributesExclusion:(NSDictionary*)attributesExclusion;
- (void)safeSetValuesForKeysWithDictionary:(NSDictionary *)keyedValues;
- (void)clearAllAttributesWithExclusion:(NSArray*)attributesToKeep;
- (void)clearAllRelationshipsWithExclusion:(NSArray*)relationshipsToKeep;

@end
