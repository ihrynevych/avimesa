//
//  NSManagedObject+Helper.m
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "NSManagedObject+Helper.h"
#import "NSString+Helper.h"



@implementation NSManagedObject (Helper)
-(void)safeSetValuesForKeysWithDictionary:(NSDictionary *)keyedValues withAttributesExclusion:(NSDictionary *)attributesExclusion
{
    NSDictionary *attributes = [[self entity] attributesByName];
    for (NSString *attribute in attributes) {
        id value = [keyedValues objectForKey:attribute];
        if (attributesExclusion && value == nil && [attributesExclusion objectForKey:attribute]) {
            value = [keyedValues objectForKey:[attributesExclusion objectForKey:attribute]];
        }
        if (value == nil) {
            // Don't attempt to set nil, or you'll overwite values in self that aren't present in keyedValues
            continue;
        }
        NSAttributeType attributeType = [[attributes objectForKey:attribute] attributeType];
        if ((attributeType == NSStringAttributeType) && ([value isKindOfClass:[NSNumber class]])) {
            value = [value stringValue];
        } else if (((attributeType == NSInteger16AttributeType) || (attributeType == NSInteger32AttributeType) || (attributeType == NSInteger64AttributeType) || (attributeType == NSBooleanAttributeType)) && ([value isKindOfClass:[NSString class]])) {
            value = [NSNumber numberWithInteger:[value  integerValue]];
        } else if ((attributeType == NSFloatAttributeType) && ([value isKindOfClass:[NSString class]])) {
            value = [NSNumber numberWithDouble:[value doubleValue]];
        }
        else if (attributeType == NSDateAttributeType) 
        {

            value= [NSDate dateWithTimeIntervalSince1970:[(NSNumber*)value longLongValue]];
            NSLog(@"");
        }
        
        [self setValue:value forKey:attribute];
    }
}
- (void)safeSetValuesForKeysWithDictionary:(NSDictionary *)keyedValues
{
    [self safeSetValuesForKeysWithDictionary:keyedValues withAttributesExclusion:nil];
}

- (void)clearAllAttributesWithExclusion:(NSArray*)attributesToKeep
{
    NSDictionary *attributes = [self.entity attributesByName];
    for (NSString *attribute in attributes) {
        if (![attributesToKeep containsObject:attribute]) {
            [self setValue:nil forKey:attribute];
            NSLog(@"Cleared attribute: %@", attribute);
        } else {
            NSLog(@"Did not clear attribute: %@", attribute);
        }
    }
}

- (void)clearAllRelationshipsWithExclusion:(NSArray*)relationshipsToKeep
{
    NSDictionary *relationships = [self.entity relationshipsByName];
    for (NSString *relationship in relationships) {
        if (![relationshipsToKeep containsObject:relationship]) {
            [self setValue:nil forKey:relationship];
            NSLog(@"Cleared relationship: %@", relationship);
        } else {
            NSLog(@"Did not clear relationship: %@", relationship);
        }
    }
}


@end
