//
//  NSManagedObjectContext+Helper.h
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Helper)

- (NSArray*)fetchObjectsWithEntityName:(NSString*)entityName
                             predicate:(NSPredicate*)predicate
                               sortKey:(NSString*)sortKey
                             ascending:(BOOL)ascending;

- (NSManagedObject*)fetchObjectWithEntityName:(NSString*)entityName
                                    predicate:(NSPredicate*)predicate;

- (NSManagedObject*)createObjectWithEntityName:(NSString*)entityName;

- (NSString*)searchEntityNameForString:(NSString*)string;

- (NSArray*) updatedObjectsByIdentifierUsingUpdates:(NSArray*)updates
                                         entityName:(NSString*)entityName;

- (NSArray*) updatedObjectsByIdentifierUsingUpdates:(NSArray*)updates
                                         entityName:(NSString*)entityName
                            withAttributesExclusion:(NSDictionary*)attributesExclusion
                            deleteNotUpdatedObjects:(BOOL)deleteObjects;

+ (NSManagedObjectContext*)mainQContext;

+ (NSManagedObjectContext*)privateQFetchingContext;

+ (NSManagedObjectContext*)privateQParsingContextObserved;

+ (void)stopObserverForPrivateQParsingContext:(NSManagedObjectContext*)context;

+ (NSManagedObjectContext*)confinementConcurrencyTypeContext;
@end
