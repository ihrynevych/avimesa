//
//  NSManagedObjectContext+Helper.m
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "NSManagedObjectContext+Helper.h"
#import "NSString+Helper.h"
#import "NSManagedObject+Helper.h"
#import "ARCoreDataStack_Private.h"

#define  OBJECT_IDENTIFIER_VALUE_KEY @"identifier"

@implementation NSManagedObjectContext (Helper)
- (NSArray*)fetchObjectsWithEntityName:(NSString*)entityName predicate:(NSPredicate*)predicate sortKey:(NSString*)sortKey ascending:(BOOL)ascending
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
    [fetchRequest setEntity:entity];
    
    if (predicate) [fetchRequest setPredicate:predicate];
    
    if (sortKey) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending];
        NSArray *sortDescriptors = @[sortDescriptor];
        [fetchRequest setSortDescriptors:sortDescriptors];
    }
    
    __block NSArray *fetchedObjects = nil;
    __block NSError *error = nil;
    if (self.concurrencyType == NSConfinementConcurrencyType) {
        fetchedObjects = [self executeFetchRequest:fetchRequest error:&error];
    } else {
        [self performBlockAndWait:^{
            fetchedObjects = [self executeFetchRequest:fetchRequest error:&error];
        }];
    }
    
    if (fetchedObjects == nil || [fetchedObjects count] == 0) {
        return nil;
    }
    else {
        return fetchedObjects;
    }
}

- (NSManagedObject*)fetchObjectWithEntityName:(NSString*)entityName predicate:(NSPredicate*)predicate
{
    NSArray *results = [self fetchObjectsWithEntityName:entityName predicate:predicate sortKey:nil ascending:NO];
//    if ([results count] == 1) {
//        return [results lastObject];
//    }
    return [results lastObject];
}

- (NSManagedObject*)createObjectWithEntityName:(NSString*)entityName
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
    return [[NSManagedObject alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:self];
}

- (NSString*)searchEntityNameForString:(NSString*)string
{
    NSDictionary *entitiesByName = [self.persistentStoreCoordinator.managedObjectModel entitiesByName];
    for (NSString *name in entitiesByName.allKeys) {
        if ([string isEqualIgnoringCase:name]) {
            return name;
        }
    }
    return nil;
}

- (NSArray*)updatedObjectsByIdentifierUsingUpdates:(NSArray*)updates entityName:(NSString*)entityName
{
    return [self updatedObjectsByIdentifierUsingUpdates:updates entityName:entityName withAttributesExclusion:nil deleteNotUpdatedObjects:YES];
}

-(NSArray *)updatedObjectsByIdentifierUsingUpdates:(NSArray *)updates entityName:(NSString *)entityName withAttributesExclusion:(NSDictionary *)attributesExclusion deleteNotUpdatedObjects:(BOOL)deleteObjects
{
    NSString *tempIdentifierKey ;
    if (attributesExclusion) {
        tempIdentifierKey = [attributesExclusion valueForKey:OBJECT_IDENTIFIER_VALUE_KEY];
    }
    //fetch all objects:
    NSArray *allObjects = [self fetchObjectsWithEntityName:entityName predicate:nil sortKey:nil ascending:NO];
    
    //predicate:
    NSArray *identifiers = [updates valueForKey: tempIdentifierKey ? tempIdentifierKey : OBJECT_IDENTIFIER_VALUE_KEY];
    NSMutableArray *subpredicates = [NSMutableArray array];
    for (NSNumber *identifier in identifiers) {
        [subpredicates addObject:[NSPredicate predicateWithFormat:@"%K == %@",OBJECT_IDENTIFIER_VALUE_KEY, identifier]];
    }
    NSPredicate *orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:subpredicates];
    NSPredicate *notPredicate = [NSCompoundPredicate notPredicateWithSubpredicate:orPredicate];
    
    //split objects list:
    NSArray *objectsToDelete = [allObjects filteredArrayUsingPredicate:notPredicate];
    NSArray *objectsToUpdate = [allObjects filteredArrayUsingPredicate:orPredicate];
    
    if (deleteObjects) {
        //delete objects that are not present in updates array:
        for (NSManagedObject *objectToDelete in objectsToDelete) {
            [self deleteObject:objectToDelete];
        }
    }
    
    
    //update objects:
    NSMutableArray *updatedObjects = [NSMutableArray array];
    for (NSDictionary *update in updates) {
        if (update[tempIdentifierKey ? tempIdentifierKey : OBJECT_IDENTIFIER_VALUE_KEY]) {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",OBJECT_IDENTIFIER_VALUE_KEY,[update valueForKey:tempIdentifierKey ? tempIdentifierKey : OBJECT_IDENTIFIER_VALUE_KEY]];
            NSManagedObject *object = [[objectsToUpdate filteredArrayUsingPredicate:predicate] lastObject];
            if (object) {
                [object safeSetValuesForKeysWithDictionary:update withAttributesExclusion:attributesExclusion];
            } else {
                object = [self createObjectWithEntityName:entityName];
                [object safeSetValuesForKeysWithDictionary:update withAttributesExclusion:attributesExclusion];
            }
            if (object) [updatedObjects addObject:object];
        }
    }
    return [updatedObjects count] ? updatedObjects : nil;
}
+ (NSManagedObjectContext*)mainQContext
{
    return [[ARCoreDataStack sharedInstance] mainQueueManagedObjectContext];
}

+ (NSManagedObjectContext*)privateQFetchingContext
{
    NSPersistentStoreCoordinator *coordinator = [[ARCoreDataStack sharedInstance] persistentStoreCoordinator];
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [context performBlockAndWait:^{
        context.persistentStoreCoordinator = coordinator;
    }];
    return context;
}

+ (NSManagedObjectContext*)privateQParsingContextObserved
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [[ARCoreDataStack sharedInstance] startPrivateQContextObserving:context];
    return context;
}

+ (NSManagedObjectContext*)confinementConcurrencyTypeContext
{
    NSPersistentStoreCoordinator *coordinator = [[ARCoreDataStack sharedInstance] persistentStoreCoordinator];
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = coordinator;
    return context;
}

+ (void)stopObserverForPrivateQParsingContext:(NSManagedObjectContext*)context
{
    [[ARCoreDataStack sharedInstance] stopPrivateQContextObserving:context];
}
@end
