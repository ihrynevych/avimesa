#import <Foundation/Foundation.h>

@interface NSString (Helper)

- (BOOL)containsSubstring:(NSString*)substring;
- (BOOL)isEqualIgnoringCase:(NSString *)string;


+ (NSString*)stringWithBundledJsonNamed:(NSString*)fileName;
+ (NSString*)stringWithBundledHTMLNamed:(NSString*)fileName;

//hex strings
+ (NSData *)dataFromHexString:(NSString *)string;
+ (NSString *)hexStringFromData:(NSData *)data;

@end
