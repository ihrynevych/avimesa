#import "NSString+Helper.h"

@implementation NSString (Helper)

- (BOOL)isEqualIgnoringCase:(NSString *)string 
{
    return [self caseInsensitiveCompare:string] == NSOrderedSame;
}

+ (NSString*)stringWithBundledJsonNamed:(NSString*)fileName
{
    NSError *error = nil;
    NSString *jsonText = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    if (path) {
        jsonText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"error reading JSON from file: %@", [error localizedDescription]);
        }
    }
    return jsonText;
}

+ (NSString*)stringWithBundledHTMLNamed:(NSString*)fileName
{
    NSError *error = nil;
    NSString *jsonText = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"html"];
    if (path) {
        jsonText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"error reading HTML from file: %@", [error localizedDescription]);
        }
    }
    return jsonText;
}

- (BOOL)containsSubstring:(NSString*)substring
{
    NSRange range = [self rangeOfString:substring options:NSCaseInsensitiveSearch];
    BOOL found = (range.location != NSNotFound);
    return found;
}


+ (NSData *)dataFromHexString:(NSString *)string
{
    string = [string lowercaseString];
    NSMutableData *data= [NSMutableData new];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i = 0;
    int length = string.length;
    while (i < length-1) {
        char c = [string characterAtIndex:i++];
        if (c < '0' || (c > '9' && c < 'a') || c > 'f')
            continue;
        byte_chars[0] = c;
        byte_chars[1] = [string characterAtIndex:i++];
        whole_byte = strtol(byte_chars, NULL, 16);
        [data appendBytes:&whole_byte length:1];
        
    }
    
    return data;
}

+ (NSString *)hexStringFromData:(NSData *)data
{
    
    NSUInteger bytesToConvert = [data length];
    const unsigned char *uuidBytes = [data bytes];
    NSMutableString *hexString = [NSMutableString stringWithCapacity:16];
    
    for (NSUInteger currentByteIndex = 0; currentByteIndex < bytesToConvert; currentByteIndex++)
    {
        switch (currentByteIndex)
        {
            case 3:
            case 5:
            case 7:
            case 9:[hexString appendFormat:@"%02x-", uuidBytes[currentByteIndex]]; break;
            default:[hexString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
        }
        
    }
    return hexString;
}

@end
