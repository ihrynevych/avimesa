//
//  Created by Pavlo Yonak on 8/7/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (EnumView)
+ (void)startInstanceMonitor;
+ (void)stopInstanceMonitor;
+ (void)dismissAll;
+ (BOOL)isAnyVisible;
@end
