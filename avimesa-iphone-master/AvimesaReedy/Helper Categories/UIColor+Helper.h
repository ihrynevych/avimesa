//
//  UIColor+Helper.h


#import <UIKit/UIKit.h>

@interface UIColor (Helper)

+ (UIColor*)colorFromHexString:(NSString *)hexString;

+ (UIColor*)reedyListCellNameOpenColor;
+ (UIColor*)reedyListCellNameClosedColor;
@end
