//
//  UIImage+Helper.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 12/23/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helper)
+ (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original;
@end
