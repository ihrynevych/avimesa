//
//  UIImage+Helper.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 12/23/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "UIImage+Helper.h"

@implementation UIImage (Helper)
+ (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:original];
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(cornerRadius, cornerRadius), NO, 1.0);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0f, 0.0f, cornerRadius, cornerRadius)
                                cornerRadius:cornerRadius] addClip];
    // Draw your image
    [original drawInRect:CGRectMake(0.0f, 0.0f, cornerRadius, cornerRadius)];
    
    // Get the image, here setting the UIImageView image
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return imageView.image;
}
@end
