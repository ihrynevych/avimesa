//
//  UITextField+TextPadding.h
//  ACWIS.Alt
//
//  Created by Pavlo Yonak on 6/6/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (TextPadding)

-(void) setLeftPadding:(int) paddingValue;
-(void) setRightPadding:(int) paddingValue;

@end
