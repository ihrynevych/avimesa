//
//  UITextField+TextPadding.m
//  ACWIS.Alt
//
//  Created by Pavlo Yonak on 6/6/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "UITextField+TextPadding.h"

@implementation UITextField (TextPadding)

-(void) setLeftPadding:(int) paddingValue
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
    paddingView.backgroundColor = [UIColor clearColor];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void) setRightPadding:(int) paddingValue
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
    paddingView.backgroundColor = [UIColor clearColor];
    self.rightView = paddingView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
