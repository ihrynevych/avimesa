//
//  ARHubSetupContainer.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 1/30/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARHubSetupContainer : UIViewController

-(void)updateSetupView;

@end
