//
//  ARHubSetupContainer.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 1/30/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARHubSetupContainer.h"
#import "ARDeviceManager.h"
#import "ARReedyListViewController.h"
static NSString * HubSetupToReedySetupSegue = @"HubSetupToReedySetup";

@interface ARHubSetupContainer()

@property (weak, nonatomic) IBOutlet UILabel *topInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *addHubButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@end


@implementation ARHubSetupContainer

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_topInfoLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15.0]];
    [_bottomInfoLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:11.0]];
    [_addHubButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Light" size:24.0]];
    [_nextButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18.0]];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void)updateSetupView
{
    if ([[ARDeviceManager sharedManager] isExistsHub]) {
        self.nextButton.enabled = true;
        if ([self.navigationController.viewControllers count] == 1)
        {
            [self performSegueWithIdentifier:HubSetupToReedySetupSegue sender:self];
        }
    }else
    {
        self.nextButton.enabled = true;
        if ([self.navigationController.viewControllers count] > 1) {
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
}

#pragma mark -

- (IBAction)nextButtonAction:(UIButton *)sender
{
    id serchObj = self;
    while (serchObj)
    {
        serchObj = [serchObj parentViewController];
        if ([serchObj isKindOfClass:[ARReedyListViewController class]])
        {
            [(ARReedyListViewController*)serchObj hubStup:self];
            break;
        }
    }
}

- (IBAction)addHubAction:(id)sender {
    id serchObj = self;
    while (serchObj)
    {
        serchObj = [serchObj parentViewController];
        if ([serchObj isKindOfClass:[ARReedyListViewController class]])
        {
            [(ARReedyListViewController*)serchObj hubStup:self];
            break;
        }
    }
    
}


@end
