//
//  ARHubListViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 11/10/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARHubListViewController.h"
#import "ARAppDelegate.h"
#import "NSManagedObjectContext+Helper.h"
#import "ARCoreDataStack.h"
#import "ARHubSelectionCell.h"
#import "Hub.h"
#import "ARDeviceManager.h"
#import "ARWifiListViewController.h"

@interface ARHubListViewController ()<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) Location *location;
@property (nonatomic,strong) Hub *selectedHub;

@end

@implementation ARHubListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.location = [[[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Location" predicate:nil sortKey:nil ascending:NO] lastObject];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mocDidSave) name:AR_MOC_DID_SAVE object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [NSFetchedResultsController deleteCacheWithName:@"hubslist"];
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateHubsWithCompletion:nil];
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"HubListToWifiSetup"]) {
         ARWifiListViewController* controller = [segue destinationViewController];
        controller.hubValidation = YES;
    }
    
}

#pragma mark - actions



#pragma mark - private

-(void)mocDidSave
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadData];
    });
}

-(void)reloadData
{
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    NSAssert(!error, @"error: %@", error);
    [self.tableView reloadData];
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSInteger locationId = [[ARDeviceManager sharedManager]  curentLocationId];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Hub"];
    if (locationId >0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id == %d",locationId];
        [request setPredicate:predicate];
    }
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:NO];
    [request setSortDescriptors:@[ sortDescriptor ]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext mainQContext] sectionNameKeyPath:nil cacheName:@"hubslist"];
    self.fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return [[self.fetchedResultsController sections] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController  sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ARHubSelectionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARHubSelectionCell"];
    
    Hub *hub = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.hub= hub;
    return cell;
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Hub * hub = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [self.fetchedResultsController.managedObjectContext deleteObject:hub];
        if (hub) {
            [[ARDeviceManager sharedManager] deleteHubWithID:hub.identifier callback:nil];
        }
        
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch((int)type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView endUpdates];
}

@end
