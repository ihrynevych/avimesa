//
//  ARHubSelectionCell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/25/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Hub;
@interface ARHubSelectionCell : UITableViewCell
@property (nonatomic,strong) Hub * hub;
@end
