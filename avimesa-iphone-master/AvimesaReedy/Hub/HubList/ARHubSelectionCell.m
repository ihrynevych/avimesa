//
//  ARHubSelectionCell.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/25/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARHubSelectionCell.h"
#import "Hub.h"


@interface ARHubSelectionCell()
@property (weak, nonatomic) IBOutlet UILabel *name;
@end

@implementation ARHubSelectionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setHub:(Hub *)hub
{
    self->_hub = hub;
    self.name.text=hub.name;
    
}
@end
