//
//  ARHubWifiInfoContainerViewController.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 6/4/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#warning This controller does not use Connection manager. Refactoring needed


#import <UIKit/UIKit.h>
@class ARWifiObject;
@interface ARHubWifiInfoContainerViewController : UIViewController
-(void)reloadViewWithWifiArray:(NSArray*)theWifiArray;
@property (strong, nonatomic)  ARWifiObject *selectedWifiObject;


@property (weak, nonatomic) IBOutlet UITextField *wifiPasswordTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *hubNameTextField;
@end
