//
//  ARHubWifiInfoContainerViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 6/4/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARHubWifiInfoContainerViewController.h"
#import "ARWifiObject.h"
#import "ARWifiContainerCell.h"
#import "UIColor+Helper.h"

@interface ARHubWifiInfoContainerViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSArray *wifiArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;


//wifi connectView
@property (weak, nonatomic) IBOutlet UIView *wifiConnectView;
@property (weak, nonatomic) IBOutlet UILabel *wifiNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;


@end

@implementation ARHubWifiInfoContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)saveAction:(UIButton *)sender {
    if (self.selectedWifiObject.protection.boolValue && ![self.wifiPasswordTextFiled.text length])
        return;
    self.wifiConnectView.hidden = YES;
    [self.wifiPasswordTextFiled resignFirstResponder];
    [self.tableView reloadData];
    [self.wifiPasswordTextFiled resignFirstResponder];
}

- (IBAction)cancelAction:(UIButton *)sender {
    self.wifiConnectView.hidden = YES;
    [self.wifiPasswordTextFiled resignFirstResponder];
    self.selectedWifiObject = nil;
    [self.tableView reloadData];
}

-(void)reloadViewWithWifiArray:(NSArray*)theWifiArray
{
    self.selectedWifiObject = nil;
    wifiArray = theWifiArray;
    [self.tableView reloadData];
    
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [wifiArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ARWifiContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARWifiContainerCell"];
    ARWifiObject * wifiObject= [wifiArray objectAtIndex:indexPath.row];
    if (self.selectedWifiObject && [self.selectedWifiObject.ssid isEqualToString:wifiObject.ssid]) {
        cell.name.textColor = [UIColor colorFromHexString:@"#1d94b3"];
    }else {
        cell.name.textColor = [UIColor colorFromHexString:@"#aeaeae"];
    }
    cell.name.text =wifiObject.ssid;
    if (wifiObject.protection.boolValue) {
        cell.lockImage.hidden = false;
    }else {
        cell.lockImage.hidden = true;
    }
    [cell updateSignalValue:wifiObject.signalStrength.integerValue];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedWifiObject = [wifiArray objectAtIndex:indexPath.row];;
    self.wifiConnectView.hidden = NO;
    self.wifiNameLabel.text = self.selectedWifiObject.ssid;
    
    if (![self.selectedWifiObject.protection boolValue]) {
        self.wifiPasswordTextFiled.hidden = YES;
    }
    else
    {
        self.wifiPasswordTextFiled.hidden = NO;
    }
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    [textField resignFirstResponder];
    
    return YES;
}


@end
