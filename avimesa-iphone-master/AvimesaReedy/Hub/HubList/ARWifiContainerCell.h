//
//  ARWifiContainerCell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 6/4/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARWifiContainerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel * name;
@property (weak, nonatomic) IBOutlet UIImageView * lockImage;

-(void)updateSignalValue:(NSInteger)signalValue;

@end
