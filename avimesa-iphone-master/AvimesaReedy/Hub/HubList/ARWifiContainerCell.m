//
//  ARWifiContainerCell.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 6/4/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARWifiContainerCell.h"
@interface ARWifiContainerCell ()

@property (weak, nonatomic) IBOutlet UIImageView * signalImage;

@end
@implementation ARWifiContainerCell



- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)updateSignalValue:(NSInteger)signalValue
{
    if (signalValue < 3){
        self.signalImage.image = [UIImage imageNamed:@"wi_fi_1_icon"];
    }else if (signalValue < 33){
        self.signalImage.image = [UIImage imageNamed:@"wi_fi_2_icon"];
        
    }else if (signalValue < 67){
        self.signalImage.image = [UIImage imageNamed:@"wi_fi_3_icon"];
        
    }else {
        self.signalImage.image = [UIImage imageNamed:@"wi_fi_4_icon"];
    }
    
}
@end
