//
//  ARHubViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 6/4/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARHubViewController.h"
#import "ARPeripheralConnectionsManager.h"
#import "ARAlertManager.h"
#import "ARHubWifiInfoContainerViewController.h"
#import "ARDeviceManager.h"
#import "ARWifiObject.h"
@interface ARHubViewController ()<ARPeripheralConnectionsManagerObserver>{
    int percentProgressValue;
    BOOL isFirstLoad;
    BOOL isHubCreated;
}

@property (weak, nonatomic) ARHubWifiInfoContainerViewController *wifiContainer;

@property (weak, nonatomic) IBOutlet UIButton *addHubButton;
@property (weak, nonatomic) IBOutlet UIView *wifiInfoContainerView;
@property (weak, nonatomic) IBOutlet UIView *progressContainerView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (strong, nonatomic) id tempHubID;

//progress view  Type Reedy Name
//progress view components
@property (nonatomic,weak) IBOutlet UIView * progressBorderView;
@property (nonatomic,weak) IBOutlet UIView * progressBackgroundView;
@property (nonatomic,weak) IBOutlet UIView * progressFillView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressLeadingSpaceConstraint;

@end

@implementation ARHubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupProgress];
    self.navigationItem.titleView = [ARSideMenuController logo];;
    // Do any additional setup after loading the view.
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(back:)];
    backButtonItem.title = @"Back";
    [self.navigationItem setLeftBarButtonItem:backButtonItem];
    self.addHubButton.hidden = true;
    self.wifiInfoContainerView.hidden = true;
    self.progressContainerView.hidden = false;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    [[ARPeripheralConnectionsManager sharedManager] stopAllConnections];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setProgressValue:0];
    if (!isFirstLoad) {
        [[ARPeripheralConnectionsManager sharedManager] registerManagerStateObserver:self];
        [[ARPeripheralConnectionsManager sharedManager] startHubConnection];
        
        [[ARAlertManager sharedManager] showGreenAlertWithText:@"Searching for Hub"];
        self.topLabel.text = @"Searching for Hub";
        isFirstLoad = true;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [[ARPeripheralConnectionsManager sharedManager] unregisterManagerStateObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateProgressView];
}
- (IBAction)addHubAction:(id)sender {
    if (!self.wifiContainer.hubNameTextField.text||![self.wifiContainer.hubNameTextField.text length]){
        [[ARAlertManager sharedManager] showRedAlertWithText:@"Type Hub Name"];
        return;
    }
    self.addHubButton.enabled = false;
    [[ARAlertManager sharedManager] showGreenAlertWithText:@"Creating a new hub"];
    NSInteger locationID = [[ARDeviceManager sharedManager] curentLocationId];
    [[ARDeviceManager sharedManager] createHubWithName:self.wifiContainer.hubNameTextField.text  locationID:@(locationID) callback:^(BOOL isSucceess, id identifier, NSString *uuid) {
        if (isSucceess && uuid) {
            isHubCreated = false;
            self.tempHubID = identifier;
            self.topLabel.text = @"";
            [[ARPeripheralConnectionsManager sharedManager] updateHubWithUUID:uuid];
            self.addHubButton.hidden = true;
            self.wifiInfoContainerView.hidden = true;
            self.progressContainerView.hidden = false;
        }
        else
        {
            self.topLabel.text = @"Type Hub Name";
            self.addHubButton.enabled = true;
            [[ARAlertManager sharedManager] showRedAlertWithText:@"Ups... Something went wrong"];
        }
    }];

}
-(void)setupProgress{
    
    // rounded corners
    self.progressBackgroundView.layer.cornerRadius = self.progressBackgroundView.bounds.size.height/2;
    self.progressBorderView.layer.cornerRadius = self.progressBorderView.bounds.size.height/2;
    self.progressFillView.layer.cornerRadius = self.progressFillView.bounds.size.height/2;
}
-(void)updateProgressView
{
    //calulate max width
    CGFloat maxWidth = self.progressBorderView.bounds.size.width - 2*self.progressLeadingSpaceConstraint.constant;
    
    //calculate progress value width
    CGFloat progressWidth;
    if (percentProgressValue >=100)
        progressWidth = maxWidth;
    else if (percentProgressValue<=0)
        progressWidth = 0.0f;
    else
        progressWidth = maxWidth*percentProgressValue/100;
    
    //update progress
    self.progressWidthConstraint.constant = progressWidth;
    
}
-(void)setProgressValue:(int)progressValue
{
    percentProgressValue = progressValue;
    [self updateProgressView];
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"HubVCToWifiContainer"]) {
        
        self.wifiContainer = segue.destinationViewController;
        
    }
    
}
-(void)connectionTimeout{
    if (isHubCreated && self.tempHubID){
        isHubCreated = false;
        [[ARDeviceManager sharedManager] deleteHubWithID:self.tempHubID callback:nil];
    }
    [[ARAlertManager sharedManager] showRedAlertWithText:@"Connection timeout"];
    self.addHubButton.hidden = true;
    self.wifiInfoContainerView.hidden = true;
    self.progressContainerView.hidden = true;
    self.topLabel.text = @"";
    
}
-(void)connectionFail{
    if (isHubCreated && self.tempHubID){
        isHubCreated = false;
        [[ARDeviceManager sharedManager] deleteHubWithID:self.tempHubID callback:nil];
    }
    [[ARAlertManager sharedManager] showRedAlertWithText:@"Connection did fail"];
    self.addHubButton.hidden = true;
    self.wifiInfoContainerView.hidden = true;
    self.progressContainerView.hidden = true;
    self.topLabel.text = @"";
}

-(void)progressDidChangeWithValue:(int)progressValue{
    [self setProgressValue:progressValue];
    
}

-(void)didConnectToReedyWithUDID:(NSString*)reedyUDID{
    
    
}

-(void)didUpdateReedy{
    
}

-(void)reedyUpdateFailed{
    
}
-(void)didConnectToHubWithUUID:(NSString *)reedyUUID wifiArray:(NSArray *)wifiArray
{
    [[ARAlertManager sharedManager] showGreenAlertWithText:@"Successfully connected to the Hub"];
    self.addHubButton.hidden = false;
    self.wifiInfoContainerView.hidden = false;
    self.progressContainerView.hidden = true;
    self.topLabel.text = @"Type Hub Name";
    [self.wifiContainer reloadViewWithWifiArray:wifiArray];
    
}
-(void)didUpdateHubUUID{
    [[ARAlertManager sharedManager] showGreenAlertWithText:@"Hub Successfully Added"];
    if (self.wifiContainer.selectedWifiObject){
        [[ARAlertManager sharedManager] showGreenAlertWithText:@"Setting up Wifi"];
        [[ARPeripheralConnectionsManager sharedManager] updateHubWifiWithSSID:self.wifiContainer.selectedWifiObject.ssid password:self.wifiContainer.selectedWifiObject.protection.boolValue?self.wifiContainer.wifiPasswordTextFiled.text:@""];
    }
    else {
        self.tempHubID = nil;
        isHubCreated = true;
        [self back:nil];}
    
}
-(void)didUpdateHubWifi{
    [[ARAlertManager sharedManager] showGreenAlertWithText:@"Wifi was set up successfully"];
    isHubCreated = true;
    self.tempHubID = nil;
    [self back:nil];
    
}

-(void)hubUUIDUpdateFailed{
    
}

-(void)hubWifiUpdateFailed{
    
}



@end
