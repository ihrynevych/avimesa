//
//  ARWifiCell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 11/7/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ARWifiObject;
@interface ARWifiCell : UITableViewCell
@property (nonatomic,strong) ARWifiObject * wifiObject;
@end
