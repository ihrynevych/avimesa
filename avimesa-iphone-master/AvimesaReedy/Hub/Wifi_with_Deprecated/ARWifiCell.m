//
//  ARWifiCell.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 11/7/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARWifiCell.h"
#import "ARWifiObject.h"


@interface ARWifiCell ()

@property (weak, nonatomic) IBOutlet UILabel * ssid;
@property (weak, nonatomic) IBOutlet UILabel * protection;
@property (weak, nonatomic) IBOutlet UILabel * signalStrength;

@end

@implementation ARWifiCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setWifiObject:(ARWifiObject *)wifiObject
{
    self->_wifiObject = wifiObject;
    self.protection.hidden = YES;
    self.ssid.text = @"";
    self.signalStrength.text = @"";
    if (wifiObject.ssid) {
        self.ssid.text = wifiObject.ssid;
    }
    if (wifiObject.protection.boolValue) {
        self.protection.hidden = NO;
    }
    if (wifiObject.signalStrength) {
        self.signalStrength.text = [NSString stringWithFormat:@"%ld%%",wifiObject.signalStrength.integerValue];
    }
}
@end
