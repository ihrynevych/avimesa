//
//  ARWifiListViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 11/7/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARWifiListViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "ARWifiCell.h"
#import "ARWifiObject.h"
#import "NSString+Helper.h"
#import "NSManagedObjectContext+Helper.h"

@interface ARWifiListViewController ()< CBCentralManagerDelegate, CBPeripheralDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSMutableArray *wifiArray;
    NSMutableData *wifiBuffer;
    NSTimer * connectTimer;
    NSTimer * timeoutTimer;
    BOOL connecting;
    BOOL gettingWifiList;
    BOOL dataIsSending;
    NSInteger lastSendIndex;
    NSData * sendData;
    NSData * bufferToSend;
    NSArray * hubsArray;
}

@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UILabel *wifiSsidLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIView *connectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) ARWifiObject *selectedWifiObject;

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *connectedPeripheral;
@property (strong, nonatomic) CBCharacteristic *wifiCharacteristic;
@property (strong, nonatomic) CBCharacteristic *uuidCharacteristic;
@property (strong, nonatomic) CBCharacteristic *wifiListCharacteristic;


@property (weak, nonatomic) IBOutlet UIView *activityContainer;
@property (weak, nonatomic) IBOutlet ARActivityViewController *activityViewController;

@end

@implementation ARWifiListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(back:)];
    backButtonItem.title = @"Back";
    [self.navigationItem setLeftBarButtonItem:backButtonItem];
    //all hubs
    hubsArray = [[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Hub" predicate:nil sortKey:nil ascending:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    

    [self hideActivityView:NO];
    [self setActivityProgress:0];
    [self _initTimeoutTimer];
    [self setActivityText:@"Scanning for Hubs."];
    
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self hideActivityView:YES];
    [self setActivityProgress:0];
    
    [self cancelCentralConnections];
    [self _cancelTimeoutTimer];
    [self _cancelTimer];
}

-(void)cancelCentralConnections
{
    if (_centralManager) {
        [_centralManager stopScan];
        if (_connectedPeripheral) {
            [_centralManager cancelPeripheralConnection:_connectedPeripheral];
            _connectedPeripheral = nil;
        }
        _centralManager.delegate = nil;
        _centralManager = nil;
    }
}

-(void)dealloc
{
    [self cancelCentralConnections];
    [self _cancelTimeoutTimer];
    [self _cancelTimer];
}

#pragma mark - navigation

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"WifiScanToActivityController"]) {
        self.activityViewController = [segue destinationViewController];
    }
    
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - Actions

- (IBAction)connectAction:(id)sender {
    if (self.selectedWifiObject.protection.boolValue && ![self.passwordTextField.text length])
        return;
    [self hideActivityView:NO];
    [self setActivityProgress:0];
    [self _initTimeoutTimer];
    [self setActivityText:@"Sending data to Hub"];
    
    [self sendSSID:self.selectedWifiObject.ssid password:self.passwordTextField.text];
    [self.passwordTextField resignFirstResponder];
}

- (IBAction)cancelAction:(id)sender
{
    self.connectionView.hidden = YES;
    [self.passwordTextField resignFirstResponder];
}

#pragma mark - private

-(void)refreshWifiList
{

    wifiArray = [NSMutableArray array];
    NSArray * components = [self componentsFromData :wifiBuffer separatedBySign:@"00"];
    for (int i = 0; i< [components count]; i+=3) {
        ARWifiObject * wifiObject= [[ARWifiObject alloc] init];
        wifiObject.ssid = [[NSString alloc] initWithData:components[i] encoding:NSUTF8StringEncoding];
        if (i+1 < [components count]) {
            wifiObject.protection = @([[[NSString alloc] initWithData:components[i+1] encoding:NSUTF8StringEncoding] boolValue]);
        }
        if (i+2 < [components count]) {
            wifiObject.signalStrength = @([[[NSString alloc] initWithData:components[i+2] encoding:NSUTF8StringEncoding] integerValue]);
        }
        [wifiArray addObject:wifiObject];
    }
    [self.tableView reloadData];
}

-(void)sendSSID:(NSString *)ssid password:(NSString *)password
{
    NSString * ssidDataInterpretation = [ssid length] ? [self hexStringFromData:[ssid dataUsingEncoding:NSUTF8StringEncoding] fromated:NO]:@"";
    NSString * passwordDataInterpretation = [password length] ? [self hexStringFromData:[password dataUsingEncoding:NSUTF8StringEncoding] fromated:NO]:@"";
    NSString * outputString = [NSString stringWithFormat:@"%@00%@00",ssidDataInterpretation,passwordDataInterpretation];
    if (_wifiCharacteristic&&_connectedPeripheral) {
        lastSendIndex = 0;
        sendData = [NSString dataFromHexString:outputString];
        if ([sendData length]>20) {
            bufferToSend = [sendData subdataWithRange:NSMakeRange(0, 20)];
            lastSendIndex = 19;
        }
        else
        {
            lastSendIndex = [sendData length]-1;
            bufferToSend= sendData;
        }
        
        [_connectedPeripheral writeValue:bufferToSend forCharacteristic:_wifiCharacteristic type:CBCharacteristicWriteWithResponse];
        
        dataIsSending = YES;
    }else
    {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                             message:@"Something went wrong. Refresh wifi list"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
    }
    
}

-(NSArray *)componentsFromData :(NSData*)data separatedBySign:(NSString *)sign
{
    NSString * hexString = [self unformatedHexStringFromData:data];
    NSArray * array = [hexString componentsSeparatedByString:sign];
    NSMutableArray * result = [NSMutableArray array];
    for (NSString *stringData in array) {
        if(stringData && [stringData length])
        {
            [result addObject:[NSString dataFromHexString:stringData]];
        }
    }
    return result;
}

-(BOOL)dataContains :(NSData*)data sign:(NSString *)sign
{
    NSString * hexString = [self unformatedHexStringFromData:data];
    NSRange range =[hexString rangeOfString:sign];
    if (range.location == NSNotFound) {
        return NO;
    }
    else
        return YES;
    
}

- (NSString *)unformatedHexStringFromData:(NSData *)data {
    
    return [self hexStringFromData:data fromated:NO];
}

- (NSString *)hexStringFromData:(NSData *)data fromated:(BOOL)fromated{
    
    NSUInteger bytesToConvert = [data length];
    const unsigned char *uuidBytes = [data bytes];
    NSMutableString *hexString = [NSMutableString stringWithCapacity:16];
    
    for (NSUInteger currentByteIndex = 0; currentByteIndex < bytesToConvert; currentByteIndex++)
    {
        if (fromated)
        {
            switch (currentByteIndex)
            {
                case 3:
                case 5:
                case 7:
                case 9:[hexString appendFormat:@"%02x-", uuidBytes[currentByteIndex]]; break;
                default:[hexString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
            }
        }
        else
        {
            [hexString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
        }
        
        
    }
    return hexString;
}

-(void)callToWifiCharacteristic
{
    if (_wifiListCharacteristic && _connectedPeripheral) {
        unsigned char mydata = 1;
        [_connectedPeripheral writeValue:[NSMutableData dataWithBytes:&mydata length:sizeof(mydata)] forCharacteristic:_wifiListCharacteristic type:CBCharacteristicWriteWithResponse];
    }
    
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if (central.state != CBCentralManagerStatePoweredOn) {
        return;
    }
    
    [self setActivityProgress:16];
    [self _resetTimeoutTimer];
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        // Scan for devices
        [_centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:HUB_SERVICE_UUID]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
    if ([RSSI integerValue]<0 && [RSSI integerValue]>(-50))
    {
        
        [self setActivityProgress:32];
        [self _resetTimeoutTimer];
        
        _connectedPeripheral = peripheral;
        _connectedPeripheral.delegate = self;
        [_centralManager connectPeripheral:peripheral options:nil];
        [_centralManager stopScan];
        
    }
    
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [self _initTimer];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    if (connectTimer) {
        [self _cancelTimer];
        [_centralManager connectPeripheral:peripheral options:nil];
    }
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (error) {
        return;
    }
    
    [self setActivityProgress:48];
    [self _resetTimeoutTimer];
    
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:[NSArray arrayWithObjects:[CBUUID UUIDWithString:HUB_WIFI_SETTING_CHARACTERISTIC_UUID],[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID],[CBUUID UUIDWithString:HUB_ID_CHARACTERISTIC_UUID], nil] forService:service];
    }
    NSLog(@"");
    // Discover other characteristics
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if (error) {
        return;
    }
    
    [self setActivityProgress:64];
    [self _resetTimeoutTimer];
    
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_ID_CHARACTERISTIC_UUID]])
        {
            _uuidCharacteristic =characteristic;
            [peripheral readValueForCharacteristic:characteristic];
            
        }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_SETTING_CHARACTERISTIC_UUID]])
        {
            _wifiCharacteristic =characteristic;
            
        }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID]])
        {
            _wifiListCharacteristic =characteristic;
//            unsigned char mydata = 1;
//            _wifiListCharacteristic =characteristic;
//            [peripheral writeValue:[NSMutableData dataWithBytes:&mydata length:sizeof(mydata)] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
            
        }
    }
    
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        NSLog(@"Error");
        return;
    }
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_ID_CHARACTERISTIC_UUID]])
    {
        [self setActivityProgress:80];
        [self _cancelTimeoutTimer];
        
        
        if (self.hubValidation) {
            if ([[hubsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"uuid == %@",[NSString hexStringFromData:characteristic.value]]] count]) {
                [self callToWifiCharacteristic];
                
            }else
            {
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"This HUB cannot be configured.\n Please make sure it was added to your list of HUBs." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alertView show];
                
                [self _cancelTimeoutTimer];
                [self cancelCentralConnections];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else
            [self callToWifiCharacteristic];
        
        
    }
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID]])
    {
        
        if ([characteristic.value length] <= 1) {
            if ([wifiBuffer length]) {
                [self setActivityProgress:99];
                [self _cancelTimeoutTimer];
                
                [self hideActivityView:YES];
                
                [wifiBuffer appendData:characteristic.value];
                [self refreshWifiList];
            }
        }
        else
        {
            if ([self dataContains:characteristic.value sign:@"0000"])
            {
                if ([[self componentsFromData :characteristic.value separatedBySign:@"0000"] count]) {
                    [wifiBuffer appendData:[self componentsFromData :characteristic.value separatedBySign:@"0000"][0]];
                }
                
                [self setActivityProgress:99];
                [self _cancelTimeoutTimer];
                
                [self hideActivityView:YES];
                [self refreshWifiList];
            }else
            {
                [self _resetTimeoutTimer];
                [wifiBuffer appendData:characteristic.value];
                [peripheral readValueForCharacteristic:characteristic];
            }
        }
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error");
        return;
    }
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID]])
    {
        if (!wifiBuffer)
        {
            wifiBuffer = nil;
        }
        wifiBuffer = [NSMutableData data];
        [peripheral readValueForCharacteristic:characteristic];
        
        [self setActivityProgress:84];
        [self _resetTimeoutTimer];
        
        [self setActivityText:@"Load wifi list"];
    }
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_SETTING_CHARACTERISTIC_UUID]])
    {
        if(dataIsSending)
        {
            NSInteger  dataLength = [sendData length];
            [self setActivityProgress:(int)(lastSendIndex*100/dataLength)];
            [self _resetTimeoutTimer];
            if (dataLength - lastSendIndex - 1 > 0) {
                if (dataLength - lastSendIndex - 1 > 20)
                {
                    bufferToSend = [sendData subdataWithRange:NSMakeRange(lastSendIndex+1, 20)];
                    lastSendIndex += 20;
                }
                else
                {
                    NSInteger tempLength = dataLength - lastSendIndex -1;
                    bufferToSend = [sendData subdataWithRange:NSMakeRange(lastSendIndex+1, tempLength)];
                    lastSendIndex += tempLength;
                }
                [_connectedPeripheral writeValue:bufferToSend forCharacteristic:_wifiCharacteristic type:CBCharacteristicWriteWithResponse];
            }
            else
            {
                [self _cancelTimeoutTimer];
                dataIsSending = NO;
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                     message:@"WiFi network is connected"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles: nil] ;
                [alertView show];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [wifiArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ARWifiCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARWifiCell"];
    cell.wifiObject= [wifiArray objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedWifiObject = [wifiArray objectAtIndex:indexPath.row];;
    self.connectionView.hidden = NO;
    self.wifiSsidLabel.text = self.selectedWifiObject.ssid;
    
    if (![self.selectedWifiObject.protection boolValue]) {
        self.passwordTextField.hidden = YES;
    }
    else
    {
        self.passwordTextField.hidden = NO;
    }
}



#pragma mark - timers

-(void)_cancelTimeoutTimer
{
    if (timeoutTimer)
    {
        [timeoutTimer invalidate];
        timeoutTimer = nil;
    }
}

-(void)_initTimeoutTimer
{
    timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(handleTimeout) userInfo:nil repeats:NO];
}

-(void)_resetTimeoutTimer
{
    [self _cancelTimeoutTimer];
    [self _initTimeoutTimer];
    
}
-(void)handleTimeout
{
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Scanning timeout"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil] ;
    [alertView show];
    [self cancelCentralConnections];
    [self hideActivityView:YES];
    
}

-(void)_cancelTimer
{
    if (connectTimer)
    {
        [connectTimer invalidate];
        connectTimer = nil;
    }
}

-(void)_initTimer
{
    connectTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(discoverServ) userInfo:nil repeats:NO];
}

-(void)discoverServ
{
    [self setActivityProgress:48];
    [self _resetTimeoutTimer];
    
    [_connectedPeripheral discoverServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:HUB_SERVICE_UUID]]];
}


#pragma mark - Activity view controller

-(void)hideActivityView:(BOOL)hidden
{
    [self.activityContainer setHidden:hidden];
}

-(void)setActivityProgress:(int)progress
{
    if (self.activityViewController) {
        [self.activityViewController setProgressValue:progress];
    }
}
-(void)setActivityText:(NSString*)text
{
    if (self.activityViewController) {
        self.activityViewController.informationLabel.text = text;
    }
}


@end
