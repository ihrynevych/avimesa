//
//  ARWifiObject.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 11/7/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARWifiObject : NSObject

@property (nonatomic,strong) NSString * ssid;
@property (nonatomic,strong) NSNumber * protection;
@property (nonatomic,strong) NSNumber * signalStrength;
-(id)init;
@end
