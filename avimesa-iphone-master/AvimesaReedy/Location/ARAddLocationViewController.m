//
//  ARAddLocationViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARAddLocationViewController.h"
#import "ARDeviceManager.h"

@interface ARAddLocationViewController ()
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UILabel *mainTextLabel;
@end

@implementation ARAddLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [ARSideMenuController logo];;
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(back:)];
    backButtonItem.title = @"Back";
    [self.navigationItem setLeftBarButtonItem:backButtonItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (IBAction)back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)addAction:(id)sender
{
    self.addButton.enabled = false;
    [self createLocationWithCallback:^(BOOL isSucceess, id identifier) {
        if (isSucceess) {
            [self back:nil];
        }
        self.addButton.enabled = true;
    }];
}
@end
