//
//  ARLocationViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARChangeLocationViewController.h"
#import "Location.h"
#import "ARLocationCell.h"
#import "ARDeviceManager.h"
#import "NSManagedObjectContext+Helper.h"
#import "ARAppDelegate.h"

@interface ARChangeLocationViewController ()<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>
{
    NSInteger  selectedLocationIndex;
    BOOL isVisible;
    
}


@property (weak, nonatomic) IBOutlet UILabel *selectedLocationName;
@property (nonatomic,weak)IBOutlet UITableView* tableView;
@property (nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) Location *selectedLocation;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@end

@implementation ARChangeLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    isVisible = YES;
    [super viewWillAppear:animated];
    
    //clear fetch cache
    [NSFetchedResultsController deleteCacheWithName:@"Locations"];
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    //update locations
    [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateLocationsWithCompletion:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            selectedLocationIndex = [[ARDeviceManager sharedManager] curentLocationId];
            [self reloadData];
        });
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    isVisible = NO;
    
    [super viewWillDisappear:animated];
    [self.tableView setEditing:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)reloadData
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    //sort by id
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:NO];
    [request setSortDescriptors:@[ sortDescriptor ]];
    
    //init fetch result controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext mainQContext] sectionNameKeyPath:nil cacheName:@"Locations"];
    NSError *error;
    
    //perform fetch
    [self.fetchedResultsController performFetch:&error];
    self.fetchedResultsController.delegate = self;
    NSAssert(!error, @"error: %@", error);
    [self.tableView reloadData];
}

#pragma mark - actions

- (IBAction)save:(id)sender {
    if (self.selectedLocation) {
        [[ARDeviceManager sharedManager] switchToLocationWithId:self.selectedLocation.identifier.integerValue];
        self.saveButton.enabled = false;
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Location saved successfully"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
    }
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController  sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ARLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARLocationCell"];
    
    Location *location = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (location.identifier.integerValue == selectedLocationIndex) {
        self.selectedLocationName.text = [location.name uppercaseString];
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    cell.location= location;
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedLocation = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (![[self.selectedLocationName.text uppercaseString] isEqualToString:[self.selectedLocation.name uppercaseString]]) {
        self.saveButton.enabled = true;
    }
    self.selectedLocationName.text = [self.selectedLocation.name uppercaseString];
    selectedLocationIndex = self.selectedLocation.identifier.integerValue;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return isVisible;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([self tableView:tableView numberOfRowsInSection:0] > 1)
        {
            Location * location = [self.fetchedResultsController objectAtIndexPath:indexPath];
            [self.fetchedResultsController.managedObjectContext deleteObject:location];
            if (location) {
                [[ARDeviceManager sharedManager] deleteLocationWithID:location.identifier callback:nil];
            }
        }else
        {
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Location"
                                                                 message:@"You cannot delete all locations.\nAt least one should remain."
                                                                delegate:nil
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles: nil] ;
            [alertView show];
        }
        
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch((int)type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView endUpdates];
}
@end
