//
//  AREditLocationViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 12/23/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "AREditLocationViewController.h"
#import "Location.h"

@interface AREditLocationViewController ()

@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *mainTextLabel;

@end

@implementation AREditLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.nameTextField.text = [self.location.name uppercaseString];
    if (!selectedImage) {
        if ([self.location.image length]) {
    
            [self.imageView sd_setImageWithURL:[NSURL URLWithString: self.location.image] placeholderImage:nil options:SDWebImageRefreshCached];
        }else
        {
            [self.imageView setImage:[UIImage imageNamed:@"no_photo"]];
        }
        
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)saveAction:(id)sender {
    self.saveButton.enabled = false;
    [self updateLocationWithLocationId:self.location.identifier.integerValue callback:^(BOOL isSucceess, id identifier) {
        if (isSucceess) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                     message:@"Update saved successfully"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles: nil] ;
                [alertView show];});
        }
        self.saveButton.enabled = true;
    }];
}
@end
