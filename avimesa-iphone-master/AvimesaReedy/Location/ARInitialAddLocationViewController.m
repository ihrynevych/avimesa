//
//  ARInitialAddLocationViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 1/30/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARInitialAddLocationViewController.h"
#import "ARAppDelegate.h"

@interface ARInitialAddLocationViewController ()

@property (weak, nonatomic) IBOutlet UILabel *mainTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *topTextFirstLine;
@property (weak, nonatomic) IBOutlet UILabel *topTextSecondLine;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@end

@implementation ARInitialAddLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)nextButtonAction:(id)sender {
    
    [self createLocationWithCallback:^(BOOL isSucceess, id identifier) {
        if (isSucceess) {
            [self loadLocations];
        }
    }];
}
- (void)loadLocations
{
    [self hideActivityView:NO];
    [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateAllWithCompletion:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideActivityView:YES];
            if(success) {[[ARDeviceManager sharedManager] subscribeToSNS];}
            [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeMain];
        });
    }];
    
}
@end
