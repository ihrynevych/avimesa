//
//  ARBaseLocationViewController.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 2/3/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARDeviceManager.h"

@interface ARBaseLocationViewController : UIViewController
{
    UIImage * selectedImage;
}

//change name elements
@property (weak, nonatomic) IBOutlet UILabel *nameInfoLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIButton *changeNameButton;

//add photo elements
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *addPhoto;

//activity indicator
@property (weak, nonatomic) IBOutlet UIView *activityView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * activityBottomConstraint;


//update location
-(void)updateLocationWithLocationId:(NSInteger)locationId callback:(ARDeviceManagerCallback)callback;
//create location
-(void)createLocationWithCallback:(ARDeviceManagerCallback)callback;

-(void)hideActivityView:(BOOL)hidden;
@end
