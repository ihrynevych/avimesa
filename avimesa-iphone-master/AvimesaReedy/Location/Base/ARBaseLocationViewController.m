//
//  ARBaseLocationViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 2/3/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARBaseLocationViewController.h"
#import "Location.h"
#import "UIImage+Helper.h"

@interface ARBaseLocationViewController ()<UIActionSheetDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>


@end

@implementation ARBaseLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    // keyboard notifications
    [center addObserver:self
               selector:@selector(keyboardWillShow:)
                   name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - actions

- (IBAction)changeNameAction:(id)sender {
    [self.nameTextField becomeFirstResponder];
}

- (IBAction)addPhotoAction:(id)sender {
    [self.nameTextField resignFirstResponder];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Capture Type"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera",@"Photo Library", nil];
    [actionSheet showInView:self.view];
}

-(void)createLocationWithCallback:(ARDeviceManagerCallback)callback
{
    if (!self.nameTextField.text || [self.nameTextField.text length]<=0) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                             message:@"Please enter Location Name"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
        callback (false,nil);
        return;
    }
    [self hideActivityView:NO];
    [[ARDeviceManager sharedManager] createLocationWithName:self.nameTextField.text callback:^(BOOL isSucceess, id identifier) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideActivityView:YES];
            if (isSucceess) {
                if (selectedImage) {
                    //temp solution because dosent work on api side
                    [self updateLocationWithLocationId:[identifier integerValue] callback:callback];
                }
                else
                    callback(isSucceess,identifier);
                
            }else
                callback(isSucceess,identifier);
        });
    }];
    
}



-(void)updateLocationWithLocationId:(NSInteger)locationId callback:(ARDeviceManagerCallback)callback
{
    if (!self.nameTextField.text || [self.nameTextField.text length]<=0) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                             message:@"Please enter Location Name"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
        return;
    }
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
    
    if (self.nameTextField.text&&[self.nameTextField.text length]) {
        [dictionary setObject:self.nameTextField.text forKey:@"name"];
    }
    if (selectedImage) {
        [dictionary setObject:UIImagePNGRepresentation(selectedImage) forKey:@"image"];
    }
    [self hideActivityView:NO];
    [[ARDeviceManager sharedManager] updateLocationWithDictionary:dictionary locationId:locationId callback:^(BOOL isSucceess, id identifier) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideActivityView:YES];
            callback(isSucceess,identifier);
        });
    }];
    
}
//activity indicator
-(void)hideActivityView:(BOOL)hidden
{
    [UIView animateWithDuration:0.2f animations:^{
        self.activityView.alpha = hidden?0.0f:1.0f;
    }];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    if (editedImage) {
        //round image
        
        CGFloat  originalImageSizeValue = originalImage.size.width<originalImage.size.height ?  originalImage.size.width :originalImage.size.height ;
        CGFloat  editedImageSizeValue = editedImage.size.width<editedImage.size.height ?  editedImage.size.width :editedImage.size.height ;
        
        if (editedImageSizeValue/originalImageSizeValue>2.0f) {
            selectedImage =[UIImage imageWithRoundedCornersSize:originalImageSizeValue < 180.0f ? originalImageSizeValue : 180.0f usingImage:originalImage];
        }
        else
        {
            selectedImage =[UIImage imageWithRoundedCornersSize:editedImageSizeValue < 180.0f ? editedImageSizeValue : 180.0f usingImage:editedImage];
        }
        
    }
    if (selectedImage) {
        [self.imageView setImage:selectedImage];
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark - Keyboard Notification
- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    
    NSTimeInterval animationDuration = [[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue] << 16;
    CGRect keyboardRect = [[userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:animationDuration
                          delay:0.0 options:curve
                     animations:^{
                         self.activityBottomConstraint.constant = keyboardRect.size.height;
                         
                     }
                     completion:nil];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    //standart constraint value
    
    
    NSDictionary *userInfo = notification.userInfo;
    
    NSTimeInterval animationDuration = [[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue] << 16;
    
    [UIView animateWithDuration:animationDuration
                          delay:0.0 options:curve
                     animations:^{
                         
                         self.activityBottomConstraint.constant = 0.0f;
                         
                     }
                     completion:nil];
}
#pragma mark - UICollectionViewDataSource

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
        dispatch_async(dispatch_get_main_queue(),^{
            [self showImagePickerWithCaptureFromCamera:YES];
        });
    }
    else if (buttonIndex == 1)
    {
        
        dispatch_async(dispatch_get_main_queue(),^{
            [self showImagePickerWithCaptureFromCamera:NO];
        });
    }
}
- (void)showImagePickerWithCaptureFromCamera:(BOOL)cameraFlag
{
    //init piker
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    // capture type
    if (cameraFlag)
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    else
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    //show picker
    [self presentViewController:picker animated:YES completion:NULL];
}


@end
