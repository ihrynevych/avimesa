//
//  ARLocationCell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/16/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Location;
@interface ARLocationCell : UITableViewCell
@property (nonatomic,strong) Location * location;
@end
