//
//  ARLocationCell.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/16/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARLocationCell.h"
#import "Location.h"

@interface ARLocationCell ()

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
@implementation ARLocationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setLocation:(Location *)location
{
    self->_location = location;
    self.name.text = [location.name uppercaseString];
    if ([location.image length]) {
        [self.image sd_setImageWithURL:[NSURL URLWithString: location.image] placeholderImage:nil options:SDWebImageRefreshCached];
    }
    else
    {
        [self.image setImage:[UIImage imageNamed:@"no_photo"]];
        
    }
    
}

@end
