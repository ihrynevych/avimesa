//
//  ARLoginViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/17/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARLoginViewController.h"
#import "UITextField+TextPadding.h"
#import "ARLoginManager.h"
#import "ARAppDelegate.h"
#import "ARDeviceManager.h"
@interface ARLoginViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewTopSpase;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotButton;

@property (weak, nonatomic) IBOutlet UIView *activityView;

@end

@implementation ARLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set paddings for textfiels
    [self.userNameTextField setLeftPadding:10];
    [self.passwordTextField setLeftPadding:10];
    
    //custom fonts
    self.userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"User Name"
                                                                                   attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                                NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:17.0f]}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password"
                                                                                   attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                                NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:17.0f]}];
    self.loginButton.titleLabel.font =[UIFont fontWithName:@"Lato-Regular" size:17.0f];
    self.forgotButton.titleLabel.font =[UIFont fontWithName:@"Lato-Regular" size:12.0f];
    

    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    // keyboard notifications
    [center addObserver:self
               selector:@selector(keyboardWillShow:)
                   name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self
               selector:@selector(keyboardWillHide:)
                   name:UIKeyboardWillHideNotification object:nil];
    
    // sign up notification
//    [center addObserver:self
//               selector:@selector(signupDidFinish:)
//                   name:AR_SIGNUP_DID_FINISH object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //custom fonts
    self.userNameTextField.attributedText = [[NSAttributedString alloc] initWithString:@""
                                                                            attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                         NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:17.0f]}];
    self.passwordTextField.attributedText = [[NSAttributedString alloc] initWithString:@""
                                                                            attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                         NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:17.0f]}];
    [self hideActivityView:YES];
    
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - actions

- (IBAction)loginAction:(UIButton*)sender {
    
    //fields validation
    if (!self.userNameTextField.text || [self.userNameTextField.text length]<=0) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                             message:@"Please enter User Name"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
        return;
    }
    if (!self.passwordTextField.text || [self.passwordTextField.text length]<=0) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                             message:@"Please enter Password"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
        sender.enabled= true;
        return;
    }    
    [self hideActivityView:NO];
    sender.userInteractionEnabled = false;
    
    //perform login
    [[ARLoginManager sharedLoginManager] performLoginWithUserName:self.userNameTextField.text password:self.passwordTextField.text callback:^(BOOL isSucceess, NSDictionary *response, NSString *userID) {
        sender.userInteractionEnabled = true;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isSucceess&&userID) {
                // load locations
                [self loadLocations];
            }
            else{
                [self hideActivityView:YES];
            }
        });
        
        
    }];
}

#pragma mark - private

- (void)loadLocations
{
    [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateAllWithCompletion:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(success) {
                // subscribe to SNS topic
                [[ARDeviceManager sharedManager] subscribeToSNS];
            }
            
            [self hideActivityView:YES];
            
            // welcome or standart navigation
            if ([[ARDeviceManager sharedManager] isExistsLocation])
                [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeMain];
            else
                [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeWelcome];
        });
    }];
    
}

//error alert
-(void)showFailedLoginAlertForResponse:(NSDictionary*)response andError:(NSError*) error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* message = @"";
        if(response[@"ResponseStatus"])
        {
            message = response[@"ResponseStatus"][@"Message"];
        }else{
            message = [error localizedDescription];
        }
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Login failed"
                                                             message:message
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
    });
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 1) {
        [self.passwordTextField becomeFirstResponder];
    }else if (textField.tag == 2)
    {
        [textField resignFirstResponder];
        [self loginAction:nil];
    }
    return NO;
    
}

#pragma mark - Keyboard Notification
- (void)keyboardWillShow:(NSNotification *)notification {
    if (IS_IPHONE_4_OR_LESS) {
        // iphone 4 delta
        self.containerViewTopSpase.constant = 0;
    }
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    //standart constraint value
    self.containerViewTopSpase.constant = 92;
}

#pragma mark - Sign Up Notification
- (void)signupDidFinish:(NSNotification *)notification {
    //perform autologin after sign up
    NSDictionary * userInfo = notification.userInfo;
    if (userInfo[@"username"]&&userInfo[@"password"]) {
        [[ARLoginManager sharedLoginManager] performLoginWithUserName:userInfo[@"username"] password:userInfo[@"password"] callback:^(BOOL isSucceess, NSDictionary *response, NSString *userID) {
            if (isSucceess&&userID) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // load locations
                    [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeWelcome];
                });
            }
            
        }];
    }
}


-(void)hideActivityView:(BOOL)hidden
{
    [UIView animateWithDuration:0.2f animations:^{
        self.activityView.alpha = hidden?0.0f:1.0f;
    }];
}
@end
