//
//  ARAlertManager.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARAlertManager : NSObject

+ (ARAlertManager *)sharedManager;
-(void)showGreenAlertWithText:(NSString*)text;
-(void)showRedAlertWithText:(NSString*)text;
@end
