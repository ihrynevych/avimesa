//
//  ARAlertManager.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARAlertManager.h"

@interface ARAlertManager(){
    NSMutableArray * visibleViews;
}



@end

@implementation ARAlertManager

+ (ARAlertManager *)sharedManager
{
    static id _default = nil;
    static dispatch_once_t safer;
    dispatch_once(&safer, ^(void)
                  {
                      _default = [[[self class] alloc] init];
                  });
    return _default;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        visibleViews = @[].mutableCopy;
    }
    return self;
}

-(void)showRedAlertWithText:(NSString*)text
{
    if (visibleViews.count > 0) [self removeView:@(true)];
    if (text && text.length > 0)
    {
        UIView *currentView = [[UIView alloc] initWithFrame:CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, 64)];
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 44)];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:18];
        [currentView addSubview:label];
        label.text = text;
        currentView.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.90];
        
        UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
        [currentWindow addSubview:currentView];
        [visibleViews addObject:currentView];
        [UIView animateWithDuration:.3 animations:^{
            currentView.frame = CGRectMake(0, -0, [UIScreen mainScreen].bounds.size.width, 64);
        } ];
        [self performSelector:@selector(removeView:) withObject:@(true) afterDelay:3];
    }
    
    
    
}

-(void)showGreenAlertWithText:(NSString*)text
{
    
    if (visibleViews.count > 0) [self removeView:@(true)];
    if (text && text.length > 0)
    {
        
        UIView *currentView = [[UIView alloc] initWithFrame:CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, 64)];
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 44)];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:18];
        [currentView addSubview:label];
        label.text = text;
        currentView.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.90];
        
        UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
        [currentWindow addSubview:currentView];
        [visibleViews addObject:currentView];
        [UIView animateWithDuration:.3 animations:^{
            currentView.frame = CGRectMake(0, -0, [UIScreen mainScreen].bounds.size.width, 64);
        } ];
        [self performSelector:@selector(removeView:) withObject:@(true) afterDelay:3];
    }
}

-(void)removeView:(NSNumber*)animatedFlag
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    BOOL animated= false;
    if  (animatedFlag && animatedFlag.boolValue){
        animated = true;
    }
    for (UIView * view in visibleViews) {
        if (view && view.superview){
            [UIView animateWithDuration:animated?.3:0 animations:^{
                view.frame = CGRectMake(0, -64, [UIScreen mainScreen].bounds.size.width, 64);
            } completion:^(BOOL finished) {
                [view removeFromSuperview];
                [visibleViews removeObject:view];
                
            }];
        }
    }
    
}

@end
