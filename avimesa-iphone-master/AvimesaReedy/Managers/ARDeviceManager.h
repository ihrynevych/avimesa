//
//  ARDeviceManager.h
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AWSSNS/AWSSNS.h>
@class Location;

//callback type
typedef void(^ARDeviceManagerCallback)(BOOL isSucceess,id identifier);
typedef void(^ARDeviceManagerReedySaveCallback)(BOOL isSucceess,id identifier,NSDictionary*response,NSError *error);

typedef void(^ARDeviceManagerHubCallback)(BOOL isSucceess,id identifier,NSString* uuid);
typedef void(^ARDeviceManagerDeleteCallback)(BOOL isSucceess);

@interface ARDeviceManager : NSObject

+ (ARDeviceManager *)sharedManager;

/////////////////////////////////////////////
//             create methods              //
/////////////////////////////////////////////

//location
- (void)createLocationWithName:(NSString*)name  callback:(ARDeviceManagerCallback)callback;
- (void)createLocationWithName:(NSString*)name  imageData:(NSData*)imageData  callback:(ARDeviceManagerCallback)callback;
//hub
- (void)createHubWithName:(NSString *)name locationID:(id)locationID callback:(ARDeviceManagerHubCallback)callback;
//reedy
- (void)createReedyWithName:(NSString *)name hubID:(id)hubID deviceID:(id)deviceID state:(BOOL)state callback:(ARDeviceManagerCallback)callback;


/////////////////////////////////////////////
//             update methods              //
/////////////////////////////////////////////

//profile
- (void)updateProfileWithDictionary:(NSDictionary *)dictionary callback:(ARDeviceManagerCallback)callback;
//location
- (void)updateLocationWithDictionary:(NSDictionary *)dictionary locationId:(NSInteger)locationId callback:(ARDeviceManagerCallback)callback;
//reedy
- (void)updateReedyWithDictionary:(NSDictionary *)dictionary reedyID:(NSInteger)reedyID callback:(ARDeviceManagerReedySaveCallback)callback;


/////////////////////////////////////////////
//             delete methods              //
/////////////////////////////////////////////

//location
- (void)deleteLocationWithID:(id)locationID callback:(ARDeviceManagerDeleteCallback)callback;
//hub
- (void)deleteHubWithID:(id)hubID callback:(ARDeviceManagerDeleteCallback)callback;
//reedy
- (void)deleteReedyWithID:(id)reedyID callback:(ARDeviceManagerDeleteCallback)callback;



// location managment
- (NSInteger)curentLocationId;
- (BOOL)switchToLocationWithId:(NSInteger)locationId;
- (Location*)curentLocationObject;


// exists objects
- (BOOL)isExistsLocation;
- (BOOL)isExistsReedy;
- (BOOL)isExistsHub;


//amazon sns
- (void)subscribeToSNS;
- (void)unsubscribeFromSNS;
@end
