 //
//  ARDeviceManager.m
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARDeviceManager.h"
#import "NSManagedObjectContext+Helper.h"
#import "ARAPIClient.h"
#import "Location.h"


@implementation ARDeviceManager
+ (ARDeviceManager *)sharedManager
{
    static id _default = nil;
    static dispatch_once_t safer;
    dispatch_once(&safer, ^(void)
                  {
                      _default = [[[self class] alloc] init];
                  });
    return _default;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

#pragma mark - create methods
- (void)createLocationWithName:(NSString*)name callback:(ARDeviceManagerCallback)callback
{
    [self createLocationWithName:name imageData:nil callback:callback];
}

- (void)createLocationWithName:(NSString*)name  imageData:(NSData*)imageData callback:(ARDeviceManagerCallback)callback
{
    if (name) {
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        
        [parameters setObject:name forKey:@"name"];
        if (imageData) {
            [parameters setObject:imageData forKey:@"image"];
        }
        
        [[ARAPIClient sharedClient] putRequestPath:@"/api/v1/locations" parameters:parameters block:^(ARAPIClientResponse *response) {
            if (response.error) {
                callback(NO,nil);
            }
            else
            {
                callback(YES,[response.data valueForKey:@"id"]);
            }
        }];
    }
    else
        callback(NO,nil);
}

- (void)createHubWithName:(NSString *)name locationID:(id)locationID callback:(ARDeviceManagerHubCallback)callback
{
    if (name&&locationID) {
        NSDictionary* parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                     locationID,@"location_id",
                                     name,@"name",nil];
        [[ARAPIClient sharedClient]putJSON:nil withRequestPath:@"/api/v1/hubs"  parameters:parameters block:^(ARAPIClientResponse *response) {
            if (response.error) {
                callback(NO,nil,nil);
            }
            else
            {
                callback(YES,[response.data valueForKey:@"id"],[response.data valueForKey:@"uuid"]);
            }
        }];
    }
    else
        callback(NO,nil,nil);
}

- (void)createReedyWithName:(NSString *)name hubID:(id)hubID deviceID:(id)deviceID state:(BOOL)state callback:(ARDeviceManagerCallback)callback
{
    if (name&&hubID&&deviceID) {
        NSDictionary* parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                     hubID,@"hub_id",
                                     name,@"name",
                                     deviceID,@"device_id",
                                     @(state),@"state",nil];
        [[ARAPIClient sharedClient]putJSON:nil withRequestPath:@"/api/v1/reedies"  parameters:parameters block:^(ARAPIClientResponse *response) {
            if (response.error) {
                callback(NO,nil);
            }
            else
            {
                callback(YES,[response.data valueForKey:@"device_id"]);
            }
        }];
    }
    else
        callback(NO,nil);
    
}

#pragma mark - update methods

- (void)updateProfileWithDictionary:(NSDictionary *)dictionary callback:(ARDeviceManagerCallback)callback
{
    
    [[ARAPIClient sharedClient]postJSON:nil withRequestPath:@"/api/v1/profile"  parameters:dictionary block:^(ARAPIClientResponse *response) {
        if (response.error) {
            callback(NO,nil);
        }
        else
        {
            callback(YES,nil);
        }
    }];
    
}

- (void)updateLocationWithDictionary:(NSDictionary *)dictionary locationId:(NSInteger)locationId callback:(ARDeviceManagerCallback)callback
{
    [[ARAPIClient sharedClient]postLocationWithRequestPath:[NSString stringWithFormat:@"/api/v1/locations/%ld",(long)locationId] parameters:dictionary block:^(ARAPIClientResponse *response)
     {
         if (response.error)
         {
             callback(NO,nil);
         }
         else
         {
             callback(YES,nil);
         }
     }];
}

- (void)updateReedyWithDictionary:(NSDictionary *)dictionary reedyID:(NSInteger)reedyID callback:(ARDeviceManagerReedySaveCallback)callback
{
    
    [[ARAPIClient sharedClient]postJSON:nil withRequestPath:[NSString stringWithFormat:@"/api/v1/reedies/%ld",(long)reedyID] parameters:dictionary block:^(ARAPIClientResponse *response) {
        if (response.error)
        {
            callback(NO,nil,response.data,response.error);
        }
        else
        {
            callback(YES,[response.data valueForKey:@"id"],response.data,nil);
        }
    }];
    
}

#pragma mark - delete methods

- (void)deleteLocationWithID:(id)locationID callback:(ARDeviceManagerDeleteCallback)callback
{
    if (locationID) {
        [[ARAPIClient sharedClient] deleteRequestPath:[NSString stringWithFormat:@"/api/v1/locations/%ld",(long)[locationID integerValue] ] parameters:nil block:^(ARAPIClientResponse *response) {
            if (callback) {
                if (response.error) {
                    callback(NO);
                }
                else
                {
                    callback(YES);
                }
            }
        }];
    }
}

- (void)deleteHubWithID:(id)hubID callback:(ARDeviceManagerDeleteCallback)callback
{
    if (hubID) {
        [[ARAPIClient sharedClient] deleteRequestPath:[NSString stringWithFormat:@"/api/v1/hubs/%ld",(long)[hubID integerValue] ] parameters:nil block:^(ARAPIClientResponse *response) {
            if (callback) {
                if (response.error) {
                    callback(NO);
                }
                else
                {
                    callback(YES);
                }
            }
        }];
    }
}

- (void)deleteReedyWithID:(id)reedyID callback:(ARDeviceManagerDeleteCallback)callback
{
    if (reedyID) {
        [[ARAPIClient sharedClient] deleteRequestPath:[NSString stringWithFormat:@"/api/v1/reedies/%ld",(long)[reedyID integerValue] ] parameters:nil block:^(ARAPIClientResponse *response) {
            if (callback) {
                if (response.error) {
                    callback(NO);
                }
                else
                {
                    callback(YES);
                }
            }
        }];
    }
}

#pragma mark - location managment

//curent location
- (NSInteger)curentLocationId
{
    NSArray * locationsArray =[[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Location" predicate:nil sortKey:nil ascending:NO] ;
  
    return [self curentLocationIdWithLocationsArray:locationsArray];
}

- (Location*)curentLocationObject
{
    NSArray * locationsArray =[[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Location" predicate:nil sortKey:nil ascending:NO] ;
    NSInteger locationId = [self curentLocationIdWithLocationsArray:locationsArray];
    return [[locationsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %d",@"identifier",locationId]] firstObject];
}

- (NSInteger)curentLocationIdWithLocationsArray:(NSArray*)locationsArray
{
    if (![locationsArray count])
        return 0;
    NSInteger locationId = 0;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:APP_SELECTED_LOCATION_KEY]) {
        locationId = [[[NSUserDefaults standardUserDefaults] valueForKey:APP_SELECTED_LOCATION_KEY] integerValue];
    }
    
    if ([self isLocationIdValid:locationId locationsArray:locationsArray])
    {
        return locationId;
    }
    else
    {
        Location * location = [locationsArray firstObject];
        
        [[NSUserDefaults standardUserDefaults] setObject:location.identifier forKey:APP_SELECTED_LOCATION_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return location.identifier.integerValue;
    }
    return 0;
}

- (BOOL)switchToLocationWithId:(NSInteger)locationId
{
    NSArray * locationsArray =[[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Location" predicate:nil sortKey:nil ascending:NO];
    
    [self unsubscribeFromSNS];
    
    if ([self isLocationIdValid:locationId locationsArray:locationsArray])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@(locationId) forKey:APP_SELECTED_LOCATION_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self subscribeToSNS];
        
        return true;
    }
    else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:APP_SELECTED_LOCATION_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return false;
    }
    
}

-(BOOL)isLocationIdValid:(NSInteger)locationId locationsArray:(NSArray*)locationsArray
{
    if ([locationsArray count]) {
        return [[locationsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %d", @"identifier",locationId]] count];
    }else
        return false;
}


#pragma mark - Amazon SNS

- (void)subscribeToSNS
{
    // curent location object
    Location * location = [self curentLocationObject];
    
    // get device endpoint ARN
    NSString * endpointARN =[[NSUserDefaults standardUserDefaults] valueForKey:AMAZON_ENDPOINT_ARN_KEY];
    
    // topic arn from location
    NSString *topicARN = location.topic_arn;
    
    if (endpointARN && topicARN)
    {
        @try
        {
            //init sns client
            AmazonSNSClient *snsClient = [[AmazonSNSClient alloc] initWithAccessKey:AMAZON_ACCESS_KEY_ID withSecretKey:AMAZON_SECRET_KEY];
            snsClient.endpoint = AMAZON_ENDPOINT_URL;
            
            //init subscribe request
            SNSSubscribeRequest *subscribeRequest = [[SNSSubscribeRequest alloc] initWithTopicArn:topicARN andProtocol:AMAZON_PROTOVOL andEndpoint: endpointARN];
            
            [subscribeRequest setTopicArn:topicARN];
            [snsClient subscribe:subscribeRequest];
            
            //save subscribe information
            [[NSUserDefaults standardUserDefaults] setObject:topicARN forKey:LOCATION_TOPIC_ARN_KEY];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        @catch (AmazonServiceException *serviceException) {
            NSLog(@"%@", serviceException);
        }
    }
    
}

- (void)unsubscribeFromSNS
{
    // get device endpoint ARN
    NSString * endpointARN =[[NSUserDefaults standardUserDefaults] valueForKey:AMAZON_ENDPOINT_ARN_KEY];
    
    // get location topic ARN
    NSString *topicARN = [[NSUserDefaults standardUserDefaults] valueForKey:LOCATION_TOPIC_ARN_KEY];
    if (endpointARN && topicARN) {
        @try
        {
            //init sns client
            AmazonSNSClient *snsClient = [[AmazonSNSClient alloc] initWithAccessKey:AMAZON_ACCESS_KEY_ID withSecretKey:AMAZON_SECRET_KEY];
            snsClient.endpoint = AMAZON_ENDPOINT_URL;
            
            // get subscribe arn
            NSString * subscribtionARN = [self amazonSNSClient:snsClient findSubscriptionARNForTopicARN:topicARN withEndpointARN:endpointARN];
            
            if (subscribtionARN) {
                //init unsubscribe request
                SNSUnsubscribeRequest *unsubscribeRequest = [[SNSUnsubscribeRequest alloc] initWithSubscriptionArn:subscribtionARN];
                [snsClient unsubscribe:unsubscribeRequest];
            }
            
        }
        @catch (AmazonServiceException *serviceException) {
            NSLog(@"%@", serviceException);
        }
    }
    
}

- (NSString *)amazonSNSClient:(AmazonSNSClient *)snsClient findSubscriptionARNForTopicARN:(NSString *)topicARN withEndpointARN:(NSString *)endpointARN
{
    //all subscription by topic
    SNSListSubscriptionsByTopicRequest *listSubscriptionRequest = [[SNSListSubscriptionsByTopicRequest alloc] initWithTopicArn:topicARN];
    SNSListSubscriptionsByTopicResponse *response = [snsClient listSubscriptionsByTopic:listSubscriptionRequest];
    
    if (response.error) {
        NSLog(@"Error: %@", response.error);
        return nil;
    }
    for (SNSSubscription *subscription in response.subscriptions) {
        if ([subscription.endpoint isEqualToString:endpointARN]) {
            //return subscribtion arn
            return subscription.subscriptionArn;
        }
    }
    return nil;
}

#pragma mark - exists objects

-(BOOL)isExistsLocation
{
    return [[[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Location" predicate:nil sortKey:nil ascending:NO] count];
}

- (BOOL)isExistsReedy{
    return [[[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Reedy" predicate:nil sortKey:nil ascending:NO] count];
}

- (BOOL)isExistsHub{
    return [[[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Hub" predicate:nil sortKey:nil ascending:NO] count];
}

@end
