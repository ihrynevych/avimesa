//
//  JMLoginManager.h
//  JostMobile
//
//  Created by Pavlo Yonak on 6/11/14.
//  Copyright (c) 2014 Lemberg Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARAPIClient.h"
typedef void(^ARLoginCallback)(BOOL isSucceess,NSDictionary *response, NSString *userID);

@protocol ARLoginStateObserver
@required
-(void)loginStateChangedTo:(BOOL)logged;
@end

@interface ARLoginManager : NSObject

+ (ARLoginManager *)sharedLoginManager;

//sign in method
- (void)performLoginWithUserName:(NSString*)userName password:(NSString*)password callback:(ARLoginCallback)callback;

//sign up method
- (void)performRegistrationWithUserName:(NSString*)userName password:(NSString*)password callback:(ARLoginCallback)callback;

// sign out method
- (void)performLogout;

// profile properties
- (id) profileId;
- (NSString*) profileName;
- (NSString*) topicARN;

- (BOOL)isUserLoggedIn;

//login state observer
- (void) registerLoginStateObserver:(id<ARLoginStateObserver,NSObject>) observer;
- (void) unregisterLoginStateObserver:(id<ARLoginStateObserver,NSObject>) observer;
@end
