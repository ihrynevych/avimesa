//
//  JMLoginManager.m
//  JostMobile
//
//  Created by Pavlo Yonak on 6/11/14.
//  Copyright (c) 2014 Lemberg Solutions. All rights reserved.
//

#import "ARLoginManager.h"
#import <AWSSNS/AWSSNS.h>
#import "NSManagedObjectContext+Helper.h"
#import "Location.h"
#import "ARDeviceManager.h"

static NSString*const kCookieName =@"session";

@interface ARLoginManager()

@property (nonatomic,strong) NSMutableArray* loginStateObservers;
@property BOOL userLoggedIn;


@end
@implementation ARLoginManager

#pragma mark - init
+ (ARLoginManager *)sharedLoginManager
{
    static id _default = nil;
    static dispatch_once_t safer;
    dispatch_once(&safer, ^(void)
                  {
                      _default = [[[self class] alloc] init];
                  });
    return _default;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        self.loginStateObservers = [NSMutableArray array];
        
        self.userLoggedIn = [self profileId]? YES : NO;
        
    }
    return self;
}

#pragma mark - sign methods

- (void)performLoginWithUserName:(NSString*)userName password:(NSString*)password callback:(ARLoginCallback)callback
{
    //sign out
    [self performLogout];
    
    //credential dictionary
    NSDictionary* credentials = [NSDictionary dictionaryWithObjectsAndKeys:
                                 userName,@"username",
                                 password,@"password",nil];
    
    //perform sign in
    [[ARAPIClient sharedClient] postRequestPath:@"/api/v1/login" parameters:credentials block:^(ARAPIClientResponse *response) {
        if (response.error) {
            //observer notification
            [self notifyObserversLoginStateChange:NO];
            
            //callback response
            callback(NO,response.data,nil);
        }
        else
        {
            //observer notification
            [self notifyObserversLoginStateChange:YES];
            
            //save profile properties to user defaults
            if ([response.data valueForKey:@"id"]) {
                [[NSUserDefaults standardUserDefaults] setObject:[response.data valueForKey:@"id"] forKey:PROFILE_ID_KEY];
            }
            if ([response.data valueForKey:@"username"]) {
                [[NSUserDefaults standardUserDefaults] setObject:[response.data valueForKey:@"username"] forKey:PROFILE_NAME_KEY];
            }
            if ([response.data valueForKey:@"topic_arn"]) {
                [[NSUserDefaults standardUserDefaults] setObject:[response.data valueForKey:@"topic_arn"] forKey:PROFILE_TOPIC_ARN_KEY];
            }
            if (response.data ) {
                [[NSUserDefaults standardUserDefaults] setObject:response.data forKey:PROFILE_JSON_KEY];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //callback response
            callback(YES,response.data,[response.data valueForKey:@"id"]?[response.data valueForKey:@"id"]:nil);
        }
    }];
}

- (void)performRegistrationWithUserName:(NSString*)userName password:(NSString*)password callback:(ARLoginCallback)callback
{
    //sign out
    [self performLogout];
    
    //credential dictionary
    NSDictionary* credentials = [NSDictionary dictionaryWithObjectsAndKeys:
                                 userName,@"username",
                                 password,@"password",nil];
    
    //perform sign up
    [[ARAPIClient sharedClient] postRequestPath:@"/api/v1/registration" parameters:credentials block:^(ARAPIClientResponse *response) {
        
        if (response.error)
        {
            //observer notification
            [self notifyObserversLoginStateChange:NO];
            
            //callback response
            callback(NO,response.data,nil);
        }
        else
        {
            //observer notification
            [self notifyObserversLoginStateChange:YES];
            
            //save profile properties to user defaults
            if ([response.data valueForKey:@"id"]) {
                [[NSUserDefaults standardUserDefaults] setObject:[response.data valueForKey:@"id"] forKey:PROFILE_ID_KEY];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:userName forKey:PROFILE_NAME_KEY];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //callback response
            callback(YES,response.data,[response.data valueForKey:@"id"]?[response.data valueForKey:@"id"]:nil);
        }
    }];
}

-(void)performLogout
{
    //unsubscribe from sns
    [[ARDeviceManager sharedManager] unsubscribeFromSNS];
    
    //remove profile properties from user defaults
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PROFILE_ID_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PROFILE_NAME_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PROFILE_TOPIC_ARN_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PROFILE_JSON_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //clear cookies
    [self clearLoginCookies];
    
    //observer notification
    [self notifyObserversLoginStateChange:NO];
    
}

-(id)profileId
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:PROFILE_ID_KEY];
}

-(NSString*)profileName
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:PROFILE_NAME_KEY];
}
- (NSString*) topicARN
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:PROFILE_TOPIC_ARN_KEY];
}

#pragma mark - LoginStateObservers managment

-(void)registerLoginStateObserver:(id<ARLoginStateObserver,NSObject>) observer
{
    [self.loginStateObservers addObject:observer];
}

-(void)unregisterLoginStateObserver:(id<ARLoginStateObserver,NSObject>) observer
{
    [self.loginStateObservers removeObject:observer];
}

-(void) notifyObserversLoginStateChange:(BOOL)logged
{
    if(self.userLoggedIn!= logged)
    {
        self.userLoggedIn = logged;
        for(id<ARLoginStateObserver,NSObject> observer in self.loginStateObservers)
        {
            [observer loginStateChangedTo:logged];
        }
    }
}

#pragma mark - Status methods

-(BOOL)isUserCookieValid
{
    return [self isUserLoggedInWithCookie:kCookieName];
}

- (void)clearLoginCookies
{
    NSHTTPCookieStorage * sharedCookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray * cookies = [sharedCookieStorage cookies];
    for (NSHTTPCookie * cookie in cookies)
    {
        
        if([cookie.domain hasPrefix:AR_APP_DOMAIN])
        {
            [sharedCookieStorage deleteCookie:cookie];
        }
    }
}

-(BOOL)isUserLoggedInWithCookie:(NSString*)cookieName
{
    NSHTTPCookieStorage * sharedCookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray * cookies = [sharedCookieStorage cookies];
    for(NSHTTPCookie* cookie in cookies)
    {
        if([cookie.domain rangeOfString:AR_APP_DOMAIN].location != NSNotFound
           && [cookie.name isEqualToString:cookieName]
           && cookie.expiresDate!=nil
           &&[cookie.expiresDate compare:[NSDate date]] != NSOrderedAscending)
        {
            return YES;
        }
    }
    return NO;
}

-(BOOL)isUserLoggedIn
{
    return ([self isUserCookieValid]&&[self profileId]);
}


@end

