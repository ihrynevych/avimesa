//
//  ARPeripheralConnectionsManager.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>

//menu items
typedef NS_ENUM (NSUInteger, ARPeripheralManagerState) {
    ARPeripheralReadyToProcess,
    ARPeripheralReedyConnectionInProgress,
    ARPeripheralReedyUpdateInProgress,
    ARPeripheralConnectedToReedy,
    ARPeripheralHubConnectionInProgress,
    ARPeripheralConnectedToHub,
    ARPeripheralHubUUIDUpdateInProgress,
    ARPeripheralHubWifiUpdateInProgress
};

@protocol ARPeripheralConnectionsManagerObserver

@required

-(void)connectionTimeout;
-(void)connectionFail;

-(void)progressDidChangeWithValue:(int)progressValue;

-(void)didConnectToReedyWithUDID:(NSString*)reedyUDID;
-(void)didConnectToHubWithUUID:(NSString*)reedyUUID wifiArray:(NSArray*)wifiArray;

-(void)didUpdateReedy;
-(void)reedyUpdateFailed;

-(void)didUpdateHubUUID;
-(void)hubUUIDUpdateFailed;

-(void)didUpdateHubWifi;
-(void)hubWifiUpdateFailed;

@end

@interface ARPeripheralConnectionsManager : NSObject



+ (ARPeripheralConnectionsManager *)sharedManager;

- (BOOL)startReedyConnection;
- (BOOL)updateReedyWithUDID:(NSString*)reedyUDID reedyID:(int)reedyID;


- (BOOL)startHubConnection;
- (BOOL)updateHubWithUUID:(NSString*)hubUUID ;
- (BOOL)updateHubWifiWithSSID:(NSString *)ssid password:(NSString *)password ;
- (void)stopAllConnections;


//manager  observer
- (void) registerManagerStateObserver:(id<ARPeripheralConnectionsManagerObserver,NSObject>) observer;
- (void) unregisterManagerStateObserver:(id<ARPeripheralConnectionsManagerObserver,NSObject>) observer;

@end
