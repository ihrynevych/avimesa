//
//  ARPeripheralConnectionsManager.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARPeripheralConnectionsManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "NSString+Helper.h"
#import "ARWifiObject.h"

@interface ARPeripheralConnectionsManager()< CBCentralManagerDelegate, CBPeripheralDelegate>{
    NSTimer * connectTimer;
    NSTimer * timeoutTimer;
    
    int  reedyIDValue;

    NSString * reedyUDID;
    
    NSString * hubUUID;
    
    NSMutableData *hubWifiListBuffer;
    
    NSInteger lastSendIndex;
    NSData * hubWifiBufferToSend;
    NSData * hubWifiCurrentPartToSend;
}
@property (nonatomic) ARPeripheralManagerState managerState;

@property (nonatomic,strong) NSMutableArray* managerObservers;
@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *connectedPeripheral;


@property (strong, nonatomic) CBCharacteristic *connectedReedyIDCharacteristic;
@property (strong, nonatomic) CBCharacteristic *connectedReedyValueCharacteristic;

@property (strong, nonatomic) CBCharacteristic *connectedHubIDCharacteristic;
@property (strong, nonatomic) CBCharacteristic *connectedHubWifiListCharacteristic;
@property (strong, nonatomic) CBCharacteristic *connectedHubWifiCharacteristic;


@end

@implementation ARPeripheralConnectionsManager



#pragma mark - init
+ (ARPeripheralConnectionsManager *)sharedManager
{
    static id _default = nil;
    static dispatch_once_t safer;
    dispatch_once(&safer, ^(void)
                  {
                      _default = [[[self class] alloc] init];
                  });
    return _default;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        self.managerState = ARPeripheralReadyToProcess;
        self.managerObservers = [NSMutableArray array];
    }
    return self;
}

- (void) registerManagerStateObserver:(id<ARPeripheralConnectionsManagerObserver,NSObject>) observer{
    if (observer)
    {
        [self.managerObservers addObject:observer];
    }
    
}
- (void) unregisterManagerStateObserver:(id<ARPeripheralConnectionsManagerObserver,NSObject>) observer{
    if (observer)
    {
        [self.managerObservers removeObject:observer];
    }
    
}


-(void)stopAllConnections
{
    reedyUDID = @"";
    reedyIDValue = 0;
    
    hubUUID = @"";
    
    hubWifiListBuffer = nil;
    
    lastSendIndex = 0;
    hubWifiBufferToSend = nil;
    hubWifiCurrentPartToSend = nil;
    
    if (_centralManager) {
        [_centralManager stopScan];
        if (_connectedPeripheral) {
            [_centralManager cancelPeripheralConnection:_connectedPeripheral];
            _connectedPeripheral = nil;
        }
        _centralManager.delegate = nil;
        _centralManager = nil;
        _connectedReedyIDCharacteristic = nil;
        _connectedReedyValueCharacteristic = nil;
        
        _connectedHubIDCharacteristic = nil;
        _connectedHubWifiListCharacteristic = nil;
        _connectedHubWifiCharacteristic = nil;
        
        self.managerState = ARPeripheralReadyToProcess;
    }
}


-(BOOL)startReedyConnection
{
    if (self.managerState == ARPeripheralReadyToProcess)
    {
        reedyUDID = @"";
        [self _initTimeoutTimer];
        self.managerState = ARPeripheralReedyConnectionInProgress;
        
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        
        
        return true;
    }else return false;
}
-(BOOL)updateReedyWithUDID:(NSString *)theReedyUDID reedyID:(int)reedyID
{
    if (self.managerState == ARPeripheralConnectedToReedy && _connectedPeripheral  && _connectedReedyIDCharacteristic &&_connectedReedyValueCharacteristic && reedyID >0 && theReedyUDID && theReedyUDID.length>0)
    {
        self.managerState = ARPeripheralReedyUpdateInProgress;
        reedyIDValue = reedyID;
        reedyUDID = theReedyUDID;
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer progressDidChangeWithValue:33];}
        
        [_connectedPeripheral writeValue:[NSString dataFromHexString:reedyUDID] forCharacteristic:_connectedReedyIDCharacteristic type:CBCharacteristicWriteWithResponse];
        
        [self _initTimeoutTimer];
        
        
        return true;
    }else return false;
}

-(BOOL)startHubConnection
{
    if (self.managerState == ARPeripheralReadyToProcess)
    {
        hubUUID = @"";
        hubWifiListBuffer = [NSMutableData data];
        [self _initTimeoutTimer];
        self.managerState = ARPeripheralHubConnectionInProgress;
        
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        
        
        return true;
    }else return false;
}
-(BOOL)updateHubWifiWithSSID:(NSString *)ssid password:(NSString *)password
{
    if (self.managerState == ARPeripheralConnectedToHub && _connectedPeripheral  && _connectedHubWifiCharacteristic && ssid && ssid.length){
        self.managerState = ARPeripheralHubWifiUpdateInProgress;
        NSString * ssidDataInterpretation = [ssid length] ? [self hexStringFromData:[ssid dataUsingEncoding:NSUTF8StringEncoding] fromated:NO]:@"";
        NSString * passwordDataInterpretation = [password length] ? [self hexStringFromData:[password dataUsingEncoding:NSUTF8StringEncoding] fromated:NO]:@"";
        NSString * outputString = [NSString stringWithFormat:@"%@00%@00",ssidDataInterpretation,passwordDataInterpretation];
        lastSendIndex = 0;
        hubWifiBufferToSend = [NSString dataFromHexString:outputString];
        
        if ([hubWifiBufferToSend length]>20) {
            hubWifiCurrentPartToSend = [hubWifiBufferToSend subdataWithRange:NSMakeRange(0, 20)];
            lastSendIndex = 19;
        }
        else
        {
            lastSendIndex = [hubWifiBufferToSend length]-1;
            hubWifiCurrentPartToSend = hubWifiBufferToSend;
        }
        [_connectedPeripheral writeValue:hubWifiCurrentPartToSend forCharacteristic:_connectedHubWifiCharacteristic type:CBCharacteristicWriteWithResponse];
        [self _initTimeoutTimer];

        return true;
    }else
        return false;
}
-(BOOL)updateHubWithUUID:(NSString *)theHubUUID
{
    if (self.managerState == ARPeripheralConnectedToHub && _connectedPeripheral  && _connectedHubIDCharacteristic && theHubUUID && [theHubUUID length]){
        self.managerState = ARPeripheralHubUUIDUpdateInProgress;
        
        hubUUID = theHubUUID;
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer progressDidChangeWithValue:33];}
        
        [_connectedPeripheral writeValue:[NSString dataFromHexString:hubUUID] forCharacteristic:_connectedHubIDCharacteristic type:CBCharacteristicWriteWithResponse];
        
        [self _initTimeoutTimer];

        return true;
    }else
        return false;
}

#pragma mark - private

-(NSArray *)componentsFromData :(NSData*)data separatedBySign:(NSString *)sign
{
    NSString * hexString = [self unformatedHexStringFromData:data];
    NSArray * array = [hexString componentsSeparatedByString:sign];
    NSMutableArray * result = [NSMutableArray array];
    for (NSString *stringData in array) {
        if(stringData && [stringData length])
        {
            [result addObject:[NSString dataFromHexString:stringData]];
        }
    }
    return result;
}

-(BOOL)dataContains :(NSData*)data sign:(NSString *)sign
{
    NSString * hexString = [self unformatedHexStringFromData:data];
    NSRange range =[hexString rangeOfString:sign];
    if (range.location == NSNotFound) {
        return NO;
    }
    else
        return YES;
    
}
- (NSString *)unformatedHexStringFromData:(NSData *)data {
    
    return [self hexStringFromData:data fromated:NO];
}

- (NSString *)hexStringFromData:(NSData *)data fromated:(BOOL)fromated{
    
    NSUInteger bytesToConvert = [data length];
    const unsigned char *uuidBytes = [data bytes];
    NSMutableString *hexString = [NSMutableString stringWithCapacity:16];
    
    for (NSUInteger currentByteIndex = 0; currentByteIndex < bytesToConvert; currentByteIndex++)
    {
        if (fromated)
        {
            switch (currentByteIndex)
            {
                case 3:
                case 5:
                case 7:
                case 9:[hexString appendFormat:@"%02x-", uuidBytes[currentByteIndex]]; break;
                default:[hexString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
            }
        }
        else
        {
            [hexString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
        }
        
        
    }
    return hexString;
}
#pragma mark -  timers

-(void)_cancelTimer
{
    if (connectTimer)
    {
        [connectTimer invalidate];
        connectTimer = nil;
    }
}

-(void)_initTimer
{
    connectTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(discoverServices) userInfo:nil repeats:NO];
}

-(void)discoverServices
{
    [self _resetTimeoutTimer];
    
    if (_connectedPeripheral) {
        if (self.managerState == ARPeripheralReedyConnectionInProgress){
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:60];}
            [_connectedPeripheral discoverServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:REEDY_SERVICE_UUID]]];
        }else if (self.managerState == ARPeripheralHubConnectionInProgress){

            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:42];}
            
            [_connectedPeripheral discoverServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:HUB_SERVICE_UUID]]];
            
        }
        
    }
    
}


-(void)_cancelTimeoutTimer
{
    if (timeoutTimer)
    {
        [timeoutTimer invalidate];
        timeoutTimer = nil;
    }
}

-(void)_initTimeoutTimer
{
    timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:40 target:self selector:@selector(handleTimeout) userInfo:nil repeats:NO];
}

-(void)_resetTimeoutTimer
{
    [self _cancelTimeoutTimer];
    [self _initTimeoutTimer];
    
    
    
}

-(void)handleTimeout
{
    [self stopAllConnections];
    for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
    {if (observer)[observer connectionTimeout];}
    return;
    
}

-(void)wifiInfoDidLoad
{
    for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
    {if (observer)[observer progressDidChangeWithValue:99];}

    
    NSMutableArray * wifiArray = [NSMutableArray array];
    if ([hubWifiListBuffer length]){
        NSArray * components = [self componentsFromData :hubWifiListBuffer separatedBySign:@"00"];
        for (int i = 0; i< [components count]; i+=3) {
            ARWifiObject * wifiObject= [[ARWifiObject alloc] init];
            wifiObject.ssid = [[NSString alloc] initWithData:components[i] encoding:NSUTF8StringEncoding];
            if (i+1 < [components count]) {
                wifiObject.protection = @([[[NSString alloc] initWithData:components[i+1] encoding:NSUTF8StringEncoding] boolValue]);
            }
            if (i+2 < [components count]) {
                wifiObject.signalStrength = @([[[NSString alloc] initWithData:components[i+2] encoding:NSUTF8StringEncoding] integerValue]);
            }
            [wifiArray addObject:wifiObject];
        }
    }
    self.managerState = ARPeripheralConnectedToHub;
    for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
    {if (observer)[observer didConnectToHubWithUUID:hubUUID wifiArray:wifiArray];}
}

#pragma mark -  CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    [self _resetTimeoutTimer];
    if (central.state != CBCentralManagerStatePoweredOn) {
        [self stopAllConnections];
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer connectionFail];}
        return;
    }
    
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        // Scan for devices
        
        if (self.managerState == ARPeripheralReedyConnectionInProgress){
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:20];}
            
            [_centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:REEDY_SERVICE_UUID]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
        }else if (self.managerState == ARPeripheralHubConnectionInProgress){
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:14];}

            
            [_centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:HUB_SERVICE_UUID]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
        }
        
        
    }

}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (self.managerState == ARPeripheralReedyConnectionInProgress){
        if ([RSSI integerValue]<0 && [RSSI integerValue]>(-30))
        {
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:40];}
            
            [self _resetTimeoutTimer];
            _connectedPeripheral = peripheral;
            _connectedPeripheral.delegate = self;
            [_centralManager connectPeripheral:peripheral options:nil];
            [_centralManager stopScan];
            
        }
    }else if (self.managerState == ARPeripheralHubConnectionInProgress){
        
        if ([RSSI integerValue]<0 && [RSSI integerValue]>(-50))
        {
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:28];}

            
            [self _resetTimeoutTimer];
            
            _connectedPeripheral = peripheral;
            _connectedPeripheral.delegate = self;
            [_centralManager connectPeripheral:peripheral options:nil];
            [_centralManager stopScan];
            
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [self _initTimer];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    if (connectTimer) {
        [self _cancelTimer];
        [_centralManager connectPeripheral:peripheral options:nil];
    }
}

#pragma mark -  CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    [self _resetTimeoutTimer];
    if (error)
    {
        [self stopAllConnections];
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer connectionFail];}
        return;
    }
    if (self.managerState == ARPeripheralReedyConnectionInProgress){
        for (CBService *service in peripheral.services)
        {
            [peripheral discoverCharacteristics:[NSArray arrayWithObjects:[CBUUID UUIDWithString:REEDY_VALUES_CHARACTERISTIC_UUID], [CBUUID UUIDWithString:REEDY_ID_CHARACTERISTIC_UUID],nil] forService:service];
        }
    }else if (self.managerState == ARPeripheralHubConnectionInProgress)
    {
        for (CBService *service in peripheral.services) {
            [peripheral discoverCharacteristics:[NSArray arrayWithObjects:[CBUUID UUIDWithString:HUB_WIFI_SETTING_CHARACTERISTIC_UUID],[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID],[CBUUID UUIDWithString:HUB_ID_CHARACTERISTIC_UUID], nil] forService:service];
        }
    }
    
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    [self _resetTimeoutTimer];
    if (error)
    {
        [self stopAllConnections];
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer connectionFail];}
        return;
    }
    
    if (self.managerState == ARPeripheralReedyConnectionInProgress){
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer progressDidChangeWithValue:80];}
        for (CBCharacteristic *characteristic in service.characteristics)
        {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:REEDY_ID_CHARACTERISTIC_UUID]])
            {
                _connectedReedyIDCharacteristic = characteristic;
                [peripheral readValueForCharacteristic:characteristic];
                
            }
            else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:REEDY_VALUES_CHARACTERISTIC_UUID]])
            {
                _connectedReedyValueCharacteristic = characteristic;
                [peripheral readValueForCharacteristic:characteristic];
            }
        }
    }else if (self.managerState == ARPeripheralHubConnectionInProgress)
    {

        for (CBCharacteristic *characteristic in service.characteristics)
        {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_ID_CHARACTERISTIC_UUID]])
            {
                _connectedHubIDCharacteristic = characteristic;
                
            }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_SETTING_CHARACTERISTIC_UUID]])
            {
                _connectedHubWifiCharacteristic = characteristic;
                
            }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID]])
            {
                _connectedHubWifiListCharacteristic =characteristic;
            }
        }
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer progressDidChangeWithValue:56];}

        if (_connectedHubIDCharacteristic) {
            [peripheral readValueForCharacteristic:_connectedHubIDCharacteristic];
        }else
        {
                    for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
                    {if (observer)[observer connectionFail];}
            
        }
        
    }
}
-(void)callToHubWifiInfoCharacteristic
{
    if (_connectedHubWifiListCharacteristic && _connectedPeripheral) {
        unsigned char mydata = 1;
        [_connectedPeripheral writeValue:[NSMutableData dataWithBytes:&mydata length:sizeof(mydata)] forCharacteristic:_connectedHubWifiListCharacteristic type:CBCharacteristicWriteWithResponse];
    }else
    {
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer connectionFail];}
        
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    [self _resetTimeoutTimer];
    if (error)
    {
        [self stopAllConnections];
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer connectionFail];}
        return;
    }
    
    if (self.managerState == ARPeripheralReedyConnectionInProgress){
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer progressDidChangeWithValue:100];}
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:REEDY_ID_CHARACTERISTIC_UUID]])
        {
            reedyUDID = [NSString hexStringFromData:characteristic.value];
            if (!reedyUDID) {
                [self stopAllConnections];
                for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
                {if (observer)[observer connectionFail];}
                return;
            }else if (_connectedReedyValueCharacteristic)
            {
                [self _cancelTimeoutTimer];
                self.managerState = ARPeripheralConnectedToReedy;
                for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
                {if (observer)[observer didConnectToReedyWithUDID:reedyUDID];}
            }
            
        }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:REEDY_VALUES_CHARACTERISTIC_UUID]]){
            if (reedyUDID&&reedyUDID.length>0) {
                [self _cancelTimeoutTimer];
                self.managerState = ARPeripheralConnectedToReedy;
                for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
                {if (observer)[observer didConnectToReedyWithUDID:reedyUDID];}
            }
            
        }
    }else if (self.managerState == ARPeripheralHubConnectionInProgress)
    {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_ID_CHARACTERISTIC_UUID]])
        {
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:70];}

            hubUUID = [NSString hexStringFromData:characteristic.value];
            [self callToHubWifiInfoCharacteristic];
        }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID]])
        {
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:84];}

            
            if ([characteristic.value length] <= 1) {
                [self _cancelTimeoutTimer];
                
                [hubWifiListBuffer appendData:characteristic.value];
                
                [self wifiInfoDidLoad];
            }
            else
            {
                if ([self dataContains:characteristic.value sign:@"0000"])
                {
                    if ([[self componentsFromData :characteristic.value separatedBySign:@"0000"] count]) {
                        [hubWifiListBuffer appendData:[self componentsFromData :characteristic.value separatedBySign:@"0000"][0]];
                    }
                    [self _cancelTimeoutTimer];
                    [self wifiInfoDidLoad];
                }else
                {
                    [self _resetTimeoutTimer];
                    [hubWifiListBuffer appendData:characteristic.value];
                    [peripheral readValueForCharacteristic:characteristic];
                }
            }
        }
        
    }
    
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    [self _resetTimeoutTimer];
    if (error)
    {
        [self stopAllConnections];
        for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
        {if (observer)[observer connectionFail];}
        return;
    }
    
    if (self.managerState == ARPeripheralReedyUpdateInProgress){
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:REEDY_ID_CHARACTERISTIC_UUID]])
        {
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:66];}
            
            int majorValue1 = ((reedyIDValue ) <<8)|(1<<2);
            
            unsigned char aBuffer[4];
            
            [_connectedReedyValueCharacteristic.value  getBytes:aBuffer range:NSMakeRange(0, 4)];
            char* bytes = (char*) &majorValue1;
            aBuffer[0] = bytes[0];
            aBuffer[1] = bytes[1];
            [_connectedPeripheral writeValue:[NSData dataWithBytes:aBuffer length:4] forCharacteristic:_connectedReedyValueCharacteristic type:CBCharacteristicWriteWithResponse];
        }else  if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:REEDY_VALUES_CHARACTERISTIC_UUID]]) {
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:99];}
            [self _cancelTimeoutTimer];
            
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer didUpdateReedy];}
        }
    }else if (self.managerState == ARPeripheralHubWifiUpdateInProgress)
    {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_SETTING_CHARACTERISTIC_UUID]])
        {
            NSInteger  dataLength = [hubWifiBufferToSend length];
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:(int)(lastSendIndex*100/dataLength)];}

            [self _resetTimeoutTimer];
            if (dataLength - lastSendIndex - 1 > 0) {
                if (dataLength - lastSendIndex - 1 > 20)
                {
                    hubWifiCurrentPartToSend = [hubWifiBufferToSend subdataWithRange:NSMakeRange(lastSendIndex+1, 20)];
                    lastSendIndex += 20;
                }
                else
                {
                    NSInteger tempLength = dataLength - lastSendIndex -1;
                    hubWifiCurrentPartToSend = [hubWifiBufferToSend subdataWithRange:NSMakeRange(lastSendIndex+1, tempLength)];
                    lastSendIndex += tempLength;
                }
                [_connectedPeripheral writeValue:hubWifiCurrentPartToSend forCharacteristic:_connectedHubWifiCharacteristic type:CBCharacteristicWriteWithResponse];
            }
            else
            {
                for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
                {if (observer)[observer progressDidChangeWithValue:99];}
                [self _cancelTimeoutTimer];
                self.managerState = ARPeripheralConnectedToHub;
                for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
                {if (observer)[observer didUpdateHubWifi];}
               
            }

        }
        
    }else if (self.managerState == ARPeripheralHubUUIDUpdateInProgress)
    {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_ID_CHARACTERISTIC_UUID]]) {
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer progressDidChangeWithValue:99];}
            [self _cancelTimeoutTimer];
            self.managerState = ARPeripheralConnectedToHub;
            for(id<ARPeripheralConnectionsManagerObserver,NSObject> observer in self.managerObservers)
            {if (observer)[observer didUpdateHubUUID];}
        }

        
    }else if (self.managerState == ARPeripheralHubConnectionInProgress){
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:HUB_WIFI_INFO_CHARACTERISTIC_UUID]])
        {
            if (!hubWifiListBuffer)
            {
                hubWifiListBuffer = nil;
            }
            hubWifiListBuffer = [NSMutableData data];
            [peripheral readValueForCharacteristic:characteristic];
            [self _resetTimeoutTimer];
        }
    }
    
}


@end
