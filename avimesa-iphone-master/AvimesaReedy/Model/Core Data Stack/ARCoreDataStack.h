//
//  ACCoreDataStack.h
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
#define AR_MOC_DID_SAVE @"ARManagedObjectContextDidSaveNotification"

@interface ARCoreDataStack : NSObject

/*
 
 Use NSManagedObjectContext+Helper category for creating/getting new NSManagedObjectContext
 
 */

+ (ARCoreDataStack*)sharedInstance;

@end
