//
//  ACCoreDataStack.m
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARCoreDataStack.h"
#import "ARCoreDataStack_Private.h"
#import <CoreData/CoreData.h>
#import "NSManagedObjectContext+Helper.h"

#define AC_MODEL_NAME @"ReedyModel"

#define AC_SQLITE_FILE_NAME @"ReedyModel.sqlite"

@implementation ARCoreDataStack


+ (ARCoreDataStack *)sharedInstance
{
    static id _default = nil;
    static dispatch_once_t safer;
    dispatch_once(&safer, ^(void)
                  {
                      _default = [[[self class] alloc] init];
                  });
    return _default;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        [self model];
        [self coordinator];
        [self backgroundMOC];
        [self mainMOC];
        [self temporaryContexts];
    }
    return self;
}
- (NSMutableDictionary*)temporaryContexts
{
    if (!_temporaryContexts) {
        return _temporaryContexts = [NSMutableDictionary dictionary];
    }
    return _temporaryContexts;
}

- (NSManagedObjectContext*)backgroundMOC
{
    if (!_backgroundMOC) {
        if (self.coordinator) {
            _backgroundMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [_backgroundMOC performBlockAndWait:^{
                _backgroundMOC.persistentStoreCoordinator = self.coordinator;
            }];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backgroundMOCDidSave:) name:NSManagedObjectContextDidSaveNotification object:_backgroundMOC];
        }
    }
    return _backgroundMOC;
}

- (NSManagedObjectContext*)mainMOC
{
    if (!_mainMOC) {
        if (self.coordinator) {
            _mainMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            _mainMOC.persistentStoreCoordinator = self.coordinator;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mainMOCDidSave:) name:NSManagedObjectContextDidSaveNotification object:_mainMOC];
        }
    }
    return _mainMOC;
}

- (NSManagedObjectContext*)backgroundQueueManagedObjectContext
{
    return self.backgroundMOC;
}

- (NSManagedObjectContext*)mainQueueManagedObjectContext
{
    return self.mainMOC;
}

- (void)startPrivateQContextObserving:(NSManagedObjectContext*)context
{
    [context performBlockAndWait:^{
        context.parentContext = self.backgroundMOC;
    }];
    self.temporaryContexts[@(context.hash)] = context;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temporaryContextDidSave:) name:NSManagedObjectContextDidSaveNotification object:context];
}

- (void)stopPrivateQContextObserving:(NSManagedObjectContext*)context
{
    if (self.temporaryContexts[@(context.hash)]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:context];
        [self.temporaryContexts removeObjectForKey:@(context.hash)];
    }
}

- (void)temporaryContextDidSave:(NSNotification*)notification
{
    [self.backgroundMOC performBlock:^{
        NSError *error = nil;
        [self.backgroundMOC processPendingChanges];
        [self.backgroundMOC save:&error];
        if (error) {
            NSLog(@"Save to disk failure, error: %@, reason: %@", [error localizedDescription] , [error localizedFailureReason]);
        }
        else {
            /*
             !!!
             if UI needs updating, make sure that its updated on main thread (otherwise - crash)
             !!!
             */
            //NSLog(@"Posted ManagedObjectContext Did Save Notification. UI reload.");
            //[[NSNotificationCenter defaultCenter] postNotificationName:AG_MOC_DID_SAVE object:nil userInfo:nil];
        }
    }];
    //[self stopPrivateQContextObserving:temporaryContext];
}

- (void)backgroundMOCDidSave:(NSNotification*)notification
{
    [self.mainMOC performBlock:^{
        [self.mainMOC mergeChangesFromContextDidSaveNotification:notification];
        //NSLog(@"Merged changes to mainMOC.");
        //NSLog(@"Posted ManagedObjectContext Did Save Notification. UI reload.");
//        [self saveContext:self.mainMOC];
        [[NSNotificationCenter defaultCenter] postNotificationName:AR_MOC_DID_SAVE object:nil userInfo:nil];
        
    }];
}

- (void)mainMOCDidSave:(NSNotification*)notification
{
    [self.backgroundMOC performBlock:^{
        [self.backgroundMOC mergeChangesFromContextDidSaveNotification:notification];
        //NSLog(@"Merged changes to backgroundMOC.");
        
    }];
}

- (NSPersistentStoreCoordinator*)coordinator
{
    if (!_coordinator) {
        NSString *storePath = [[self applicationContentDirectoryPath] stringByAppendingPathComponent:AC_SQLITE_FILE_NAME];
        NSURL *storeURL = [NSURL fileURLWithPath:[storePath stringByExpandingTildeInPath] isDirectory:NO];
        NSError *error = nil;
        _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
        if (![_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
            abort();
        }
    }
    return _coordinator;
}

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator
{
    return self.coordinator;
}

- (NSManagedObjectModel*)model
{
    if (!_model) {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:AC_MODEL_NAME withExtension:@"momd"];
        _model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    return _model;
}

- (NSString*)applicationContentDirectoryPath
{
    NSString *path = [self documentDirectoryPath];
    if (path) {
        path = [path stringByAppendingPathComponent:@"Contents"] ;
        if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
            NSError *error;
            if (![[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error]) {
                NSLog(@"Unable to create Contents directory: %@", error);
            }
            // exclude content from iCloud backup
            BOOL isExcluded = [[NSURL fileURLWithPath:path] setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
            NSAssert(isExcluded, @"Unable to exclude content from backup: %@", [error localizedDescription]);
        }
    }
    return path;
}

- (NSString *)documentDirectoryPath
{
    NSArray *documentDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = nil;
    if (documentDir) {
        path = [documentDir objectAtIndex:0];
    }
    return path;
}
- (void)saveContext:(NSManagedObjectContext*)context
{
    [context performBlock:^{
        NSError *error = nil;
        [context save:&error];
        if (error) {
            NSLog(@"Failed to save Document %@", [error localizedDescription]);
        }
        else if (context == [self mainQueueManagedObjectContext]) {
            //NSLog(@"DocumentContextDidSave on main queue");
        }
    }];
}

#pragma mark - dealloc

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
