//
//  ACCoreDataStack_Private.h
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//
#import "ARCoreDataStack.h"

@interface ARCoreDataStack ()

@property (nonatomic,strong) NSManagedObjectModel *model;
@property (nonatomic,strong) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic,strong) NSManagedObjectContext *backgroundMOC;
@property (nonatomic,strong) NSManagedObjectContext *mainMOC;
@property (nonatomic,strong) NSMutableDictionary    *temporaryContexts;

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator;
- (NSManagedObjectContext*)backgroundQueueManagedObjectContext;
- (NSManagedObjectContext*)mainQueueManagedObjectContext;

- (void)startPrivateQContextObserving:(NSManagedObjectContext*)context;
- (void)stopPrivateQContextObserving:(NSManagedObjectContext*)context;

- (void)saveContext:(NSManagedObjectContext*)context;


@end
