//
//  ACUpdateController.h
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARUpdate.h"

@interface ARUpdateController : NSObject


- (void)updateReediesWithUserID:(NSString*)userID completion:(void (^) (BOOL success))completion;
- (void)updateReedyWithReedyID:(NSInteger)reedyID completion:(void (^) (BOOL success))completion;
- (void)updateStatsWithReedyID:(NSInteger)reedyID completion:(void (^) (BOOL success))completion;
- (void)longupdateForReediesWithUserID:(NSString*)userID completion:(void (^) (BOOL success))completion;
- (void)updateAllWithCompletion:(void (^) (BOOL success))completion;
- (void)updateHubsWithCompletion:(void (^) (BOOL success))completion;
- (void)updateLocationsWithCompletion:(void (^) (BOOL success))completion;
-(void)cancel;
@end
