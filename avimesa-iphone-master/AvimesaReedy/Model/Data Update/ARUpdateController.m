//
//  ACUpdateController.m
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARUpdateController.h"
#import "ARCoreDataStack.h"
#import "NSManagedObjectContext+Helper.h"

#define AC_DATA_UPDATE_FAILURE                                 @"DataUpdateFailure"

@interface ARUpdateController () <ARUpdateDelegate>

@property (nonatomic,strong) NSManagedObjectContext *context;
@property (nonatomic,strong) NSMutableArray *operations;
@property (nonatomic) NSMutableDictionary *completionBlocks;
@property (nonatomic) NSMutableArray *failureDescriptions;

@end

@implementation ARUpdateController
- (id)init
{
    if (self = [super init]) {
        _operations = [NSMutableArray array];
        _completionBlocks = [NSMutableDictionary dictionary];
        _failureDescriptions = [NSMutableArray array];
    }
    return self;
}

- (NSManagedObjectContext*)context
{
    if (!_context) {
        _context = [NSManagedObjectContext privateQParsingContextObserved];
    }
    return _context;
}

//- (NSMutableArray*)operations
//{
//    if (!_operations) {
//        _operations = [NSMutableArray array];
//    }
//    return _operations;
//}

#pragma mark - Dynamic Data updates


- (void)updateReediesWithUserID:(NSString*)userID completion:(void (^) (BOOL success))completion
{
    ARUpdate *updateUserReedies = [[ARUpdate alloc] initWithContext:self.context delegate:self];
    [updateUserReedies addReediesUpdateOperationWithUserID:userID];
    if (completion) {
        self.completionBlocks[updateUserReedies.operationIdentifier] = completion;
    }
    [self startOperation:updateUserReedies];
}
- (void)updateReedyWithReedyID:(NSInteger)reedyID completion:(void (^) (BOOL success))completion
{
    ARUpdate *updateUserReedies = [[ARUpdate alloc] initWithContext:self.context delegate:self];
    [updateUserReedies addReedyUpdateOperationWithReedyID:reedyID];
    if (completion) {
        self.completionBlocks[updateUserReedies.operationIdentifier] = completion;
    }
    [self startOperation:updateUserReedies];
}
- (void)updateStatsWithReedyID:(NSInteger)reedyID completion:(void (^) (BOOL success))completion
{
    ARUpdate *updateUserReedies = [[ARUpdate alloc] initWithContext:self.context delegate:self];
    [updateUserReedies addStatsUpdateOperationWithReedyID:reedyID];
    [updateUserReedies addActivityUpdateOperationWithReedyID:reedyID];
    if (completion) {
        self.completionBlocks[updateUserReedies.operationIdentifier] = completion;
    }
    [self startOperation:updateUserReedies];
}
- (void)longupdateForReediesWithUserID:(NSString*)userID completion:(void (^) (BOOL success))completion
{
    ARUpdate *updateUserReedies = [[ARUpdate alloc] initWithContext:self.context delegate:self];
    [updateUserReedies addReediesLongUpdateOperationWithUserID:userID];
    if (completion) {
        self.completionBlocks[updateUserReedies.operationIdentifier] = completion;
    }
    [self startOperation:updateUserReedies];
}

- (void)updateAllWithCompletion:(void (^) (BOOL success))completion{
    ARUpdate *updateAll = [[ARUpdate alloc] initWithContext:self.context delegate:self];
    [updateAll addLocationsUpdateOperation];
    [updateAll addHubsUpdateOperation];
    
    [updateAll addReediesUpdateOperationWithUserID:nil];
    if (completion) {
        self.completionBlocks[updateAll.operationIdentifier] = completion;
    }
    [self startOperation:updateAll];
}

- (void)updateHubsWithCompletion:(void (^) (BOOL success))completion{
    ARUpdate *updateHubs = [[ARUpdate alloc] initWithContext:self.context delegate:self];
    [updateHubs addHubsUpdateOperation];
    if (completion) {
        self.completionBlocks[updateHubs.operationIdentifier] = completion;
    }
    [self startOperation:updateHubs];
}

- (void)updateLocationsWithCompletion:(void (^) (BOOL success))completion{
    ARUpdate *updateLocations = [[ARUpdate alloc] initWithContext:self.context delegate:self];
    [updateLocations addLocationsUpdateOperation];
    if (completion) {
        self.completionBlocks[updateLocations.operationIdentifier] = completion;
    }
    [self startOperation:updateLocations];
}
-(void)cancel
{
    for (NSOperation*operation in self.operations) {
        [operation cancel];
    }
}
-(void)startOperation:(NSOperation*)operation
{
    [self.operations addObject:operation];
    NSLog(@"added %@", NSStringFromClass([operation class]));
    if ([self.operations count]==1) {
        [(NSOperation*)[self.operations lastObject] start];
        NSLog(@"started %@", NSStringFromClass([operation class]));
    }
}

#pragma mark - AGUpdateDelegate

- (void)updateSuccessfulyCompletedForOperation:(NSOperation*)operation
{
    [self.operations removeObject:operation];
    if (![self.operations count]) {
        NSLog(@"finished %@", NSStringFromClass([operation class]));
    }
    else {
        [(NSOperation*)[self.operations firstObject] start];
        NSLog(@"started %@", NSStringFromClass([(NSOperation*)[self.operations firstObject] class]));
    }
    
    ARUpdate *op = (ARUpdate*)operation;
    void (^completion)(BOOL success) = self.completionBlocks[op.operationIdentifier];
    if (completion) {
        completion(YES);
        [self.completionBlocks removeObjectForKey:op.operationIdentifier];
    }
}

- (void)updateFailedWithError:(NSError*)error forOperation:(NSOperation*)operation
{
    NSString *failureDescription = nil;
    [self.operations removeObject:operation];
    if (![self.operations count]) {
        //NSLog(@"stopObserverForPrivateQParsingContext");
        if (self.failureDescriptions.count) {
            for (NSString *description in self.failureDescriptions) {
                failureDescription = failureDescription.length ? [NSString stringWithFormat:@"%@\n%@", failureDescription, description.copy] : description.copy;
            }
            [self.failureDescriptions removeAllObjects];
        }
        if (error.localizedDescription) {
            failureDescription = failureDescription.length ? [NSString stringWithFormat:@"%@\n%@", failureDescription, error.localizedDescription] : error.localizedDescription;
        }
        
        //        if (failureDescription.length) {
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //                [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"Data update failed.\n%@", failureDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
        //            });
        //        } else {
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //                [[[UIAlertView alloc] initWithTitle:nil message:@"Data update failed" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
        //            });
        //        }
        
    } else {
        [(NSOperation*)[self.operations firstObject] start];
        NSLog(@"started %@", NSStringFromClass([(NSOperation*)[self.operations firstObject] class]));
        if (error.localizedDescription) {
            [self.failureDescriptions addObject:error.localizedDescription];
        }
    }
    NSLog(@"Data update failed, error: %@, reason: %@", [error localizedDescription], [error localizedFailureReason]);
    if (error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:AC_DATA_UPDATE_FAILURE object:nil userInfo:@{@"kError": error}];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:AC_DATA_UPDATE_FAILURE object:nil userInfo:nil];
    }
    
    ARUpdate *op = (ARUpdate*)operation;
    void (^completion)(BOOL success) = self.completionBlocks[op.operationIdentifier];
    if (completion) {
        completion(NO);
        [self.completionBlocks removeObjectForKey:op.operationIdentifier];
    }
}
-(void)dealloc
{
    [NSManagedObjectContext stopObserverForPrivateQParsingContext:self.context];
}
@end
