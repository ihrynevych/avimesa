//
//  Activity.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "Activity.h"


@implementation Activity

@dynamic timestamp;
@dynamic changed_state_to;
@dynamic reedyID;

@end
