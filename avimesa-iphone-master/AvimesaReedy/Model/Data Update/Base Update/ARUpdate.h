//
//  ACUpdate.h
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ARManagedObjectUpdateOperation.h"
#import "ARReediesUpdateOperation.h"
#import "ARHubsUpdateOperation.h"
#import "ARLocationsUpdateOperation.h"
#import "ARReediesLongPoolingUpdateOperation.h"
#import "ARActivityUpdateOperation.h"
#import "ARStatsUpdateOperation.h"
#import "ARReedyUpdateOperation.h"
@class ARUpdate;

@protocol ARUpdateDelegate <NSObject>
- (void)updateSuccessfulyCompletedForOperation:(NSOperation*)operation;
- (void)updateFailedWithError:(NSError*)error forOperation:(NSOperation*)operation;
@end

@interface ARUpdate : NSOperation<ARManagedObjectUpdateDelegate>

@property (nonatomic,strong) NSMutableArray *operations;
@property (nonatomic,readonly) NSString *operationIdentifier;


// ========= Public methods =============

- (id)initWithContext:(NSManagedObjectContext*)context delegate:(id<ARUpdateDelegate>)delegate;


// ========= Subclass methods =============

- (void)addReediesUpdateOperationWithUserID:(NSString*)userID;
- (void)addReedyUpdateOperationWithReedyID:(NSInteger)reedyID;
- (void)addReediesLongUpdateOperationWithUserID:(NSString*)userID;
- (void)addActivityUpdateOperationWithReedyID:(NSInteger)reedyID;
- (void)addStatsUpdateOperationWithReedyID:(NSInteger)reedyID;
- (void)addHubsUpdateOperation;
- (void)addLocationsUpdateOperation;
- (void)startUpdates;



// ========= Methods to be overriden by subclasses ===========
-(void)updateCompletedSuccessfully:(BOOL)sucessfully;
@end
