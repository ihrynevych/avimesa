//
//  ACUpdate.m
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARUpdate.h"
#import "NSManagedObjectContext+Helper.h"

static NSString *kIsFinished = @"isFinished";

@interface ARUpdate ()
@property (nonatomic,strong) NSOperationQueue *updateQueue;
@property (nonatomic,strong) NSManagedObjectContext *context;
@property (nonatomic,weak) id <ARUpdateDelegate> delegate;
@property (nonatomic,readwrite) NSString *operationIdentifier;

@end

@implementation ARUpdate
- (id)initWithContext:(NSManagedObjectContext *)context delegate:(id<ARUpdateDelegate>)delegate
{
    self = [super init];
    if (self) {
        _context = context;
        _delegate = delegate;
        _operationIdentifier = [self operationIdentifier];
    }
    return self;
}

- (NSString *)operationIdentifier
{
    if (!_operationIdentifier) {
        CFUUIDRef uuidRef = CFUUIDCreate(NULL);
        CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
        CFRelease(uuidRef);
        _operationIdentifier = (__bridge NSString *)uuidStringRef;
    }
    return _operationIdentifier;
}

- (void)main
{
    @autoreleasepool {
        [self startUpdates];
    }
}




- (void)addOperation:(NSOperation*)operation
{
    [self.operations addObject:operation];
}

- (void)addReediesUpdateOperationWithUserID:(NSString *)userID
{
    [self addOperation:[[ARReediesUpdateOperation alloc] initWithContext:self.context userID:userID delegate:self]];
}
- (void)addReedyUpdateOperationWithReedyID:(NSInteger)reedyID
{
    [self addOperation:[[ARReedyUpdateOperation alloc] initWithContext:self.context reedyID:reedyID delegate:self]];
}
- (void)addReediesLongUpdateOperationWithUserID:(NSString*)userID
{
    [self addOperation:[[ARReediesLongPoolingUpdateOperation alloc] initWithContext:self.context userID:userID delegate:self]];
}

- (void)addActivityUpdateOperationWithReedyID:(NSInteger)reedyID
{
    [self addOperation:[[ARActivityUpdateOperation alloc] initWithContext:self.context reedyID:reedyID delegate:self]];
}

- (void)addStatsUpdateOperationWithReedyID:(NSInteger)reedyID
{
    [self addOperation:[[ARStatsUpdateOperation alloc] initWithContext:self.context reedyID:reedyID delegate:self]];
}

- (void)addHubsUpdateOperation
{
    [self addOperation:[[ARHubsUpdateOperation alloc] initWithContext:self.context delegate:self]];
}
- (void)addLocationsUpdateOperation
{
    [self addOperation:[[ARLocationsUpdateOperation alloc] initWithContext:self.context delegate:self]];
}
- (void)startUpdates
{
    if ([self.operations count] && !self.isCancelled) {
        [self.updateQueue addOperation:[self.operations firstObject]];
    }
}

-(void)updateCompletedSuccessfully:(BOOL)sucessfully
{
    //Method can be overriden by child classes
}

#pragma mark - Getters

- (NSOperationQueue *)updateQueue
{
    if (!_updateQueue) {
        _updateQueue = [[NSOperationQueue alloc] init];
        _updateQueue.name = @"Update Queue";
        [_updateQueue setMaxConcurrentOperationCount:1];
    }
    return _updateQueue;
}

- (NSMutableArray*)operations
{
    if (!_operations) {
        _operations = [NSMutableArray array];
    }
    return _operations;
}

- (void)saveContext
{
    [self.context performBlock:^{
        NSError *error = nil;
        [self.context save:&error];
        if (!error) {
            if ([self.delegate respondsToSelector:@selector(updateSuccessfulyCompletedForOperation:)]) {
                [self.delegate updateSuccessfulyCompletedForOperation:self];
                [self updateCompletedSuccessfully:true];
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(updateFailedWithError:forOperation:)]) {
                [self.delegate updateFailedWithError:error forOperation:self];
                [self updateCompletedSuccessfully:false];
            }
        }
    }];
}

#pragma mark - AGManagedObjectUpdateDelegate

- (void)managedObjectUpdateCompletedForOperation:(NSOperation*)operation
{

    [self.operations removeObject:operation];
    if ([self.operations count]) {
        [self.updateQueue addOperation:[self.operations firstObject]];
    } else {
        [self saveContext];
    }
}

- (void)managedObjectUpdateFailedWithError:(NSError *)error forOperation:(NSOperation *)operation
{
    [self.operations removeAllObjects];
    [self.updateQueue cancelAllOperations];
    if ([self.delegate respondsToSelector:@selector(updateFailedWithError:forOperation:)]) {
        [self.delegate updateFailedWithError:error forOperation:self];
    }
}
@end
