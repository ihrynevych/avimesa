//
//  ARActivityUpdateOperation.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARActivityUpdateOperation.h"
#import "ARAPIClient.h"
#import "NSManagedObject+Helper.h"
static NSString *kActivityJsonKey = @"Activity";
static NSString *kActivityRequestnKey = @"Activity";


@interface ARActivityUpdateOperation()
@property (nonatomic) NSInteger reedyID;

@end
@implementation ARActivityUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext reedyID:(NSInteger)reedyID delegate:(id<ARManagedObjectUpdateDelegate>)delegate
{
    self = [super initWithContext:managedObjectContext jsonKey:kActivityJsonKey requestnKey:kActivityRequestnKey entityName:NSStringFromClass([Activity class]) delegate:delegate];
    if (self) {
        self->_reedyID = reedyID;
    }
    return self;
}
- (void)handleDictionary:(id)data
{
    if (self.isCancelled == YES) return;
    
    if (data&&[data isKindOfClass:[NSArray class]]) {
        [self updateActivityElements:data withOldElements:[self.managedObjectContext fetchObjectsWithEntityName:self.entityName predicate:nil sortKey:nil ascending:NO]];
        //        [self setupRelationshipsForObjects:updatedObjects updates:dictionary[self.jsonKey]];
        [self updateSuccess];
    } else {
        [self updateSuccess];
    }
}
-(void)updateActivityElements:(NSArray*)newElements withOldElements:(NSArray*)oldElements
{
    for (Activity * element in oldElements) {
        [self.managedObjectContext deleteObject:element];
    }
    for (NSDictionary * dictionary  in newElements) {
        Activity * element = (Activity*)[self.managedObjectContext createObjectWithEntityName:self.entityName];
        [element safeSetValuesForKeysWithDictionary:dictionary];
        element.reedyID = @(self.reedyID);
    }
}
- (void)updateRequestWithSuccess:(void (^) (NSDictionary *jsonDictionary))success failure:(void (^) (NSError *error))failure
{
    if (self.requestnKey) {
        [[ARAPIClient sharedClient] activityRequestWithReedyID:self.reedyID withBlock:^(ARAPIClientResponse *response) {
            if (response && response.data) {
                success(response.data);
            }
            else
                failure(response.error);
        }];
    }
}
@end
