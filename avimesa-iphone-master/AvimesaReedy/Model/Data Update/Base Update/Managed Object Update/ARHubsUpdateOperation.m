//
//  ARHubsUpdateOperation.m
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARHubsUpdateOperation.h"
#import "ARAPIClient.h"
static NSString *kHubsJsonKey = @"Hubs";
static NSString *kHubsRequestnKey = @"Hubs";
#define  kAttributesExclusion  @{@"identifier": @"id"}
@implementation ARHubsUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext delegate:(id<ARManagedObjectUpdateDelegate>)delegate
{
    self = [super initWithContext:managedObjectContext jsonKey:kHubsJsonKey requestnKey:kHubsRequestnKey entityName:NSStringFromClass([Hub class]) delegate:delegate];
    if (self) {
        
    }
    return self;
}
- (void)handleDictionary:(id)data
{
    if (self.isCancelled == YES) return;
    
    if (data&&[data isKindOfClass:[NSArray class]]) {
       // NSArray *updatedObjects = [self.managedObjectContext updatedObjectsByIdentifierUsingUpdates:(NSArray*)data entityName:self.entityName withAttributesExclusion:kAttributesExclusion deleteNotUpdatedObjects:YES];
        //        [self setupRelationshipsForObjects:updatedObjects updates:dictionary[self.jsonKey]];
        [self updateSuccess];
    } else {
        [self updateSuccess];
    }
}
- (void)setupRelationshipsForObjects:(NSArray*)objects updates:(NSArray *)updates
{
    
}

- (void)updateRequestWithSuccess:(void (^) (NSDictionary *jsonDictionary))success failure:(void (^) (NSError *error))failure
{
    if (self.requestnKey) {
        [[ARAPIClient sharedClient] hubsRequestWithBlock:^(ARAPIClientResponse *response) {
            if (response && response.data) {
                success(response.data);
            }
            else
                failure(response.error);
        }];
    }
}
@end
