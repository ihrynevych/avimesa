//
//  ARLocationsUpdateOperation.h
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARManagedObjectUpdateOperation.h"

@interface ARLocationsUpdateOperation : ARManagedObjectUpdateOperation

@end
