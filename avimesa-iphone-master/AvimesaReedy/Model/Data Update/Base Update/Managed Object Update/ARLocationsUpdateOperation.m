//
//  ARLocationsUpdateOperation.m
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARLocationsUpdateOperation.h"
#import "ARAPIClient.h"
static NSString *kLocationsJsonKey = @"Locations";
static NSString *kLocationsRequestnKey = @"Locations";
#define  kAttributesExclusion  @{@"identifier": @"id"}
@implementation ARLocationsUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext delegate:(id<ARManagedObjectUpdateDelegate>)delegate
{
    self = [super initWithContext:managedObjectContext jsonKey:kLocationsJsonKey requestnKey:kLocationsRequestnKey entityName:NSStringFromClass([Location class]) delegate:delegate];
    if (self) {
        
    }
    return self;
}
- (void)handleDictionary:(id)data
{
    if (self.isCancelled == YES) return;
    
    if (data&&[data isKindOfClass:[NSArray class]]) {
       // NSArray *updatedObjects = [self.managedObjectContext updatedObjectsByIdentifierUsingUpdates:(NSArray*)data entityName:self.entityName withAttributesExclusion:kAttributesExclusion deleteNotUpdatedObjects:YES];
        //        [self setupRelationshipsForObjects:updatedObjects updates:dictionary[self.jsonKey]];
        [self updateSuccess];
    } else {
        [self updateSuccess];
    }
}
- (void)setupRelationshipsForObjects:(NSArray*)objects updates:(NSArray *)updates
{
    
}

- (void)updateRequestWithSuccess:(void (^) (NSDictionary *jsonDictionary))success failure:(void (^) (NSError *error))failure
{
    if (self.requestnKey) {
        [[ARAPIClient sharedClient] locationsRequestWithBlock:^(ARAPIClientResponse *response) {
            if (response && response.data) {
                success(response.data);
            }
            else
                failure(response.error);
        }];
    }
}
@end
