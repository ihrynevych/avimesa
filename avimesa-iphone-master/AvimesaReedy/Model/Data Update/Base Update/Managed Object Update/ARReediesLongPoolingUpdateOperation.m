//
//  ARReediesLongPoolingUpdateOperation.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/24/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReediesLongPoolingUpdateOperation.h"
#import "ARAPIClient.h"
static NSString *kReediesJsonKey = @"Reedies";
static NSString *kReediesRequestnKey = @"Reedies";

#define  kAttributesExclusion  @{@"identifier": @"id"}

@interface ARReediesLongPoolingUpdateOperation()
@property (nonatomic,strong) NSString *userID;

@end
@implementation ARReediesLongPoolingUpdateOperation

- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext userID:(NSString*)userID delegate:(id<ARManagedObjectUpdateDelegate>)delegate
{
    self = [super initWithContext:managedObjectContext jsonKey:kReediesJsonKey requestnKey:kReediesRequestnKey entityName:NSStringFromClass([Reedy class]) delegate:delegate];
    if (self) {
        self->_userID = userID;
    }
    return self;
}
- (void)handleDictionary:(id)data
{
    if (self.isCancelled == YES) return;
    
    if (data&&[data isKindOfClass:[NSArray class]]) {
      //  NSArray *updatedObjects = [self.managedObjectContext updatedObjectsByIdentifierUsingUpdates:(NSArray*)data entityName:self.entityName withAttributesExclusion:kAttributesExclusion deleteNotUpdatedObjects:YES];
        //        [self setupRelationshipsForObjects:updatedObjects updates:dictionary[self.jsonKey]];
        [self updateSuccess];
    } else {
        [self updateSuccess];
    }
}
- (void)setupRelationshipsForObjects:(NSArray*)objects updates:(NSArray *)updates
{
    
}

- (void)updateRequestWithSuccess:(void (^) (NSDictionary *jsonDictionary))success failure:(void (^) (NSError *error))failure
{
    if (self.requestnKey) {

        [[ARAPIClient sharedClient] reediesLongPolingRequestWithParameters:[[NSUserDefaults standardUserDefaults]objectForKey:LastModifiedKey]?@{IfModifiedSinceKey:[[NSUserDefaults standardUserDefaults]objectForKey:LastModifiedKey]}:nil WithBlock:^(ARAPIClientResponse *response) {
            if (response.error)
            {
                failure(response.error);
 
            }
            else
            {
                if (response && response.data) {
                    success(response.data);
                    if ([response.headers valueForKey:LastModifiedKey]) {
                        [[NSUserDefaults standardUserDefaults] setObject:[response.headers valueForKey:LastModifiedKey] forKey:LastModifiedKey];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                }
                else
                    failure(nil);
            }
            
                
        }];
    }
}


@end
