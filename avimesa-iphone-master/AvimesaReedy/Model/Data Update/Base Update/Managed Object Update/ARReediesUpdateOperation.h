//
//  ARReediesUpdateOperation.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/17/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARManagedObjectUpdateOperation.h"

@interface ARReediesUpdateOperation : ARManagedObjectUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext userID:(NSString*)userID delegate:(id<ARManagedObjectUpdateDelegate>)delegate;
@end
