//
//  ARReedyUpdateOperation.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 11/27/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARManagedObjectUpdateOperation.h"

@interface ARReedyUpdateOperation : ARManagedObjectUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext reedyID:(NSInteger)reedyID delegate:(id<ARManagedObjectUpdateDelegate>)delegate;
@end
