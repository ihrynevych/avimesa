//
//  ARReedyUpdateOperation.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 11/27/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReedyUpdateOperation.h"
#import "ARAPIClient.h"
static NSString *kReediesJsonKey = @"Reedy";
static NSString *kReediesRequestnKey = @"Reedy";

#define  kAttributesExclusion  @{@"identifier": @"id"}
@interface ARReedyUpdateOperation()
@property (nonatomic) NSInteger reedyID;

@end
@implementation ARReedyUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext reedyID:(NSInteger)reedyID delegate:(id<ARManagedObjectUpdateDelegate>)delegate
{
    self = [super initWithContext:managedObjectContext jsonKey:kReediesJsonKey requestnKey:kReediesRequestnKey entityName:NSStringFromClass([Reedy class]) delegate:delegate];
    if (self) {
        self->_reedyID = reedyID;
    }
    return self;
}
- (void)handleDictionary:(id)data
{
    if (self.isCancelled == YES) return;
    
    if (data&&[data isKindOfClass:[NSArray class]]) {
       // NSArray *updatedObjects = [self.managedObjectContext updatedObjectsByIdentifierUsingUpdates:(NSArray*)data entityName:self.entityName withAttributesExclusion:kAttributesExclusion deleteNotUpdatedObjects:NO];
        //        [self setupRelationshipsForObjects:updatedObjects updates:dictionary[self.jsonKey]];
        [self updateSuccess];
    } else {
        [self updateSuccess];
    }
}
- (void)setupRelationshipsForObjects:(NSArray*)objects updates:(NSArray *)updates
{
    
}

- (void)updateRequestWithSuccess:(void (^) (NSDictionary *jsonDictionary))success failure:(void (^) (NSError *error))failure
{
    if (self.requestnKey) {
        [[ARAPIClient sharedClient] reediesRequestWithBlock:^(ARAPIClientResponse *response) {
            if (response && response.data) {
                success(response.data);
            }
            else
                failure(response.error);
        }];
    }
}
@end
