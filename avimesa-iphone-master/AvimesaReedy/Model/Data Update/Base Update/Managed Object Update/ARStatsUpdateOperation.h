//
//  ARStatsUpdateOperation.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARManagedObjectUpdateOperation.h"

@interface ARStatsUpdateOperation : ARManagedObjectUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext reedyID:(NSInteger)reedyID delegate:(id<ARManagedObjectUpdateDelegate>)delegate;
@end
