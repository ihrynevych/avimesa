//
//  ARStatsUpdateOperation.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARStatsUpdateOperation.h"
#import "ARAPIClient.h"
static NSString *kStatsJsonKey = @"Stats";
static NSString *kStatsRequestnKey = @"Stats";
static NSString *ktodayKey = @"today";
static NSString *kyesterdayKey = @"yesterday";
static NSString *kthis_weekKey = @"this_week";
static NSString *klast_weekKey = @"last_week";
static NSString *kthis_monthKey = @"this_month";
static NSString *klast_monthKey = @"last_month";
static NSString *kopenedKey = @"opened";
static NSString *kclosedKey = @"closed";


@interface ARStatsUpdateOperation()
@property (nonatomic) NSInteger reedyID;

@end

@implementation ARStatsUpdateOperation
- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext reedyID:(NSInteger)reedyID delegate:(id<ARManagedObjectUpdateDelegate>)delegate
{
    self = [super initWithContext:managedObjectContext jsonKey:kStatsJsonKey requestnKey:kStatsRequestnKey entityName:NSStringFromClass([Stats class]) delegate:delegate];
    if (self) {
        self->_reedyID = reedyID;
    }
    return self;
}

- (void)handleDictionary:(id)data
{
    if (self.isCancelled == YES) return;
    
    if (data&&[data isKindOfClass:[NSDictionary class]]) {
        [self updateRelationshipsForObject:data oldObject:(Stats*)[self.managedObjectContext fetchObjectWithEntityName:NSStringFromClass([Stats class]) predicate:nil]];
        //        [self setupRelationshipsForObjects:updatedObjects updates:dictionary[self.jsonKey]];
        [self updateSuccess];
    } else {
        [self updateSuccess];
    }
}
- (void)updateRelationshipsForObject:(NSDictionary*)object oldObject:(Stats *)oldObject
{
    Stats *stats;
    if (oldObject) {
        if (oldObject.reedyID.integerValue!=self.reedyID)
        {
            [self.managedObjectContext deleteObject:oldObject];
        }
        else
        {
            stats = oldObject;
        }
    }
    
    if (!stats) {
        stats = (Stats *)[self.managedObjectContext createObjectWithEntityName:NSStringFromClass([Stats class])];
        stats.reedyID = @(self.reedyID);
    }

    stats.today = [self updateStatsElement:stats.today withDictionary:[object valueForKey:ktodayKey]];
    stats.yesterday = [self updateStatsElement:stats.yesterday withDictionary:[object valueForKey:kyesterdayKey]];
    stats.this_week = [self updateStatsElement:stats.this_week withDictionary:[object valueForKey:kthis_weekKey]];
    stats.last_week = [self updateStatsElement:stats.last_week withDictionary:[object valueForKey:klast_weekKey]];
    stats.this_month = [self updateStatsElement:stats.this_month withDictionary:[object valueForKey:kthis_monthKey]];
    stats.last_month = [self updateStatsElement:stats.last_month withDictionary:[object valueForKey:klast_monthKey]];
    
}

-(StatsElement*)updateStatsElement:(StatsElement*)statsElement withDictionary:(NSDictionary*)dictionary
{
    StatsElement * objectToUpdate = statsElement;
    if (dictionary) {
        
        if (!objectToUpdate||![self validateStatsElement:objectToUpdate]) {
            objectToUpdate = (StatsElement *)[self.managedObjectContext createObjectWithEntityName:NSStringFromClass([StatsElement class])];
        }
        [self fillStatsElement:objectToUpdate withDictionary:dictionary];
        return objectToUpdate;
    }
    else{
        if (objectToUpdate) {
            [self.managedObjectContext deleteObject:objectToUpdate];
        }
        return nil;
    }
}

-(BOOL)validateStatsElement:(StatsElement*)statsElement
{
    if([self.managedObjectContext existingObjectWithID:statsElement.objectID error:nil])
        return YES;
    else
        return NO;
}
-(void)fillStatsElement:(StatsElement*)statsElement withDictionary:(NSDictionary*)dictionary
{
    statsElement.opened = [dictionary valueForKey:kopenedKey];
    statsElement.closed = [dictionary valueForKey:kclosedKey];
    
}
- (void)updateRequestWithSuccess:(void (^) (NSDictionary *jsonDictionary))success failure:(void (^) (NSError *error))failure
{
    if (self.requestnKey) {
        [[ARAPIClient sharedClient] statsRequestWithReedyID:self.reedyID withBlock:^(ARAPIClientResponse *response) {
            if (response && response.data) {
                success(response.data);
            }
            else
                failure(response.error);
        }];
    }
}
@end
