//
//  ACManagedObjectUpdateOperation.h
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Helper.h"
#import "NSDictionary+Helper.h"
#import "NSManagedObjectContext+Helper.h"
#import "NSManagedObject+Helper.h"
#import "Reedy.h"
#import "Location.h"
#import "Hub.h"
#import "Activity.h"
#import "Stats.h"
#import "StatsElement.h"


typedef NS_ENUM (NSUInteger, AGErrorCode) {
    AGUpdateError
};

@class ARManagedObjectUpdateOperation;

@protocol ARManagedObjectUpdateDelegate <NSObject>
- (void)managedObjectUpdateCompletedForOperation:(NSOperation*)operation;
- (void)managedObjectUpdateFailedWithError:(NSError*)error forOperation:(NSOperation*)operation;
@end


@interface ARManagedObjectUpdateOperation : NSOperation

@property (nonatomic,strong) NSString *entityName;
@property (nonatomic,strong) NSString *jsonKey;
@property (nonatomic,strong) NSString *requestnKey;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;

// ========= Public methods =============

- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext delegate:(id<ARManagedObjectUpdateDelegate>)delegate;

// ========= Subclass methods =============

- (id)initWithContext:(NSManagedObjectContext*)managedObjectContext
              jsonKey:(NSString*)jsonKey
          requestnKey:(NSString*)requestnKey
           entityName:(NSString*)entityName
             delegate:(id <ARManagedObjectUpdateDelegate>)delegate;

- (void)performUpdates;
- (void)updateSuccess;
- (void)updateFailed:(NSError*)error;
- (void)handleDictionary:(NSDictionary*)dictionary;

/**
 Subclass relationships setup.
 @note To be overriden by subclass if relationship setup needed.
 */
- (void)setupRelationshipsForObjects:(NSArray*)objects updates:(NSArray*)updates;

@end
