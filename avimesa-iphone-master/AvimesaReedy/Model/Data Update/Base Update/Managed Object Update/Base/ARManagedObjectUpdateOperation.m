//
//  ACManagedObjectUpdateOperation.m
//
//  Created by Pavlo Yonak
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARManagedObjectUpdateOperation.h"

@interface ARManagedObjectUpdateOperation ()
@property (nonatomic,weak) id <ARManagedObjectUpdateDelegate> delegate;
@end

@implementation ARManagedObjectUpdateOperation

- (id)initWithContext:(NSManagedObjectContext *)managedObjectContext delegate:(id<ARManagedObjectUpdateDelegate>)delegate
{
    //override in subclass
    self = [super init];
    if (self) {
        _delegate = delegate;
        _managedObjectContext = managedObjectContext;
    }
    return self;
}

- (id)initWithContext:(NSManagedObjectContext*)managedObjectContext jsonKey:(NSString*)jsonKey requestnKey:(NSString *)requestnKey  entityName:(NSString*)entityName delegate:(id <ARManagedObjectUpdateDelegate>)delegate
{
    self = [super init];
    if (self) {
        _requestnKey = requestnKey;
        _jsonKey = jsonKey;
        _entityName = entityName;
        _delegate = delegate;
        _managedObjectContext = managedObjectContext;
    }
    return self;
}

- (void)main
{
    @autoreleasepool {
        if (self.isCancelled == NO) {
            NSLog(@"starting update for %@", self.requestnKey);
            [self performUpdates];
        }
    }
}

- (void)performUpdates
{
    [self updateRequestWithSuccess:^(NSDictionary *jsonDictionary) {
        if (self.isCancelled) { return; }
        [self handleDictionary:jsonDictionary];
    } failure:^(NSError *error) {
        [self updateFailed:error];
    }];
}

- (void)updateSuccess
{
    if (self.isCancelled == NO) {
        if ([self.delegate respondsToSelector:@selector(managedObjectUpdateCompletedForOperation:)]) {
            [self.delegate managedObjectUpdateCompletedForOperation:self];
        }
    }
}

- (void)updateFailed:(NSError*)error
{
    
    NSLog(@"update failed for %@ %@", self.requestnKey, [error localizedDescription]);
    if (self.isCancelled == NO) {
        if ([self.delegate respondsToSelector:@selector(managedObjectUpdateFailedWithError:forOperation:)]) {
            [self.delegate managedObjectUpdateFailedWithError:error forOperation:self];
        }
    }
}

- (void)handleDictionary:(NSDictionary*)dictionary
{
    if (self.isCancelled == YES) return;
    
    if ([dictionary[self.jsonKey] count]) {
        NSArray *updatedObjects = [self.managedObjectContext updatedObjectsByIdentifierUsingUpdates:dictionary[self.jsonKey] entityName:self.entityName];
        [self setupRelationshipsForObjects:updatedObjects updates:dictionary[self.jsonKey]];
    } else {
        [self updateSuccess];
    }
}

- (void)setupRelationshipsForObjects:(NSArray*)objects updates:(NSArray*)updates
{
    /*
     override in subclass to setup Relationships, otherwise update is Completed here.
     */
    [self updateSuccess];
}

#pragma mark - API requests

- (void)updateRequestWithSuccess:(void (^) (NSDictionary *jsonDictionary))success failure:(void (^) (NSError *error))failure
{
    if (self.requestnKey) {
        
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithJSON:[NSString stringWithBundledJsonNamed:self.requestnKey]];
         if (dictionary) {
         success(dictionary);
         } else {
         failure(nil);
         }
    }
}



@end
