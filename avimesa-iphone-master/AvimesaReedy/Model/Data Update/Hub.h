//
//  Hub.h
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Hub : NSManagedObject

@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * location_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * is_enabled;
@property (nonatomic, retain) NSString * uuid;

@end
