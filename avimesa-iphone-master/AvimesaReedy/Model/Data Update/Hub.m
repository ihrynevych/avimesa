//
//  Hub.m
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "Hub.h"


@implementation Hub

@dynamic identifier;
@dynamic location_id;
@dynamic name;
@dynamic is_enabled;
@dynamic uuid;

@end
