//
//  Location.h
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * topic_arn;
@property (nonatomic, retain) NSString * image;

@end
