//
//  Location.m
//  AvimesaReedy
//
//  Created by Alex on 9/18/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "Location.h"


@implementation Location

@dynamic identifier;
@dynamic name;
@dynamic topic_arn;
@dynamic image;

@end
