//
//  Reedy.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Reedy : NSManagedObject

@property (nonatomic, retain) NSNumber * battery_level;
@property (nonatomic, retain) NSNumber * location_id;
@property (nonatomic, retain) NSNumber * device_id;
@property (nonatomic, retain) NSNumber * hub_id;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSString * mac_address;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * notification_sound;
@property (nonatomic, retain) NSNumber * state;
@property (nonatomic, retain) NSDate   * state_change_time;
@property (nonatomic, retain) NSNumber * notification_threshold;
@property (nonatomic, retain) id notification_types;


@end
@interface Notification_types : NSValueTransformer

@end