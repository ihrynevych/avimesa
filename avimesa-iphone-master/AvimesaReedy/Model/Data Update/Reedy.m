//
//  Reedy.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "Reedy.h"


@implementation Reedy

@dynamic battery_level;
@dynamic device_id;
@dynamic hub_id;
@dynamic identifier;
@dynamic mac_address;
@dynamic name;
@dynamic state;
@dynamic state_change_time;
@dynamic notification_threshold;
@dynamic notification_sound;
@dynamic location_id;
@dynamic notification_types;

@end
@implementation Notification_types

+ (Class)transformedValueClass
{
    return [NSArray class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    return [NSKeyedArchiver archivedDataWithRootObject:value];
}

- (id)reverseTransformedValue:(id)value
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}
@end
