//
//  StatsElement.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StatsElement : NSManagedObject

@property (nonatomic, retain) NSNumber * opened;
@property (nonatomic, retain) NSNumber * closed;

@end
