//
//  Stats.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StatsElement;

@interface Stats : NSManagedObject

@property (nonatomic, retain) NSNumber * reedyID;
@property (nonatomic, retain) StatsElement *today;
@property (nonatomic, retain) StatsElement *yesterday;
@property (nonatomic, retain) StatsElement *this_week;
@property (nonatomic, retain) StatsElement *last_week;
@property (nonatomic, retain) StatsElement *this_month;
@property (nonatomic, retain) StatsElement *last_month;

@end
