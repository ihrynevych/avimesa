//
//  Stats.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "Stats.h"
#import "StatsElement.h"


@implementation Stats

@dynamic reedyID;
@dynamic today;
@dynamic yesterday;
@dynamic this_week;
@dynamic last_week;
@dynamic this_month;
@dynamic last_month;

@end
