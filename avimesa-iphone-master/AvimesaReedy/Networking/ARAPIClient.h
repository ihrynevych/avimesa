//
//  JMAPIClient.h
//  JostMobile
//
//  Created by Pavlo Yonak on 5/26/14.
//  Copyright (c) 2014 Lemberg Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@class ARAPIClientResponse;
@interface ARAPIClient : NSObject
+(ARAPIClient *)sharedClient;

//base  request methods
- (void)headRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;

- (void)postRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;

- (void)putRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;

- (void)getRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;

- (void)deleteRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;


//request with json
- (void)postJSON:(NSDictionary*)JSON withRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;

- (void)putJSON:(NSDictionary *)JSON withRequestPath:(NSString *)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;

//location
- (void)postLocationWithRequestPath:(NSString *)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block;
- (void)locationsRequestWithBlock:(void (^)(ARAPIClientResponse *response))block;

//reedy
- (void)reediesRequestWithReedyID:(NSInteger)reedyID withBlock:(void (^)(ARAPIClientResponse * response))block;
- (void)reediesRequestWithBlock:(void (^)(ARAPIClientResponse *response))block;

//reedy detail
- (void)statsRequestWithReedyID:(NSInteger)reedyID withBlock:(void (^)(ARAPIClientResponse *response))block;
- (void)activityRequestWithReedyID:(NSInteger)reedyID withBlock:(void (^)(ARAPIClientResponse *response))block;

// hub
- (void)hubsRequestWithBlock:(void (^)(ARAPIClientResponse *response))block;

//profile
- (void)profileRequestWithBlock:(void (^)(ARAPIClientResponse *response))block;

//deprecated
- (void)reediesLongPolingRequestWithParameters:(NSDictionary*)parameters WithBlock:(void (^)(ARAPIClientResponse *response))block;

- (void)cancelReediesRequest;


@end

@interface ARAPIClientResponse: NSObject

@property (nonatomic,strong,readonly) id data;
@property (nonatomic,strong,readonly) NSDictionary * headers;
@property (nonatomic,readonly) NSInteger statusCode ;
@property (nonatomic,readonly) NSError* error;

@end;
