//
//  JMAPIClient.m
//  JostMobile
//
//  Created by Pavlo Yonak on 5/26/14.
//  Copyright (c) 2014 Lemberg Solutions. All rights reserved.
//

#import "ARAPIClient.h"
#import "NSDictionary+Helper.h"
#import "NSManagedObjectContext+Helper.h"
#import "NSString+Helper.h"

#pragma mark ARAPIClientResponse

@implementation ARAPIClientResponse
-(id)initWithResponceObject:(id)data operation:(AFHTTPRequestOperation*) operation andError:(NSError*)error
{
    self = [super init];
    if(self)
    {
        self->_data = data;
        self->_statusCode = operation.response.statusCode;
        self->_error = error;
        self->_headers = [operation.response allHeaderFields];
    }
    return self;
}

-(NSString*)description
{
    NSString* desc = [NSString stringWithFormat:@"status code:%ld\ndata:%@\nerror code:%@",(long)self.statusCode,self.data,self.error];
    return desc;
}
@end

@interface ARAPIClient()
@property (nonatomic,strong) AFHTTPRequestOperationManager* afOperationManager;
//@property (nonatomic,strong) AFHTTPRequestOperationManager* afReqestClient;
@end;

@implementation ARAPIClient
+(ARAPIClient *)sharedClient
{
    static ARAPIClient * __sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:ARAPIBaseURL]];
    });
    
    return __sharedInstance;
}

-(id)initWithBaseURL:(NSURL *)url
{
    self = [super init];
    
    if (self) {
        self.afOperationManager = [AFHTTPRequestOperationManager manager];
//        self.afOperationManager.requestSerializer = [AFJSONRequestSerializer serializer];
//        self.afOperationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

#pragma mark - base
-(void)putJSON:(NSDictionary *)JSON withRequestPath:(NSString *)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *))block
{
    [self requestWithMethod:@"PUT" path:path withJSONBody:JSON timeout:nil parameters:parameters multipart:NO block:block callbackQueue:nil];
}

- (void)requestOfType:(NSString*)type withPath:(NSString*)path andParameters:(NSDictionary*)parameters
{
    [self.afOperationManager.requestSerializer  requestWithMethod:type URLString:path parameters:parameters error:nil];
}

- (void)headRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block
{
    [self requestWithMethod:@"HEAD" path:path parameters:parameters block:block callbackQueue:nil];
}

- (void)postRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block
{
    [self requestWithMethod:@"POST" path:path parameters:parameters block:block callbackQueue:nil];
}
- (void)putRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block
{
    [self requestWithMethod:@"PUT" path:path parameters:parameters block:block callbackQueue:nil];
}

- (void)getRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block
{
    [self requestWithMethod:@"GET" path:path parameters:parameters block:block callbackQueue:nil];
}

- (void)deleteRequestPath:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block
{
    [self requestWithMethod:@"DELETE" path:path parameters:parameters block:block callbackQueue:nil];
}

-(void)postJSON:(NSDictionary *)JSON withRequestPath:(NSString *)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *))block
{
    [self requestWithMethod:@"POST" path:path withJSONBody:JSON timeout:nil parameters:parameters multipart:NO block:block callbackQueue:nil];
}

-(void)deleteJSON:(NSDictionary *)JSON withRequestPath:(NSString *)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *))block
{
    [self requestWithMethod:@"DELETE" path:path withJSONBody:JSON timeout:nil parameters:parameters multipart:NO block:block callbackQueue:nil];
}

- (void)requestWithMethod:(NSString*)method path:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block callbackQueue:(dispatch_queue_t)callbackQueue
{
    [self requestWithMethod:method path:path withJSONBody:nil timeout:nil parameters:parameters multipart:NO block:block callbackQueue:callbackQueue];
}
#pragma mark - deprecated
- (void)longPolingRequestWithMethod:(NSString*)method path:(NSString*)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *response))block callbackQueue:(dispatch_queue_t)callbackQueue
{
    [self requestWithMethod:method path:path withJSONBody:nil timeout:@(120.0f) parameters:parameters multipart:NO block:block callbackQueue:callbackQueue];
}

-(void)reediesLongPolingRequestWithParameters:(NSDictionary*)parameters WithBlock:(void (^)(ARAPIClientResponse *))block
{
    
    [self longPolingRequestWithMethod:@"GET" path:@"/api/v1/reedies" parameters:parameters block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}

-(void)cancelReediesRequest
{
//    NSOperation *operation = [[self.afOperationManager.operationQueue operations] lastObject];
//    if (operation !=nil)
//    {
//        [operation cancel];
//        [operation waitUntilFinished];
//        operation = nil;
//    }
}

#pragma mark - objects requests

-(void)reediesRequestWithReedyID:(NSInteger)reedyID withBlock:(void (^)(ARAPIClientResponse *))block
{
    
    [self requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api/v1/reedies/%ld",(long)reedyID]  parameters:nil block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}
-(void)reediesRequestWithBlock:(void (^)(ARAPIClientResponse *))block
{
    
    [self requestWithMethod:@"GET" path:@"/api/v1/reedies" parameters:nil block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}
- (void)statsRequestWithReedyID:(NSInteger)reedyID withBlock:(void (^)(ARAPIClientResponse *response))block
{
    
    [self requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api/v1/reedies/%ld/stats",(long)reedyID] parameters:nil block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}
- (void)activityRequestWithReedyID:(NSInteger)reedyID withBlock:(void (^)(ARAPIClientResponse *response))block
{
    
    [self requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api/v1/reedies/%ld/activity",(long)reedyID]  parameters:nil block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}

-(void)profileRequestWithBlock:(void (^)(ARAPIClientResponse *))block
{
    
    [self requestWithMethod:@"GET" path:@"/api/v1/profile" parameters:nil block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}

- (void)hubsRequestWithBlock:(void (^)(ARAPIClientResponse *response))block
{
    [self requestWithMethod:@"GET" path:@"/api/v1/hubs" parameters:nil block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}

- (void)locationsRequestWithBlock:(void (^)(ARAPIClientResponse *response))block
{
    
    [self requestWithMethod:@"GET" path:@"/api/v1/locations" parameters:nil block:block callbackQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
}

-(void)postLocationWithRequestPath:(NSString *)path parameters:(NSDictionary *)parameters block:(void (^)(ARAPIClientResponse *))block
{
    [self requestWithMethod:@"POST" path:path withJSONBody:nil timeout:nil parameters:parameters multipart:YES block:block callbackQueue:nil];
}

#pragma mark - private
- (void)requestWithMethod:(NSString*)method path:(NSString*)path withJSONBody:(NSDictionary *)json timeout:(NSNumber *)timeout parameters:(NSDictionary *)parameters multipart:(BOOL)multipart block:(void (^)(ARAPIClientResponse *response))block callbackQueue:(dispatch_queue_t)callbackQueue
{
    [self requestWithMethod:method path:[NSString stringWithFormat:@"%@%@",ARAPIBaseURL, path] JSONBody:json timeout:timeout parameters:parameters
                    multipart:multipart success:^(__unused AFHTTPRequestOperation *operation, id JSON){
        [operation.response allHeaderFields];
        if (block) {
            ARAPIClientResponse* responce = [[ARAPIClientResponse alloc] initWithResponceObject:JSON operation:operation andError:nil];
            block(responce);
        }
    } failure:^(__unused AFHTTPRequestOperation *operation, NSError *error){
        
        NSLog(@"APIClient method:%@ request: %@ failed with body:%@ , error: %@",method,path,json,[error localizedDescription]);
        
        NSDictionary *responseDict = operation.responseData ? [NSDictionary dictionaryWithJSON:[[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding]] : nil;
        
        if (responseDict||error)
        {
            NSString* message = @"";
            
            // error message
            if(responseDict && responseDict[@"errors"] && [responseDict[@"errors"] count])
            {
                if ([responseDict[@"errors"]  isKindOfClass:[NSArray class]])
                    message = responseDict[@"errors"][0];
            }else{
                message = [error localizedDescription];
            }
            
            
            if (error && operation.response.statusCode == 401)
            {
                //logout
                [[NSNotificationCenter defaultCenter] postNotificationName:AR_USER_LOGIN_NOT_VALID object:nil userInfo:nil ];
            }
            else
            {
                //show error
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Operation failed"
                                                                     message:message
                                                                    delegate:nil
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles: nil] ;
                if ([[operation.request.URL absoluteString] containsSubstring:@"/api/v1/reedies"])
                {
                    if (![UIAlertView isAnyVisible])
                    {
                        [alertView show];
                    }
                }
                else
                    [alertView show];
            }     
            
        }
        if (block) {
            ARAPIClientResponse* responce = [[ARAPIClientResponse alloc] initWithResponceObject:responseDict operation:operation andError:error];
            block(responce);
        }
    } callbackQueue:callbackQueue];
}

- (void)requestWithMethod:(NSString*)method path:(NSString *)path
               parameters:(NSDictionary *)parameters
                  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
            callbackQueue:(dispatch_queue_t)callbackQueue
{
	[self requestWithMethod:method path:path JSONBody:nil  timeout:nil parameters:parameters multipart:NO  success:success failure:failure callbackQueue:callbackQueue];
}

- (void)requestWithMethod:(NSString*)method path:(NSString *)path JSONBody:(NSDictionary *)json
               timeout:(NSNumber *)timeout
               parameters:(NSDictionary *)parameters
                multipart:(BOOL)multipart
                  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
            callbackQueue:(dispatch_queue_t)callbackQueue
{
    NSMutableURLRequest *request;
    if(multipart)
    {
        //multipart image send
        NSMutableDictionary* newParameters = [parameters mutableCopy];
        NSData *imageData = [parameters valueForKey:@"image"];
        if (imageData) {
            [newParameters removeObjectForKey:@"image"];
        }
        
        request = [self.afOperationManager.requestSerializer multipartFormRequestWithMethod:method URLString:path parameters:newParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
             [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.png" mimeType:@"image/png"];
        } error:nil ];
        
    }else if ([parameters valueForKey:IfModifiedSinceKey])
    {
        //deprecated
        //modified key
        NSMutableDictionary* tempDictionary = [parameters mutableCopy];
        id headerValue = [parameters valueForKey:IfModifiedSinceKey];
        [tempDictionary removeObjectForKey:IfModifiedSinceKey];
        request = [self.afOperationManager.requestSerializer requestWithMethod:method URLString:path parameters:tempDictionary error:nil];
        [request setValue:headerValue forHTTPHeaderField:IfModifiedSinceKey];
    }
    else
    {
        request = [self.afOperationManager.requestSerializer requestWithMethod:method URLString:path parameters:parameters error:nil];
    }
    
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    //set custom timeout
    if (timeout) {
        [request setTimeoutInterval:[timeout doubleValue]];
    }
    
    // set json body
    if (json) {
        NSError *error = nil;
        NSData *bodyData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
        if (!error&&bodyData) {
            [request setHTTPBody:bodyData];
        }
        
    }
    
    //init operation
	AFHTTPRequestOperation *operation = [self.afOperationManager HTTPRequestOperationWithRequest:request success:success failure:failure];

    [self.afOperationManager.operationQueue addOperation:operation];
}

@end
