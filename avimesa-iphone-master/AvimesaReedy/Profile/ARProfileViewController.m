//
//  ARProfileViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARProfileViewController.h"
#import "ARDeviceManager.h"
#import "ARAPIClient.h"
#import "UITextField+TextPadding.h"

@interface ARProfileViewController ()<UITextFieldDelegate>
{
    NSDictionary * profileJSON;
    NSString * name;
    NSString * email;
    NSString * phone;
}
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation ARProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set padding
    [self.nameField setLeftPadding:10];
    [self.emailField setLeftPadding:10];
    [self.phoneNumber setLeftPadding:10];
    
    self.saveButton.enabled = false;
    
    //update profile
    [[ARAPIClient sharedClient] profileRequestWithBlock:^(ARAPIClientResponse *response) {
        if (response.data&& !response.error) {
            if ([response.data valueForKey:@"name"]) {
                self.nameField.text =[response.data valueForKey:@"name"];
                name =[response.data valueForKey:@"name"];
            }
            if ([response.data valueForKey:@"email"]) {
                self.emailField.text =[response.data valueForKey:@"email"];
                email =[response.data valueForKey:@"email"];
            }
            if ([response.data valueForKey:@"phone"]) {
                self.phoneNumber.text =[response.data valueForKey:@"phone"];
                phone =[response.data valueForKey:@"phone"];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 1) {
        if (self.nameField.text&&![self.nameField.text isEqualToString:name]) {
            self.saveButton.enabled = true;
        }
        [self.emailField becomeFirstResponder];
    }
    else if (textField.tag == 2) {
        [self.phoneNumber becomeFirstResponder];
        if (self.phoneNumber.text&&![self.phoneNumber.text isEqualToString:phone]) {
            self.saveButton.enabled = true;
        }
    }
    else
    {
        [textField resignFirstResponder];
        if (self.emailField.text&&![self.emailField.text isEqualToString:email]) {
            self.saveButton.enabled = true;
        }
    }
    
    return YES;
}

#pragma mark - actions

- (IBAction)saveAction:(id)sender {
    self.saveButton.enabled = false;
    //update dictionary
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];

    if (self.emailField.text&&[self.emailField.text length]) {
        [dictionary setObject:self.emailField.text forKey:@"email"];
        email = self.emailField.text;
    }else
    {
        [dictionary setObject:@"" forKey:@"email"];
        email = @"";
    }
    if (self.nameField.text&&[self.nameField.text length]) {
        [dictionary setObject:self.nameField.text forKey:@"name"];
        name = self.nameField.text;
    }
    else
    {
        [dictionary setObject:@"" forKey:@"name"];
        name = @"";
    }
    if (self.phoneNumber.text&&[self.phoneNumber.text length]) {
        [dictionary setObject:self.phoneNumber.text forKey:@"phone"];
        phone = self.phoneNumber.text;
    }else
    {
        [dictionary setObject:@"" forKey:@"phone"];
        phone = @"";
    }
    
    //perform update
    [[ARDeviceManager sharedManager] updateProfileWithDictionary:dictionary callback:^(BOOL isSucceess, id identifier) {
        if (isSucceess) {
            self.saveButton.enabled = false;
        }
        else
            self.saveButton.enabled = true;
    }];
    
    
}
@end
