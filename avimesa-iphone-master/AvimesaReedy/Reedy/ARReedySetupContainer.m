//
//  ARReedySetupContainer.m
//  AvimesaReedy
//
//  Created by Volodymyr Hyrka on 1/30/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARReedySetupContainer.h"
#import "ARReedyListViewController.h"

@interface ARReedySetupContainer ()

@property (nonatomic, weak) IBOutlet UILabel * noReedyLabel;
@property (nonatomic, weak) IBOutlet UILabel * plusInfoLabel;
@property (nonatomic, weak) IBOutlet UIButton * addReedyButton;

- (IBAction)onAddButton:(UIButton*)sender;

@end

@implementation ARReedySetupContainer

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_noReedyLabel setFont:[UIFont fontWithName:@"Lato-Light" size:24.0]];
    [_plusInfoLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15.0]];
    [_addReedyButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18.0]];
}

#pragma mark - IBActions

- (IBAction)onAddButton:(UIButton *)sender
{
    id serchObj = self;
    while (serchObj)
    {
        serchObj = [serchObj parentViewController];
        if ([serchObj isKindOfClass:[ARReedyListViewController class]])
        {
            [(ARReedyListViewController*)serchObj addReedy:self];
            break;
        }
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

@end
