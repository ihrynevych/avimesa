//
//  ARReedyDetailCell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Activity;

@interface ARReedyDetailCell : UITableViewCell
@property (nonatomic,strong) Activity * activity;
@end

