//
//  ARReedyDetailCell.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReedyDetailCell.h"
#import "Activity.h"
@interface ARReedyDetailCell ()

@property (weak, nonatomic) IBOutlet UIImageView *stateImage;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

@end

@implementation ARReedyDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.topLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:14.0f]];
    [self.bottomLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:8.0f]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setActivity:(Activity *)activity
{
     self->_activity = activity;
    
    if ([activity.changed_state_to boolValue]) {
        [self.stateImage setImage:[UIImage imageNamed:@"open_btn"]];
    }
    else
    {
        [self.stateImage setImage:[UIImage imageNamed:@"closed_btn"]];
    }
    
    if (activity.timestamp) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"EEE MMM dd";
        self.topLabel.text = [formatter stringFromDate:activity.timestamp];
        formatter.dateFormat = @"HH:mm";
        self.bottomLabel.text = [formatter stringFromDate:activity.timestamp];
    }
}
@end
