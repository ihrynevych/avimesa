//
//  ARReedyDetailController.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Reedy;
@interface ARReedyDetailController : UIViewController
@property (strong, nonatomic) Reedy *reedy;
-(void)update;
@end
