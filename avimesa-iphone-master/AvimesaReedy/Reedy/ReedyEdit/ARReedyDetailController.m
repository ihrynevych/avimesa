//
//  ARReedyDetailController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReedyDetailController.h"
#import "ARAppDelegate.h"
#import "Reedy.h"
#import "Stats.h"
#import "StatsElement.h"
#import "Activity.h"
#import "ARReedyDetailCell.h"
#import "NSManagedObjectContext+Helper.h"
#import "ARReedySetingsViewController.h"
#import "ARCoreDataStack.h"

@interface ARReedyDetailController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSTimer * updateTimer;
    BOOL  isVisible;
    NSInteger reedyID;
    BOOL reedyState;
    NSDate *reedyUpdateDate;
    CGFloat widthConstant;
}

@property (weak, nonatomic) IBOutlet UILabel *totalTimesOpenedLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLogLabel;

@property (weak, nonatomic) IBOutlet UILabel *todayLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;

@property (weak, nonatomic) IBOutlet UILabel *todayBottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekBottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthBottomLabel;


@property (weak, nonatomic) IBOutlet UILabel *mainLabel;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UIImageView *stateImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mailWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notificationWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *smsWidth;

@property (weak, nonatomic) IBOutlet UIView *activityView;

@end

@implementation ARReedyDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.mainLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21.0f]];
    [self.totalTimesOpenedLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:11.0f]];
    [self.statusLogLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:14.0f]];
    
    [self.todayLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:40.0f]];
    [self.weekLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:40.0f]];
    [self.monthLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:40.0f]];
    
    [self.todayBottomLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:11.0f]];
    [self.weekBottomLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:11.0f]];
    [self.monthBottomLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:11.0f]];
    
    widthConstant = self.mailWidth.constant;
    self.navigationItem.titleView = [ARSideMenuController logo];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(back:)];
    [backButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Lato-Regular" size:14.0],
                                             NSForegroundColorAttributeName: [UIColor whiteColor]}
                                  forState:UIControlStateNormal];
    backButtonItem.title = @"Back";
    [self.navigationItem setLeftBarButtonItem:backButtonItem];
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(editSelected:)];
    menuButtonItem.title = @"Edit";
    [menuButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Lato-Regular" size:14.0],
                                             NSForegroundColorAttributeName: [UIColor whiteColor]}
                                  forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:menuButtonItem];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self hideActivityView:NO];
    if (reedyID<=0 && self.reedy.identifier) {
        reedyID = [self.reedy.identifier integerValue];
    }
    [super viewWillAppear:animated];
    isVisible = YES;
    [self updateReedy];
    [self checkUpdate];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    isVisible = NO;
    [self _cancelTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - updates timers

-(void)_cancelTimer
{
    if (updateTimer)
    {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}
-(void)_initTimer
{
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(checkUpdate) userInfo:nil repeats:NO];
}

#pragma mark - private

-(void)checkUpdate
{
    [self _cancelTimer];
    [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateReedyWithReedyID:reedyID completion:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isVisible) {
                [self _initTimer];
                [self updateReedy];
            }
        });
    }];
}

-(void)update
{
    [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateStatsWithReedyID:self.reedy.identifier.integerValue completion:^(BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{[self reloadData];
            [self updateStats];
            [self hideActivityView:YES];
        });
        
    }];
}

-(void)updateReedy
{
    
    Reedy * reedy = (Reedy*)[[NSManagedObjectContext mainQContext] fetchObjectWithEntityName:@"Reedy" predicate:[NSPredicate predicateWithFormat:@"identifier == %ld",reedyID]];
    if (reedyID>0&&reedy) {
        self.reedy = reedy;
        [self updateReedyState];
        if (!reedyUpdateDate || [reedyUpdateDate compare:reedy.state_change_time]!=NSOrderedSame || reedyState!=reedy.state.boolValue) {
            NSLog(@"update stats");
            reedyState = reedy.state.boolValue;
            reedyUpdateDate = reedy.state_change_time;
            [self update];
        }
        else
        {
            [self hideActivityView:YES];
        }
    }
    else
    {
        [self hideActivityView:YES];
    }
}

-(void)updateStats
{
    Stats * stats= (Stats * )[[NSManagedObjectContext mainQContext] fetchObjectWithEntityName:@"Stats" predicate:nil];
    if(stats)
    {
        self.todayLabel.text = [NSString stringWithFormat:@"%ld",(long)stats.today.opened.integerValue];
        self.weekLabel.text = [NSString stringWithFormat:@"%ld",(long)stats.this_week.opened.integerValue];
        self.monthLabel.text = [NSString stringWithFormat:@"%ld",(long)stats.this_month.opened.integerValue];
    }
}
-(void)updateReedyState
{
    if (![self.reedy.state boolValue]) {
        [self.stateImageView setImage:[UIImage imageNamed:@"close_big"]];
    }
    else
    {
        [self.stateImageView setImage:[UIImage imageNamed:@"open_big"]];
    }
    if (self.reedy && self.reedy.notification_types) {
        NSArray * notificationTypes = self.reedy.notification_types;
        self.smsWidth.constant = 0.0f;
        self.notificationWidth.constant = 0.0f;
        self.mailWidth.constant = 0.0f;
        for (NSString * type in notificationTypes) {
            if ([type isEqualToString:@"push"])
                self.notificationWidth.constant = widthConstant;
            if ([type isEqualToString:@"email"])
                self.mailWidth.constant = widthConstant;
            if ([type isEqualToString:@"sms"])
                self.smsWidth.constant = widthConstant;
        }
        
    }
    self.mainLabel.text = self.reedy.name;
}

-(IBAction)editSelected:(id)sender
{
    [self performSegueWithIdentifier:@"ReedyDetailToSettings" sender:nil];
}

-(void)reloadData
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Activity"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO];
    [request setSortDescriptors:@[ sortDescriptor ]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext mainQContext] sectionNameKeyPath:nil cacheName:@"activities"];
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    NSAssert(!error, @"error: %@", error);
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ARReedyDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARReedyDetailCell"];
    
    Activity *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.activity= activity;
    return cell;
}

#pragma mark - navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ReedyDetailToSettings"])
    {
        ARReedySetingsViewController* controller = [segue destinationViewController];
        controller.reedy = self.reedy;
    }
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)hideActivityView:(BOOL)hidden
{
    self.activityView.hidden = hidden;
}

@end
