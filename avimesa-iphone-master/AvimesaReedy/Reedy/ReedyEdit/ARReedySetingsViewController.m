//
//  ARReedySetingsViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/15/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReedySetingsViewController.h"
#import "Reedy.h"
#import "ARDeviceManager.h"
#import "UITextField+TextPadding.h"
#import "UIColor+Helper.h"


@interface ARReedySetingsViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    UIPickerView *picker;
    UIPickerView *soundPicker;
    NSInteger alertTimeinterval;
    NSMutableArray * notification_types;
    NSMutableArray * sounds;
    UIBarButtonItem *saveButtonItem;
}
@property (weak, nonatomic) IBOutlet UIButton *pushButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *smsButton;
@property (weak, nonatomic) IBOutlet UITextField *reedyName;
@property (weak, nonatomic) IBOutlet UITextField *timeSelector;
@property (weak, nonatomic) IBOutlet UITextField *soundSelector;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *selecrNotificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectAlertIntervalLabel;
@property (weak, nonatomic) IBOutlet UILabel *alertThesholdLabel;
@property (weak, nonatomic) IBOutlet UILabel *alertMeLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeSoundButton;
@property (weak, nonatomic) IBOutlet UIButton *changeIntervalButton;

@end

@implementation ARReedySetingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initIntervalInputView];
    [self initSoundInputView];
    [self.reedyName setLeftPadding:15];
    //custom fonts
    
    [self.selectAlertIntervalLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:11.0f]];
    [self.alertThesholdLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:11.0f]];
    [self.selecrNotificationLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:11.0f]];
    [self.alertMeLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:10.0f]];
    
    self.smsButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:11.0f];
    self.pushButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:11.0f];
    self.emailButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:11.0f];
    
    self.changeIntervalButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:10.0f];
    self.changeSoundButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:10.0f];
    
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:16.0f];
    
    self.reedyName.attributedText=
    [[NSAttributedString alloc] initWithString:@""
                                    attributes:@{
                                                 NSForegroundColorAttributeName: [UIColor colorFromHexString:@"#a2a2a2"],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                 }
     ];
    self.reedyName.attributedPlaceholder=
    [[NSAttributedString alloc] initWithString:@"name"
                                    attributes:@{
                                                 NSForegroundColorAttributeName: [UIColor colorFromHexString:@"#a2a2a2"],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                 }
     ];
   
    

    //navigation bar
    self.navigationItem.titleView = [ARSideMenuController logo];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(back:)];
    [backButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Lato-Regular" size:14.0],
                                             NSForegroundColorAttributeName: [UIColor whiteColor]}
                                  forState:UIControlStateNormal];
    backButtonItem.title = @"Back";
    [self.navigationItem setLeftBarButtonItem:backButtonItem];
    saveButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(saveAction:)];
    saveButtonItem.title = @"Save";
    [saveButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Lato-Regular" size:14.0],
                                             NSForegroundColorAttributeName: [UIColor whiteColor]}
                                  forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:saveButtonItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateNotificationsTypeButtons];
}

#pragma mark - private

-(void)updateNotificationsTypeArray
{
    notification_types = [NSMutableArray array];
    if (self.pushButton.selected) {
        [notification_types addObject:@"push"];
    }
    if (self.emailButton.selected) {
        [notification_types addObject:@"email"];
    }
    if (self.smsButton.selected) {
        [notification_types addObject:@"sms"];
    }
}

-(void)updateNotificationsTypeButtons
{
    if (self.reedy.notification_threshold) {
        alertTimeinterval = [self.reedy.notification_threshold integerValue];
        
        [self updateNotificationThresholdWithTimeInterval:self.reedy.notification_threshold.integerValue];
        [self updateThresholdPickerWithTimeInterval:self.reedy.notification_threshold.integerValue];
    }
    if (self.reedy.notification_sound) {
        self.soundSelector.attributedText=
        [[NSAttributedString alloc] initWithString:self.reedy.notification_sound
                                        attributes:@{
                                                     NSForegroundColorAttributeName: [UIColor colorFromHexString:@"#a2a2a2"],
                                                     NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:14.0]
                                                     }
         ];
    }
    self.reedyName.attributedText=
    [[NSAttributedString alloc] initWithString:[self.reedy.name length]?self.reedy.name:@""
                                    attributes:@{
                                                 NSForegroundColorAttributeName: [UIColor colorFromHexString:@"#a2a2a2"],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0]
                                                 }
     ];
    
    self.pushButton.selected = NO;
    self.emailButton.selected = NO;
    self.smsButton.selected = NO;
    
    if (self.reedy && self.reedy.notification_types) {
        
        NSArray * notificationTypes = self.reedy.notification_types;
        notification_types = [NSMutableArray array];
        
        for (NSString * type in notificationTypes) {
        
            [notification_types addObject:type];
            
            if ([type isEqualToString:@"push"])
                self.pushButton.selected = YES;
            
            if ([type isEqualToString:@"email"])
                self.emailButton.selected = YES;
            
            if ([type isEqualToString:@"sms"])
                self.smsButton.selected = YES;
        }
        
    }
    
}
-(void)updateNotificationThresholdWithTimeInterval:(NSInteger)timeInterval
{
    NSString *notificationThresholdText = @"";
    
    int hours = (int)(timeInterval/3600);
    int minutes = (int)((timeInterval-hours*3600)/60);
    int seconds = (int)(timeInterval-hours*3600-minutes*60);
    
    if (hours>0) {
         notificationThresholdText = [NSString stringWithFormat:@"after %d h/%d m/%d s",hours,minutes,seconds];
    }
    else if (minutes>0){
        notificationThresholdText = [NSString stringWithFormat:@"after %d m/%d s",minutes,seconds];
    }
    else{
        notificationThresholdText = [NSString stringWithFormat:@"after %d seconds",seconds];
    }
    
    self.timeSelector.attributedText=
    
    [[NSAttributedString alloc] initWithString:notificationThresholdText
                                    attributes:@{
                                                 NSForegroundColorAttributeName: [UIColor colorFromHexString:@"#a2a2a2"],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:14.0]
                                                 }
     ];
}
-(void)updateThresholdPickerWithTimeInterval:(NSInteger)timeInterval
{
    if (picker) {
        
        int hours = (int)(timeInterval/3600);
        int minutes = (int)((timeInterval-hours*3600)/60);
        int seconds = (int)(timeInterval-hours*3600-minutes*60);
        
        if (hours>24||hours<0) {
            [picker selectRow:0 inComponent:0 animated:NO];
        }
        else
        {
            [picker selectRow:hours inComponent:0 animated:NO];
        }
        
        if (minutes>60||minutes<0) {
            [picker selectRow:0 inComponent:1 animated:NO];
        }
        else
        {
            [picker selectRow:minutes inComponent:1 animated:NO];
        }
        
        if (seconds>60||seconds<0) {
            [picker selectRow:0 inComponent:2 animated:NO];
        }
        else
        {
            [picker selectRow:seconds inComponent:2 animated:NO];
        }
    }
   
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - actions

- (IBAction)saveAction:(id)sender {
    self.saveButton.enabled = false;
    saveButtonItem.enabled = false;
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:self.reedy.hub_id forKey:@"hub_id"];
    if (notification_types) {
        [dictionary setObject:notification_types forKey:@"notification_types"];
    }
    if (alertTimeinterval>=0) {
        [dictionary setObject:@(alertTimeinterval) forKey:@"notification_threshold"];
    }
    else
    {
        if (self.reedy.notification_threshold) {
            [dictionary setObject:self.reedy.notification_threshold forKey:@"notification_threshold"];
        }
    }
    if (self.soundSelector.text && [self.soundSelector.text length]) {
        [dictionary setObject:self.soundSelector.text forKey:@"notification_sound"];
    }
    
    if (self.reedyName.text&&[self.reedyName.text length]) {
        [dictionary setObject:self.reedyName.text forKey:@"name"];
    }
    
    [[ARDeviceManager sharedManager] updateReedyWithDictionary:dictionary reedyID:self.reedy.identifier.integerValue callback:^(BOOL isSucceess, id identifier, NSDictionary *response, NSError *error)  {
        self.saveButton.enabled = true;
        saveButtonItem.enabled = true;
        if (isSucceess) {
            NSInteger count =[self.navigationController.viewControllers count];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count-2] animated:YES];
        }//            [self showFailedSaveAlertForResponse:response andError:error];
        
    }];
    
    
}
- (IBAction)changeInteval:(id)sender {
    [self.timeSelector becomeFirstResponder];
}

- (IBAction)changeSound:(id)sender {
    [self.soundSelector becomeFirstResponder];
}

- (IBAction)notificationButtonSelect:(UIButton*)sender {
    sender.selected = !sender.selected;
    [self updateNotificationsTypeArray];
}


-(void)showFailedSaveAlertForResponse:(NSDictionary*)response andError:(NSError*) error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* message = @"";
        if(response[@"errors"]&&[response[@"errors"] count])
        {
            message = response[@"errors"][0];
        }else{
            message = [error localizedDescription];
        }
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Update failed"
                                                             message:message
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil] ;
        [alertView show];
    });
}

#pragma mark - UITextFieldDelegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Interval Selector

-(void)initIntervalInputView
{
    UIToolbar * inputAccessoryView = [[UIToolbar alloc] init];
    
    inputAccessoryView.barStyle = UIBarStyleBlackTranslucent;
    inputAccessoryView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [inputAccessoryView sizeToFit];
    
    CGRect frame = inputAccessoryView.frame;
    frame.size.height = 44.0f;
    
    inputAccessoryView.frame = frame;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePikerButtonAction:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPikerButtonAction:)];
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *array = [NSArray arrayWithObjects:cancelButton,flexibleSpaceLeft, doneButton, nil];
    [inputAccessoryView setItems:array];
    
    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 200)];;
    picker.delegate = self;
    picker.dataSource = self;
    
    UILabel *hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(42, picker.frame.size.height / 2 - 15, 75, 30)];
    hourLabel.text = @"hour";
    
    [picker addSubview:hourLabel];
    
    UILabel *minsLabel = [[UILabel alloc] initWithFrame:CGRectMake(42 + (picker.frame.size.width / 3), picker.frame.size.height / 2 - 15, 75, 30)];
    minsLabel.text = @"min";
    
    [picker addSubview:minsLabel];
    
    UILabel *secsLabel = [[UILabel alloc] initWithFrame:CGRectMake(42 + ((picker.frame.size.width / 3) * 2), picker.frame.size.height / 2 - 15, 75, 30)];
    secsLabel.text = @"sec";
    
    [picker addSubview:secsLabel];
    
    self.timeSelector.inputView = picker;
    self.timeSelector.inputAccessoryView = inputAccessoryView;
    
}

-(void)initSoundInputView
{
    UIToolbar * inputAccessoryView = [[UIToolbar alloc] init];
    
    inputAccessoryView.barStyle = UIBarStyleBlackTranslucent;
    inputAccessoryView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [inputAccessoryView sizeToFit];
    
    CGRect frame = inputAccessoryView.frame;
    frame.size.height = 44.0f;
    sounds = [NSMutableArray arrayWithObjects:@"default",@"Basso.aiff",@"Glass.aiff", nil];
    inputAccessoryView.frame = frame;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSoundPikerButtonAction:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelSoundPikerButtonAction:)];
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *array = [NSArray arrayWithObjects:cancelButton,flexibleSpaceLeft, doneButton, nil];
    [inputAccessoryView setItems:array];
    
    soundPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 200)];
    soundPicker.tag = 2;
    soundPicker.delegate = self;
    soundPicker.dataSource = self;
    
    self.soundSelector.inputView = soundPicker;
    self.soundSelector.inputAccessoryView = inputAccessoryView;
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView.tag == 2) {
        return 1;
    }
    else
        return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 2) {
        return [sounds count];
    }
    else
    {
        if(component == 0)
            return 24;
        
        return 60;
    }
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (pickerView.tag == 2) {
        UILabel *columnView = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, self.view.frame.size.width - 35, 30)];
        columnView.textAlignment = NSTextAlignmentCenter;
        columnView.text = sounds[row];
        
        return columnView;
    }
    else
    {
        UILabel *columnView = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, self.view.frame.size.width/3 - 35, 30)];
        columnView.textAlignment = NSTextAlignmentLeft;
        columnView.text = [NSString stringWithFormat:@"%ld",(long)row];
        
        return columnView;
    }
    
}

- (IBAction)donePikerButtonAction:(id)sender
{
    [self.timeSelector resignFirstResponder];
    NSInteger hoursInt = [picker selectedRowInComponent:0];
    NSInteger minsInt = [picker selectedRowInComponent:1];
    NSInteger secsInt = [picker selectedRowInComponent:2];
    
    alertTimeinterval = secsInt + (minsInt*60) + (hoursInt*3600);
    [self updateNotificationThresholdWithTimeInterval:alertTimeinterval];
    
}

- (IBAction)cancelPikerButtonAction:(id)sender
{
    [self.timeSelector resignFirstResponder];
}

- (IBAction)doneSoundPikerButtonAction:(id)sender
{
    [self.soundSelector resignFirstResponder];
    NSInteger index = [soundPicker selectedRowInComponent:0];
    self.soundSelector.attributedText=
    [[NSAttributedString alloc] initWithString:[sounds objectAtIndex:index]
                                    attributes:@{
                                                 NSForegroundColorAttributeName: [UIColor colorFromHexString:@"#a2a2a2"],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:14.0]
                                                 }
     ];
    
}

- (IBAction)cancelSoundPikerButtonAction:(id)sender
{
    [self.soundSelector resignFirstResponder];
}
@end
