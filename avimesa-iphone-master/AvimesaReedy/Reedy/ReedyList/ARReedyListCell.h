//
//  ARReedyListCell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/17/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Reedy;
@interface ARReedyListCell : UITableViewCell
@property (nonatomic,strong) Reedy * reedy;
@end
