//
//  ARReedyListCell.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/17/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReedyListCell.h"
#import "Reedy.h"
#import "UIColor+Helper.h"
static NSString*const kMintsKey =@"MINS";
static NSString*const kDaysKey =@"DAYS";
static NSString*const kHoursKey =@"HOURS";
static NSString*const kSecondsKey =@"SECONDS";
static NSString*const kMonthsKey =@"MONTHS";
@interface ARReedyListCell ()
{
    CGFloat widthConstant;
}

@property (weak, nonatomic) IBOutlet UIImageView *stateImage;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *batteryLabel;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mailWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notificationWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *smsWidth;

@end
@implementation ARReedyListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    widthConstant = self.mailWidth.constant;
    [self.stateLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:9.0f]];
    [self.name setFont:[UIFont fontWithName:@"Lato-Regular" size:19.0f]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)setReedy:(Reedy *)reedy
{
    self->_reedy = reedy;
    NSString * batteryText = @"";
    NSString * stateText = @"";
    if (reedy.battery_level) {
        batteryText = [batteryText stringByAppendingFormat:@"BATTERY %d",reedy.battery_level.intValue];
    }
    self.batteryLabel.text = batteryText;
    if (reedy.state_change_time) {
        NSDictionary * tempData = [self deltaFromDate:reedy.state_change_time];
        NSString *key = [[tempData allKeys] lastObject];
         stateText = [stateText stringByAppendingFormat:@"FOR %d %@",[tempData[key] intValue]>0 ? [tempData[key] intValue]:0,key];
    }
    self.stateLabel.text =  stateText;
    self.name.text = reedy.name;
    if ([reedy.state boolValue]) {
        self.name.textColor = [UIColor reedyListCellNameOpenColor];
        [self.stateImage setImage:[UIImage imageNamed:@"open_btn"]];
    }
    else
    {
        self.name.textColor = [UIColor reedyListCellNameClosedColor];
        [self.stateImage setImage:[UIImage imageNamed:@"closed_btn"]];
    }
    if (reedy && reedy.notification_types) {
        NSArray * notificationTypes = self.reedy.notification_types;
        self.smsWidth.constant = 0.0f;
        self.notificationWidth.constant = 0.0f;
        self.mailWidth.constant = 0.0f;
        for (NSString * type in notificationTypes) {
            if ([type isEqualToString:@"push"])
                self.notificationWidth.constant = widthConstant;
            if ([type isEqualToString:@"email"])
                self.mailWidth.constant = widthConstant;
            if ([type isEqualToString:@"sms"])
                self.smsWidth.constant = widthConstant;
        }
        
    }
}

-(NSDictionary *)deltaFromDate:(NSDate*)date
{
    NSInteger  timeSinceNow = -(NSInteger) [date timeIntervalSinceNow];
    if ((int)(timeSinceNow/2592000)>0) {
        return @{kMonthsKey:@((int)(timeSinceNow/2592000))};
    }else if ((int)(timeSinceNow/86400)>0) {
        return @{kDaysKey:@((int)(timeSinceNow/86400))};
    }else if ((int)(timeSinceNow/3600)>0) {
        return @{kHoursKey:@((int)(timeSinceNow/3600))};
    }else if ((int)(timeSinceNow/60)>0) {
        return @{kMintsKey:@((int)(timeSinceNow/60))};
    }else{
        return @{kSecondsKey:@(timeSinceNow)};
    }
}
@end
