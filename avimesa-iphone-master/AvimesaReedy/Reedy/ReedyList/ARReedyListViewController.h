//
//  ARReedyListViewController.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/17/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ARReedyListViewController : UIViewController

-(IBAction)addReedy:(id)sender;
-(IBAction)hubStup:(id)sender;

@end
