//
//  ARReedyListViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 9/17/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReedyListViewController.h"
#import "ARLoginManager.h"
#import "ARAppDelegate.h"
#import "NSManagedObjectContext+Helper.h"
#import "Reedy.h"
#import "ARCoreDataStack.h"
#import "ARReedyListCell.h"
#import "ARReedyDetailController.h"
#import "Location.h"
#import "ARDeviceManager.h"
#import "ARReedySetupContainer.h"
#import "ARHubSetupContainer.h"

@interface ARReedyListViewController ()<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>
{
    NSTimer * updateTimer;
    NSTimer * refreshimer;
    BOOL  isVisible;
}

@property (nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,weak)IBOutlet UITableView* tableView;

//detail labels
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *updateInfo;
@property (weak, nonatomic) IBOutlet UILabel *reedyCount;

@property (strong, nonatomic)  UIBarButtonItem *addReedy;

//selected reedy
@property (strong, nonatomic)  Reedy * selectedReedy;

//location
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;

//setup views
@property (weak, nonatomic) IBOutlet UIView *setupContainer;
@property (weak, nonatomic) UINavigationController *setupContainerNavigationController;
@end

@implementation ARReedyListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
     self.tableView.allowsMultipleSelectionDuringEditing = NO;
    
    //custom fonts
    [self.reedyCount setFont:[UIFont fontWithName:@"Lato-Regular" size:12.0f]];
    [self.updateInfo setFont:[UIFont fontWithName:@"Lato-Regular" size:9.0f]];
    [self.name setFont:[UIFont fontWithName:@"Lato-Regular" size:19.0f]];
    self.addReedy = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"plus_icon"]
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(addReedy:)];
    [self.navigationItem setRightBarButtonItem:self.addReedy];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LastModifiedKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mocDidSave) name:AR_MOC_DID_SAVE object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.addReedy.enabled = true;
    [super viewWillAppear:animated];
    
    //clear fetch cache
    [NSFetchedResultsController deleteCacheWithName:@"reedies"];
    self.fetchedResultsController = nil;
    
    [self.tableView reloadData];
    isVisible = YES;
    [self checkUpdate];
    [self.tableView reloadData];
    
    //setup configuration
    [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateHubsWithCompletion:^(BOOL success) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if ([[ARDeviceManager sharedManager] isExistsReedy])
            {
                self.setupContainer.hidden = YES;
            }
            else
            {
                self.setupContainer.hidden = NO;
                if (self.setupContainerNavigationController && [[self.setupContainerNavigationController.viewControllers firstObject] isKindOfClass:[ARHubSetupContainer class]])
                {
                    ARHubSetupContainer * hubSetupContainer = [self.setupContainerNavigationController.viewControllers firstObject];
                    [hubSetupContainer updateSetupView];
                }
            }
        });
    }];
   
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    isVisible = NO;
    
    [self _cancelTimer];
    [[ARAPIClient sharedClient] cancelReediesRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedReedy = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"ReedyListToReedyDetail" sender:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController  sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ARReedyListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReedyListCell"];
    
    Reedy *reedy = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.reedy= reedy;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Reedy * reedy = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [self.fetchedResultsController.managedObjectContext deleteObject:reedy];
        if (reedy) {
            [[ARDeviceManager sharedManager] deleteReedyWithID:reedy.identifier callback:nil];
        }
        
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch((int)type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView endUpdates];
}


#pragma mark - navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ReedyListToReedyDetail"])
    {
        ARReedyDetailController* controller = [segue destinationViewController];
        controller.reedy = self.selectedReedy;
        [controller update];
    }
    else if ([segue.identifier isEqualToString:@"ReedyListToSetupContainer"])
    {
        if ([[segue destinationViewController] isKindOfClass:[UINavigationController class]]) {
            
            self.setupContainerNavigationController = [segue destinationViewController];
        }
    }
    
}

#pragma mark - actions

-(IBAction)logOut:(id)sender
{
    [[ARLoginManager sharedLoginManager] performLogout];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self _cancelTimer];
}
-(IBAction)addReedy:(id)sender
{
    self.addReedy.enabled = false;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"HomeToStep1" sender:nil];
    });
}
-(IBAction)hubStup:(id)sender
{
    self.addReedy.enabled = false;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"reedyListToHubVC" sender:nil];
    });
}



#pragma mark - updates timers

-(void)_cancelTimer
{
    if (updateTimer)
    {
        [updateTimer invalidate];
        updateTimer = nil;
    }
}

-(void)_cancelRefreshTimer
{
    if (refreshimer)
    {
        [refreshimer invalidate];
        refreshimer = nil;
    }
}

-(void)_initTimer
{
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(checkUpdate) userInfo:nil repeats:NO];
}

-(void)_initRefreshTimer
{
    updateTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(checkRefres) userInfo:nil repeats:YES];
}

#pragma mark - updates actions

-(void)checkRefres
{
    [self.tableView reloadData];
}

-(void)checkUpdate
{
    [self _cancelTimer];
    if ([[ARLoginManager sharedLoginManager] isUserLoggedIn]) {
        [[(ARAppDelegate*)([UIApplication sharedApplication].delegate) dataUpdateController] updateReediesWithUserID:@"" completion:^(BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (isVisible) {
                    [self _initTimer];
                }
            });
        }];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - private

-(void)mocDidSave
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadData];
    });
}

-(void)reloadData
{
    Location *location =[[ARDeviceManager sharedManager] curentLocationObject];
    if (location) {
        if (location.name) {
            self.name.text = [location.name uppercaseString];
        }
        if ([location.image length]) {
            [self.locationImage sd_setImageWithURL:[NSURL URLWithString: location.image] placeholderImage:nil options:SDWebImageRefreshCached];
        }
        else
        {
            [self.locationImage setImage:[UIImage imageNamed:@"no_photo"]];
        }
    }

   NSInteger locationId = [[ARDeviceManager sharedManager]  curentLocationId];
    
    //init fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Reedy"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"state_change_time" ascending:NO];
    
    if (locationId >0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %d",@"location_id",locationId];
        [request setPredicate:predicate];
    }
    [request setSortDescriptors:@[ sortDescriptor ]];
    
    //init fetch result controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext mainQContext] sectionNameKeyPath:nil cacheName:@"reedies"];
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    NSAssert(!error, @"error: %@", error);
    [self.tableView reloadData];
    self.reedyCount.text = [NSString stringWithFormat:@"Reedy Devices / %lu",[[[self.fetchedResultsController sections] objectAtIndex:0] numberOfObjects]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"'LAST UPDATED' hh:mm a"];
    
    self.updateInfo.text = [dateFormatter stringFromDate:[NSDate date]];
    self.fetchedResultsController.delegate = self;
}
@end
