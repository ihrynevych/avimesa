//
//  ARReedySetupStep1ViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import "ARReedySetupStep1ViewController.h"
#import "ARPeripheralConnectionsManager.h"
#import "ARAlertManager.h"
#import "ARReedySetupStep2ViewController.h"

@interface ARReedySetupStep1ViewController ()<ARPeripheralConnectionsManagerObserver>{
    int percentProgressValue;
    BOOL isFirstLoad;
}

//progress view
//progress view components
@property (nonatomic,weak) IBOutlet UIView * progressBorderView;
@property (nonatomic,weak) IBOutlet UIView * progressBackgroundView;
@property (nonatomic,weak) IBOutlet UIView * progressFillView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressLeadingSpaceConstraint;

@end

@implementation ARReedySetupStep1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupProgress];
    self.navigationItem.titleView = [ARSideMenuController logo];;
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(back:)];
    backButtonItem.title = @"Back";
    [self.navigationItem setLeftBarButtonItem:backButtonItem];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    [[ARPeripheralConnectionsManager sharedManager] stopAllConnections];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setProgressValue:1];
    if (!isFirstLoad) {
        [[ARPeripheralConnectionsManager sharedManager] registerManagerStateObserver:self];
        [[ARPeripheralConnectionsManager sharedManager] startReedyConnection];
        isFirstLoad = true;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[ARPeripheralConnectionsManager sharedManager] unregisterManagerStateObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateProgressView];
}

-(void)setupProgress{
    
    // rounded corners
    self.progressBackgroundView.layer.cornerRadius = self.progressBackgroundView.bounds.size.height/2;
    self.progressBorderView.layer.cornerRadius = self.progressBorderView.bounds.size.height/2;
    self.progressFillView.layer.cornerRadius = self.progressFillView.bounds.size.height/2;
}
#pragma mark Progress View

-(void)setProgressValue:(int)progressValue
{
    percentProgressValue = progressValue;
    [self updateProgressView];
}

-(void)updateProgressView
{
    //calulate max width
    CGFloat maxWidth = self.progressBorderView.bounds.size.width - 2*self.progressLeadingSpaceConstraint.constant;
    
    //calculate progress value width
    CGFloat progressWidth;
    if (percentProgressValue >=100)
        progressWidth = maxWidth;
    else if (percentProgressValue<=0)
        progressWidth = 0.0f;
    else
        progressWidth = maxWidth*percentProgressValue/100;
    
    //update progress
    self.progressWidthConstraint.constant = progressWidth;
    
}

-(void)connectionTimeout{
    [[ARAlertManager sharedManager] showRedAlertWithText:@"Connection timeout"];
    
}
-(void)connectionFail{
    [[ARAlertManager sharedManager] showRedAlertWithText:@"Connection did fail"];
}

-(void)progressDidChangeWithValue:(int)progressValue{
    [self setProgressValue:progressValue];
    
}

-(void)didConnectToReedyWithUDID:(NSString*)reedyUDID{
    [[ARAlertManager sharedManager] showGreenAlertWithText:@"Did connect to Reedy"];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ARReedySetupStep2ViewController *vc = (ARReedySetupStep2ViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ARReedySetupStep2ViewController"];
    vc.reedyOldUDID = reedyUDID;
    [self.navigationController pushViewController:vc animated:true];
    
}
    
-(void) didConnectToHubWithUUID:(NSString *)reedyUUID wifiArray:(NSArray *)wifiArray {
    
}

-(void)didUpdateReedy{
    
}

-(void)reedyUpdateFailed{
    
}

@end
