//
//  ARReedySetupStep2ViewController.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARReedySetupStep2ViewController : UIViewController

@property (strong, nonatomic)  NSString *reedyOldUDID;

@end
