//
//  ARReedySetupStep2ViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//
#import "ARPeripheralConnectionsManager.h"
#import "ARAlertManager.h"
#import "ARReedySetupStep2ViewController.h"
#import "NSManagedObjectContext+Helper.h"
#import "ARDeviceManager.h"
#import "ARReedyStep2Cell.h"
#import "Hub.h"

@interface ARReedySetupStep2ViewController ()<ARPeripheralConnectionsManagerObserver,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    int percentProgressValue;
    NSArray * hubsArray;
}

//progress view
//progress view components
@property (nonatomic,weak) IBOutlet UIView * progressBorderView;
@property (nonatomic,weak) IBOutlet UIView * progressBackgroundView;
@property (nonatomic,weak) IBOutlet UIView * progressFillView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic)  NSString *reedyNewUDID;
@property (strong, nonatomic)  id hubID;
@property (strong, nonatomic)  NSMutableArray *dataSource;

@end

@implementation ARReedySetupStep2ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupProgress];
    self.navigationItem.titleView = [ARSideMenuController logo];;
    hubsArray = [[NSManagedObjectContext mainQContext] fetchObjectsWithEntityName:@"Hub" predicate:nil sortKey:nil ascending:NO];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:nil
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(back:)];
    backButtonItem.title = @"Back";
    [self.navigationItem setLeftBarButtonItem:backButtonItem];
    
    Hub * hub = hubsArray[0];
    self.reedyNewUDID = hub.uuid;
    self.hubID = hub.identifier;
    [[ARPeripheralConnectionsManager sharedManager] registerManagerStateObserver:self];
}
- (IBAction)back:(id)sender {
    
    [[ARPeripheralConnectionsManager sharedManager] unregisterManagerStateObserver:self];
    
    [[ARPeripheralConnectionsManager sharedManager] stopAllConnections];
    NSInteger count =[self.navigationController.viewControllers count];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count-3] animated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
//    [[ARPeripheralConnectionsManager sharedManager] registerManagerStateObserver:self];
    [super viewWillAppear:animated];
    [self setProgressValue:0];

}
- (IBAction)addAction:(id)sender {
    [self.nameTextField resignFirstResponder];
    if(!self.nameTextField.text||self.nameTextField.text.length == 0){
        [[ARAlertManager sharedManager] showRedAlertWithText:@"Empty name"];
        return;
    }
    if ([self.reedyOldUDID isEqualToString:self.reedyNewUDID])
    {
        [[ARAlertManager sharedManager] showRedAlertWithText:@"Reedy already associated with this hub"];
        return;
    }
    self.addButton.enabled = false;
    [[ARAlertManager sharedManager] showGreenAlertWithText:@"Start processing"];
    [[ARDeviceManager sharedManager] createReedyWithName:self.nameTextField.text hubID:self.hubID deviceID:@(1) state:YES callback:^(BOOL isSucceess, id identifier) {
        
        if (isSucceess && identifier)
        {
            [[ARPeripheralConnectionsManager sharedManager] updateReedyWithUDID:self.reedyNewUDID reedyID:[identifier intValue]];
        }else
        {
            self.addButton.enabled = true;
            [[ARAlertManager sharedManager] showRedAlertWithText:@"Ups"];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateProgressView];
}

-(void)setupProgress{
    
    // rounded corners
    self.progressBackgroundView.layer.cornerRadius = self.progressBackgroundView.bounds.size.height/2;
    self.progressBorderView.layer.cornerRadius = self.progressBorderView.bounds.size.height/2;
    self.progressFillView.layer.cornerRadius = self.progressFillView.bounds.size.height/2;
}
#pragma mark Progress View

-(void)setProgressValue:(int)progressValue
{
    percentProgressValue = progressValue;
    [self updateProgressView];
}

-(void)updateProgressView
{
    //calulate max width
    CGFloat maxWidth = self.progressBorderView.bounds.size.width - 2*self.progressLeadingSpaceConstraint.constant;
    
    //calculate progress value width
    CGFloat progressWidth;
    if (percentProgressValue >=100)
        progressWidth = maxWidth;
    else if (percentProgressValue<=0)
        progressWidth = 0.0f;
    else
        progressWidth = maxWidth*percentProgressValue/100;
    
    //update progress
    self.progressWidthConstraint.constant = progressWidth;
    
}
-(void)connectionTimeout{
    self.addButton.enabled = true;
    [[ARAlertManager sharedManager] showRedAlertWithText:@"Connection timeout"];
    
}
-(void)connectionFail{
    self.addButton.enabled = true;
    [[ARAlertManager sharedManager] showRedAlertWithText:@"Connection did fail"];
}

-(void)progressDidChangeWithValue:(int)progressValue{
    [self setProgressValue:progressValue];
    
}

-(void)didConnectToReedyWithUDID:(NSString*)reedyUDID{
    
}

-(void)didUpdateReedy{
    self.addButton.enabled = true;
    [[ARAlertManager sharedManager] showGreenAlertWithText:@"Reedy added"];
    [self back:nil];
}

-(void)reedyUpdateFailed{
    self.addButton.enabled = true;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Hub * hub = hubsArray[indexPath.row];
    self.reedyNewUDID = hub.uuid;
    self.hubID = hub.identifier;
    [self.tableView reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [hubsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ARReedyStep2Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"ARReedyStep2Cell"];
    
    Hub * hub = hubsArray[indexPath.row];
    cell.name.text= hub.name;
    if ([hub.uuid isEqualToString:self.reedyNewUDID])
    {
        cell.checkImage.hidden = false;
    }else
    {
        cell.checkImage.hidden = true;
    }
    return cell;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField.text && [textField.text length]) {
    }
    return YES;
}

@end
