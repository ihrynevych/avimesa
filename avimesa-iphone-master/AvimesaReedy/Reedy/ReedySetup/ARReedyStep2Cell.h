//
//  ARReedyStep2Cell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 5/28/15.
//  Copyright (c) 2015 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARReedyStep2Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel * name;
@property (weak, nonatomic) IBOutlet UIImageView * checkImage;

@end
