//
//  ARTableViewCell.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/16/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARSideMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
