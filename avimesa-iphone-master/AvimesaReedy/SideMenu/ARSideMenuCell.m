//
//  ARTableViewCell.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/16/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARSideMenuCell.h"

@implementation ARSideMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.label setFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
