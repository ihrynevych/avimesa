//
//  ARSideMenuController.h
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/14/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

//menu items
typedef NS_ENUM (NSUInteger, ARMenuItem) {
    ARDashboardMenuItem,
    ARProfileMenuItem,
    ARHubsMenuItem,
    ARlocationsMenuItem,
    ARSettingsMenuItem,
    ARLogoutMenuItem,
    ARMenuItemCount,
    ARChangeLocationMenuItem
};
@interface ARSideMenuController : UIViewController

// open menu with item
+ (void)openControllerOfMenuItem:(ARMenuItem)menuItem;

//navigation bar logo
+ (UIImageView *)logo;
@end
