//
//  ARSideMenuController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/14/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARSideMenuController.h"
#import "MFSideMenuContainerViewController.h"
#import "ARLoginManager.h"
#import "ARSideMenuCell.h"
#import "ARDeviceManager.h"
#import "Location.h"


@interface ARSideMenuController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView    *menuTableView;
@property (weak, nonatomic) IBOutlet UIImageView    *selectedLocationImage;
@property (weak, nonatomic) IBOutlet UILabel        *selectedLocationTitleLable;
@property (weak, nonatomic) IBOutlet UILabel        *menuLablel;
@property (weak, nonatomic) IBOutlet UILabel        *currentLocationTitleLablel;
@property (weak, nonatomic) IBOutlet UIButton        *changeLocationButton;

@property (strong, nonatomic) NSArray *menuItems;
@end

@implementation ARSideMenuController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //custom fonts
    [self.selectedLocationTitleLable setFont:[UIFont fontWithName:@"Lato-Regular" size:16.0f]];
    [self.currentLocationTitleLablel setFont:[UIFont fontWithName:@"Lato-Regular" size:9.0f]];
    [self.menuLablel setFont:[UIFont fontWithName:@"Lato-Regular" size:10.0f]];
    self.changeLocationButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:11.0f];
    [self loadMenuItems];
    
    //default menu item
    [ARSideMenuController openControllerOfMenuItem:[[self.menuItems firstObject] integerValue]];
    
    //observe side menu states
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sideMenuStateDidChange:) name:MFSideMenuStateNotificationEvent object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //update curent location info
    Location*location = [[ARDeviceManager sharedManager] curentLocationObject];
    if (location) {
        self.selectedLocationTitleLable.text = [location.name uppercaseString];
        if ([location.image length]) {
            [self.selectedLocationImage sd_setImageWithURL:[NSURL URLWithString: location.image] placeholderImage:nil options:SDWebImageRefreshCached];
        }
        
    }
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadMenuItems
{
    self.menuItems = @[ @(ARDashboardMenuItem), @(ARProfileMenuItem), @(ARHubsMenuItem),@(ARlocationsMenuItem), @(ARSettingsMenuItem) , @(ARLogoutMenuItem)];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.menuItems count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ARSideMenuCell";
    ARSideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    if (!cell) {
        cell = [[ARSideMenuCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    ARMenuItem menuItem = [[self.menuItems objectAtIndex:indexPath.row] integerValue];
    cell.label.text = [self titleOfMenuItem:menuItem];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //select menu action
    ARMenuItem menuItem = [[self.menuItems objectAtIndex:indexPath.row] integerValue];
    
    if (menuItem == ARLogoutMenuItem)
        [self logout];
    else
        [ARSideMenuController openControllerOfMenuItem:menuItem];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Actions

-(void)sideMenuStateDidChange:(NSNotification*)notification
{
    if (notification.userInfo && [notification.userInfo valueForKey:@"eventType"]) {
        if ([[notification.userInfo valueForKey:@"eventType"] integerValue] == MFSideMenuStateEventMenuWillOpen) {
            Location*location = [[ARDeviceManager sharedManager] curentLocationObject];
            if (location) {
                self.selectedLocationTitleLable.text = [location.name uppercaseString];
                if ([location.image length]) {
                    [self.selectedLocationImage sd_setImageWithURL:[NSURL URLWithString: location.image] placeholderImage:nil options:SDWebImageRefreshCached];
                }
            }
        }
    }
}

+ (void)openMenu
{
    [[[ARApplicationFacade sharedInstance] menuContainer] toggleLeftSideMenuCompletion:nil];
}

-(IBAction)changeLocation:(id)sender
{
    [ARSideMenuController openControllerOfMenuItem:ARChangeLocationMenuItem];
}

+ (void)openControllerOfMenuItem:(ARMenuItem)menuItem
{
    NSString *controllerIdentifier;
    switch (menuItem) {
        case ARDashboardMenuItem:
            controllerIdentifier = @"ARReedyListViewController";
            break;
        case ARProfileMenuItem:
            controllerIdentifier = @"ARProfileViewController";
            break;
        case ARHubsMenuItem:
            controllerIdentifier = @"ARHubListViewController";
            break;
        case ARSettingsMenuItem:
            controllerIdentifier = @"ARPlaceholderViewController";
            break;
        case  ARlocationsMenuItem:
            controllerIdentifier = @"ARLocationListViewController";
            break;
        case ARChangeLocationMenuItem:
            controllerIdentifier = @"ARChangeLocationViewController";
            break;
    }
    NSAssert(controllerIdentifier, @"no controller");
    
    UIStoryboard *storyboard   = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:controllerIdentifier];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_btn"]
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(openMenu)];
    
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = -8.0f;
    
    
    [vc.navigationItem setLeftBarButtonItems:@[fixedSpace,menuButtonItem]];
    
    vc.navigationItem.titleView = [self logo];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    nc.navigationBar.tintColor    = [UIColor whiteColor];
    nc.navigationBar.alpha = 0.7f;
    nc.navigationBar.translucent=YES;
    
    MFSideMenuContainerViewController *menuContainer = [[ARApplicationFacade sharedInstance] menuContainer];
    menuContainer.panMode = MFSideMenuPanModeSideMenu;
    [menuContainer setCenterViewController:nc];
    [menuContainer setMenuState:MFSideMenuStateClosed];
}


+ (UIImageView *)logo
{
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_reedy_list"]];
    
    CGRect newFrame = logo.frame;
    newFrame.size = CGSizeMake(64, 24);
    logo.frame = newFrame;
    logo.contentMode = UIViewContentModeScaleAspectFit;
    return logo;
}


- (void)logout
{
    [[ARLoginManager sharedLoginManager] performLogout];
    [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeLogin];
}


#pragma mark - Helper

- (NSString *)titleOfMenuItem:(ARMenuItem)item
{
    NSString *title;
    switch (item) {
        case ARDashboardMenuItem:
            title = @"Dashboard";
            break;
        case ARProfileMenuItem:
            title = @"My Profile";
            break;
        case ARHubsMenuItem:
            title = @"Hubs";
            break;
        case ARlocationsMenuItem:
            title = @"Locations";
            break;
        case ARSettingsMenuItem:
            title = @"Settings";
            break;
        case ARLogoutMenuItem:
            title = @"Logout";
            break;
        default:
            title = @"";
            break;
    }
    return title;
}




@end
