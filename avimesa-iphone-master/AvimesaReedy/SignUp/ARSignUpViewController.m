//
//  ARSignUpViewController.m
//  AvimesaReedy
//
//  Created by Pavlo Yonak on 10/22/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARSignUpViewController.h"
#import "UITextField+TextPadding.h"
#import "ARLoginManager.h"
#import "ARAppDelegate.h"

@interface ARSignUpViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;

@property (weak, nonatomic) IBOutlet UIView *activityView;
@end

@implementation ARSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set paddings for textfiels
    [self.userNameTextField setLeftPadding:10];
    [self.passwordTextField setLeftPadding:10];
    
    self.userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"User Name"
                                                                                   attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password"
                                                                                   attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self hideActivityView:YES];
    
    self.userNameTextField.text = @"";
    self.passwordTextField.text = @"";
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - private

- (IBAction)signUPAction:(UIButton*)sender {
    self.signupButton.enabled = false;
    [self hideActivityView:NO];
    
    [[ARLoginManager sharedLoginManager] performRegistrationWithUserName:self.userNameTextField.text password:self.passwordTextField.text callback:^(BOOL isSucceess, NSDictionary *response, NSString *userID) {
        
        if (isSucceess&&userID) {
            [self signupDidFinish];
//            if ([self.userNameTextField.text length]&&[self.passwordTextField.text length]) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:AR_SIGNUP_DID_FINISH object:nil userInfo:@{@"username":self.userNameTextField.text,@"password":self.passwordTextField.text}];
//            }
//            [self performSelector:@selector(dismiss) withObject:nil afterDelay:0.3f];
            
        }
        else
        {
            self.signupButton.enabled = true;
            [self hideActivityView:YES];
        }
    }];
}

-(void)dismiss
{
    self.signupButton.enabled = true;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signupDidFinish{
    if ([self.userNameTextField.text length]&&[self.passwordTextField.text length]) {
        [[ARLoginManager sharedLoginManager] performLoginWithUserName:self.userNameTextField.text  password:self.passwordTextField.text callback:^(BOOL isSucceess, NSDictionary *response, NSString *userID) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideActivityView:YES];
                self.signupButton.enabled = true;
                if (isSucceess&&userID) {
                    // load locations
                    [[ARApplicationFacade sharedInstance] switchToType:ARNavigationTypeWelcome];
                }
            });
            
            
        }];
    }
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 1) {
        [self.passwordTextField becomeFirstResponder];
    }else if (textField.tag == 2)
    {
        [textField resignFirstResponder];
    }
    return NO;
    
}

-(void)hideActivityView:(BOOL)hidden
{
    [UIView animateWithDuration:0.2f animations:^{
        self.activityView.alpha = hidden?0.0f:1.0f;
    }];
}
@end
