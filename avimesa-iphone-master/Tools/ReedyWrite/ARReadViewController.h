//
//  ARReadViewController.h
//  ReedyWrite
//
//  Created by Pavlo Yonak on 9/9/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ARReadViewController : UIViewController
@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral    *connectedPeripheral;
@property (strong, nonatomic) CBCharacteristic *connectedCharacteristic;
@end
