//
//  ARReadViewController.m
//  ReedyWrite
//
//  Created by Pavlo Yonak on 9/9/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARReadViewController.h"

@interface ARReadViewController ()<CBPeripheralDelegate,UITextFieldDelegate,CBCentralManagerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *writeButton;
@property (weak, nonatomic) IBOutlet UIButton *readButton;
@property (weak, nonatomic) IBOutlet UIButton *disconnectButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UILabel *info;
@end

@implementation ARReadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
   
}
-(void)dealloc
{
    
}
- (IBAction)connect:(id)sender {
    self.connectButton.enabled = false;
    if (_centralManager&&_connectedPeripheral) {
        _centralManager.delegate = self;
        [_centralManager connectPeripheral:_connectedPeripheral options:nil];
    }
    
}
- (IBAction)disconnect:(id)sender
{
    self.connectButton.enabled = true;
    self.disconnectButton.enabled = false;
    self.info.text= @"disconnected";

    [_centralManager cancelPeripheralConnection:_connectedPeripheral];
}
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    self.info.text= @"connected";
    _connectedPeripheral.delegate = self;
    [_connectedPeripheral discoverServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:REEDY_SERVICE_UUID]]];
    self.connectButton.enabled = false;
    self.disconnectButton.enabled = true;
    self.readButton.enabled = true;
}
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    self.info.text= @"disconnected";
    self.connectButton.enabled = true;
    self.disconnectButton.enabled = false;
    self.readButton.enabled = false;
    self.writeButton.enabled = false;
}
-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    self.info.text= @"disconnected";
    self.connectButton.enabled = true;
    self.disconnectButton.enabled = false;
    self.readButton.enabled = false;
    self.writeButton.enabled = false;
    NSLog(@"");
    NSLog(@"");
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if (_connectedPeripheral) {
//        _connectedPeripheral.delegate = self;
//        [_connectedPeripheral discoverServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:REEDY_SERVICE_UUID]]];
//    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (error) {
        return;
    }
    
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:[NSArray arrayWithObject:[CBUUID UUIDWithString:REEDY_CHARACTERISTIC_UUID]] forService:service];
    }
    NSLog(@"");
    // Discover other characteristics
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if (error) {
        return;
    }
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:REEDY_CHARACTERISTIC_UUID]]) {
            //            [peripheral readValueForCharacteristic:characteristic];
            _connectedCharacteristic =characteristic;
            self.readButton.enabled = true;
            
        }
        
        
        //        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        //        [peripheral discoverDescriptorsForCharacteristic:characteristic];
    }
    NSLog(@"");
    
    
}
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        NSLog(@"Error");
        return;
    }
    self.writeButton.enabled= true;
    self.readButton.enabled = true;
    self.textField.text = [self hexStringFromData:characteristic.value];
    
}
- (IBAction)read:(id)sender {
    if (_connectedCharacteristic) {
        self.readButton.enabled = false;
        [_connectedPeripheral readValueForCharacteristic:_connectedCharacteristic];
    }

}
- (IBAction)write:(id)sender {
    self.writeButton.enabled = false;
    [_connectedPeripheral writeValue:[self dataFromHexString:self.textField.text] forCharacteristic:_connectedCharacteristic type:CBCharacteristicWriteWithResponse];
}
-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    self.writeButton.enabled = true;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (NSData *)dataFromHexString:(NSString *)string {
    string = [string lowercaseString];
    NSMutableData *data= [NSMutableData new];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i = 0;
    int length = string.length;
    while (i < length-1) {
        char c = [string characterAtIndex:i++];
        if (c < '0' || (c > '9' && c < 'a') || c > 'f')
            continue;
        byte_chars[0] = c;
        byte_chars[1] = [string characterAtIndex:i++];
        whole_byte = strtol(byte_chars, NULL, 16);
        [data appendBytes:&whole_byte length:1];
        
    }
    
    return data;
}

- (NSString *)hexStringFromData:(NSData *)data {
    
    NSUInteger bytesToConvert = [data length];
    const unsigned char *uuidBytes = [data bytes];
    NSMutableString *hexString = [NSMutableString stringWithCapacity:16];
    
    for (NSUInteger currentByteIndex = 0; currentByteIndex < bytesToConvert; currentByteIndex++)
    {
        switch (currentByteIndex)
        {
            case 3:
            case 5:
            case 7:
            case 9:[hexString appendFormat:@"%02x-", uuidBytes[currentByteIndex]]; break;
            default:[hexString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
        }
        
    }
    return hexString;
}
@end
