//
//  ARAppDelegate.h
//  ReedyWrite
//
//  Created by Pavlo Yonak on 9/9/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
