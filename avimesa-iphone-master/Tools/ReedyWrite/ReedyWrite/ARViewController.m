//
//  ARViewController.m
//  ReedyWrite
//
//  Created by Pavlo Yonak on 9/9/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import "ARViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>

#import "ARReadViewController.h"

@interface ARViewController ()< CBCentralManagerDelegate, CBPeripheralDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataPerh;
    NSMutableArray *dataIDs;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *connectedPeripheral;
@end

@implementation ARViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    dispatch_queue_t centralQueue = dispatch_queue_create("mycentralqueue", DISPATCH_QUEUE_SERIAL);
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    dataPerh = [NSMutableArray array];
    dataIDs = [NSMutableArray array];
	// Do any additional setup after loading the view, typically from a nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_connectedPeripheral) {
        _centralManager.delegate = self;
//        [_centralManager cancelPeripheralConnection:_connectedPeripheral];
        self.tableView.userInteractionEnabled = true;
        if (_centralManager.state == CBCentralManagerStatePoweredOn) {
            // Scan for devices
            [_centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:REEDY_SERVICE_UUID]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
            //        [_centralManager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
            NSLog(@"Scanning started");
        }
    }
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_centralManager stopScan];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    // You should test all scenarios
    if (central.state != CBCentralManagerStatePoweredOn) {
        return;
    }
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        // Scan for devices
      [_centralManager scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:REEDY_SERVICE_UUID]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
        //        [_centralManager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
        NSLog(@"Scanning started");
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSString * idString = [peripheral.identifier UUIDString];
    BOOL contain = NO;
    for (NSString * tempId in dataIDs) {
        if ([tempId isEqualToString:idString]) {
            contain = YES;
        }
    }
    if (!contain) {
        [dataIDs addObject:idString];
        [dataPerh addObject:peripheral];
        [self.tableView reloadData];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataIDs count];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (dataPerh[indexPath.row]) {
        self.tableView.userInteractionEnabled = false;
        _connectedPeripheral = dataPerh[indexPath.row];
        [self performSegueWithIdentifier:@"peripheral" sender:self];
//        [_centralManager connectPeripheral:dataPerh[indexPath.row] options:nil];
    }
    
}
//- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        _connectedPeripheral = peripheral;
//        [self performSegueWithIdentifier:@"peripheral" sender:self];
//    });
//    
//}
//-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
//{
//    NSLog(@"");
//    NSLog(@"");
//    
//}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"peripheral"]) {
        ARReadViewController* controller = [segue destinationViewController];
        controller.connectedPeripheral = _connectedPeripheral;
        controller.centralManager = _centralManager;
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"");
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"peripheralCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"peripheralCell"];
    }
    cell.textLabel.text = [dataIDs objectAtIndex:indexPath.row];
    return cell;
}


@end
