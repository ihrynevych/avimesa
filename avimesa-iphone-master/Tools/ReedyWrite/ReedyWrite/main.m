//
//  main.m
//  ReedyWrite
//
//  Created by Pavlo Yonak on 9/9/14.
//  Copyright (c) 2014 Pavlo Yonak. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ARAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ARAppDelegate class]));
    }
}
